package com.example.carlos.pruebas.ui.PruebaSugar;

import android.os.Parcel;
import android.os.Parcelable;

import co.uk.rushorm.core.RushObject;

public class TipoResulServ extends RushObject implements Parcelable {

  private String codResultadoServ;
  private String descripcion;
  private String codTipoServ;
  private String bolNotificacion;
  private String bolMailGestor;
  private String tipoAviso;
  private String idRush; //Clave primaria de la tabla. Se compone de codResultadoServ + descripcion


  public TipoResulServ() {
  }

  public String getCodResultadoServ() {
    return codResultadoServ;
  }

  public void setCodResultadoServ(String codResultadoServ) {
    this.codResultadoServ = codResultadoServ;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getCodTipoServ() {
    return codTipoServ;
  }

  public void setCodTipoServ(String codTipoServ) {
    this.codTipoServ = codTipoServ;
  }

  public String getBolNotificacion() {
    return bolNotificacion;
  }

  public void setBolNotificacion(String bolNotificacion) {
    this.bolNotificacion = bolNotificacion;
  }

  public String getBolMailGestor() {
    return bolMailGestor;
  }

  public void setBolMailGestor(String bolMailGestor) {
    this.bolMailGestor = bolMailGestor;
  }

  public String getTipoAviso() {
    return tipoAviso;
  }

  public void setTipoAviso(String tipoAviso) {
    this.tipoAviso = tipoAviso;
  }

  public String getIdRush() {
    return idRush;
  }

  public void setIdRush(String idRush) {
    this.idRush = idRush;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public String toString() {
    return descripcion;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.codResultadoServ);
    dest.writeString(this.descripcion);
    dest.writeString(this.codTipoServ);
    dest.writeString(this.bolNotificacion);
    dest.writeString(this.bolMailGestor);
    dest.writeString(this.tipoAviso);
  }


  protected TipoResulServ(Parcel in) {
    this.codResultadoServ = in.readString();
    this.descripcion = in.readString();
    this.codTipoServ = in.readString();
    this.bolNotificacion = in.readString();
    this.bolMailGestor = in.readString();
    this.tipoAviso = in.readString();
  }

  public static final Parcelable.Creator<TipoResulServ> CREATOR = new Parcelable
      .Creator<TipoResulServ>() {
    @Override
    public TipoResulServ createFromParcel(Parcel source) {
      return new TipoResulServ(source);
    }

    @Override
    public TipoResulServ[] newArray(int size) {
      return new TipoResulServ[size];
    }
  };
}
