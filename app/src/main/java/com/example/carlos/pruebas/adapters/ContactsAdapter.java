package com.example.carlos.pruebas.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.carlos.pruebas.R;
import com.example.carlos.pruebas.adapters.holders.ContactsHolder;
import com.example.carlos.pruebas.interfacesMVP.ContactsMVP;
import com.example.carlos.pruebas.model.Contact;

import java.util.List;


/**
 * Creado por Carlos Sanchidrián Sánchez en 05/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsHolder> {
    private List <Contact>listContacts;
    private onContactListInteractorListener mOnContactListInteractorListener;
    private ContactsMVP.Presenter presenter;

    public ContactsAdapter(List listContacts,
                           onContactListInteractorListener interactorListener,
                           ContactsMVP.Presenter presenter) {
        this.listContacts = listContacts;
        this.mOnContactListInteractorListener = interactorListener;
        this.presenter = presenter;
    }

    @Override
    public ContactsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contact, parent, false);
        return new ContactsHolder(v);
    }

    @Override
    public void onBindViewHolder(final ContactsHolder holder, final int position) {
        holder.bindContact(listContacts.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnContactListInteractorListener.onRowClick(position);
            }
        });

        holder.getImageViewDelete().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                presenter.deleteContact(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.listContacts.size();
    }

    public interface onContactListInteractorListener{
        void onRowClick(int position);
    }
}