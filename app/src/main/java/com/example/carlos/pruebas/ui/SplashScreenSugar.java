package com.example.carlos.pruebas.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.carlos.pruebas.R;
import com.example.carlos.pruebas.ui.PruebaSugar.BDTalos;
import com.example.carlos.pruebas.ui.PruebaSugar.DatosMaestrosRushManager;
import com.example.carlos.pruebas.ui.PruebaSugar.ListParameterizedType;
import com.example.carlos.pruebas.ui.PruebaSugar.ObjectParameterizedType;
import com.example.carlos.pruebas.ui.PruebaSugar.RespuestaDatosMaestros;
import com.example.carlos.pruebas.ui.PruebaSugar.RushConfig;
import com.example.carlos.pruebas.ui.PruebaSugar.TipoDocumento;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushList;

public class SplashScreenSugar extends AppCompatActivity {

    private RespuestaDatosMaestros respuestaDatosMaestrosVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_sugar);
        Log.d("Campo", "Inicio");


        InputStream jsonParsear = getResources().openRawResource(
                getResources().getIdentifier("datos_maestros_response",
                        "raw", getPackageName()));
        String respuestaJson = (inputStreamToString(jsonParsear, "utf-8"));
        respuestaDatosMaestrosVersion = (RespuestaDatosMaestros) parsearRespuestaJsonWrapper
                (respuestaJson, RespuestaDatosMaestros.ROOT_RESPUESTA, RespuestaDatosMaestros
                        .class);
        String a = "";

        compararaConFechaActual("19-09-2017");

    }

    @Override
    protected void onResume() {
        super.onResume();

        RushConfig.init(this);
//        ArrayList<TipoDocumento> tipoDocumento = respuestaDatosMaestrosVersion.getTiposDocumentos();
//        tipoDocumento.get(0).save();
//        RespuestaDatosMaestros tipoDocumento2 = new RushSearch().findSingle(RespuestaDatosMaestros.class);


        DatosMaestrosRushManager.save(respuestaDatosMaestrosVersion);
        RespuestaDatosMaestros respuestaDatosMaestros = DatosMaestrosRushManager.getDataFromDB();
        BDTalos.backupdDatabase();

//        SugarContext.init(this);
//        PruebaSugar pruebaSugar = new PruebaSugar();
//        pruebaSugar.setNombre("carlos");
//        ArrayList<Apellidos> a = new ArrayList<>();
//        Apellidos apellidos1 = new Apellidos("sanchidrian", "sanchez");
//        Apellidos apellidos2 = new Apellidos("sanchidrian", "sanchez");
//        a.add(apellidos1);
//        a.add(apellidos2);
//        pruebaSugar.setApellido(a);
//
//        PruebaSugar.save(pruebaSugar);
//
//        PruebaSugar pruebaSugar2 = PruebaSugar.findById(PruebaSugar.class, 1);
//
//        Apellidos pruebaSugar1 = Apellidos.findById(Apellidos.class, 1);
//        Log.d("Campo", pruebaSugar1.getApellido1());
//


//        Parent parent = new Parent();
//        parent.createList();
//        parent.save();
//
//        Child firstChild = new Child();
//        firstChild.save();
//
//        Child secondChild = new Child();
//        secondChild.save();
//
//        parent.add(firstChild);
//        parent.add(secondChild);
//        parent.save();
//
//        parent = new RushSearch().find(Parent.class).get(0);
//        parent.getChildren().get(0).setString("test1");
//        parent.save();


        /*RushCore.getInstance().registerObjectWithId(pruebaSugar, "1");
        pruebaSugar.save();*/

        //Obtiene el objeto con la información de la base de datos
        //Contact contact = new RushSearch().findSingle(Contact.class);

        //List<PruebaSugar> pruebaSugars = new RushSearch().find(PruebaSugar.class);
        //Log.d("Apellido", pruebaSugars.get(0).getApellido().get(0).getApellido1());


        //String jsonString = new Gson().toJson(pruebaSugar);
//        String jsonString = RushCore.getInstance().serialize(a);
//        List<Rush> objects = RushCore.getInstance().deserialize(jsonString);

    }


    public class Child extends RushObject {

        private String string;

        public Child(String string) {
            this.string = string;
        }

        public Child() {
        }

        public String getString() {
            return string;
        }

        public void setString(String string) {
            this.string = string;
        }
    }

    public class Parent extends RushObject {

        @RushList(classType = Child.class)
        private List<Child> children;

        public Parent() {
        }

        public void createList() {
            this.children = new ArrayList<Child>();
        }

        public boolean add(Child child) {
            return children.add(child);
        }

        public boolean remove(Child child) {
            return children.remove(child);
        }

        public List<Child> getChildren() {
            return children;
        }
    }


    public static List<?> readListGsonFromFile(Context context, String fileRoute, Class<?> clazz) {

        Type type = new ListParameterizedType(clazz);

        Gson gson = new Gson();
        InputStream input = context.getClass().getClassLoader().getResourceAsStream(fileRoute);
        if (input != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(input));
            try {
                return gson.fromJson(reader, type);
            } catch (Exception e) {

                return null;
            }
        } else {
            return null;
        }
    }


    public static Object readObjectGsonFromFile(Context context, String fileRoute, Class<?> clazz) {

        Type type = new ObjectParameterizedType(clazz);

        Gson gson = new Gson();
        InputStream input = context.getClass().getClassLoader().getResourceAsStream(fileRoute);
        if (input != null) {
            JsonReader reader = new JsonReader(new InputStreamReader(input));
            try {
                return gson.fromJson(reader, type);
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }


    public static String inputStreamToString(InputStream inputStream, String codificacion) {
        String resultado = null;
        StringBuilder stringBuilder = new StringBuilder("");
        BufferedReader bufferedReader = null;
        String linea;

        try {
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream,
                        codificacion));

                while ((linea = bufferedReader.readLine()) != null) {
                    stringBuilder.append(linea);
                    stringBuilder.append("\n");
                    linea = null;
                }

                resultado = stringBuilder.toString();
            }
        } catch (Exception ex) {
            resultado = null;
        } finally {
            linea = null;
            stringBuilder = null;

            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException ex) {
                }
            }
        }

        return resultado;
    }

    public static Object parsearRespuestaJsonWrapper(String json, String rootName,
                                                     Class<?> clazz) {
//        RespuestaGenericaJson respuesta = null;
//        if (json != null) {
//            Gson gson = new Gson();
//            JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
//            respuesta = gson.fromJson(((JsonObject) jsonObject.get(rootName)), clazz);
//        }
//        return respuesta;

        Type type = new ObjectParameterizedType(clazz);
        Gson gson = new Gson();

        try {
            //return gson.fromJson(json, type);
            JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
            return gson.fromJson(((JsonObject) jsonObject.get(rootName)), clazz);
        } catch (Exception e) {
            return null;
        }

    }


    public static Integer compararaConFechaActual(String fechaGuardada) {

        Integer resultado = null;
        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");

        String fechaActual = getFechaActualTalosFormatoSGIE();
        fechaActual = fechaActual.replaceAll("-", " ");
        fechaGuardada = fechaGuardada.replaceAll("-", " ");

        try {
            Date fecha1 = myFormat.parse(fechaActual);
            Date fecha2 = myFormat.parse(fechaGuardada);
            long diferencia = fecha1.getTime() - fecha2.getTime();
            resultado = Integer.parseInt("" +TimeUnit.DAYS.convert(diferencia, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    public static String getFechaActualTalosFormatoSGIE() {
        String fechaFormateada;
        Calendar calendar = Calendar.getInstance();
        String dia = formatearCamposFecha(String.valueOf(calendar.get(Calendar.DATE)), 2);
        String mes = formatearCamposFecha(String.valueOf(calendar.get(Calendar.MONTH) + 1), 2);
        String anio = formatearCamposFecha(String.valueOf(calendar.get(Calendar.YEAR)), 2);

        fechaFormateada = dia + "-" + mes + "-" + anio;
        return fechaFormateada;
    }

    private static String formatearCamposFecha(String campo, int longitud) {
        String resultado = campo;

        for (int indice = campo.length(); indice < longitud; indice++) {
            resultado = "0" + resultado;
        }

        return resultado;
    }


}
