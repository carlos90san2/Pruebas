package com.example.carlos.pruebas.ui.PruebaSugar;

import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase que gestionará la creación, modificación y acceso a la Base de Datos de Ejemplo de la
 * aplicación.
 */
public class BDTalos {
    // -------------------------------------------------------------------------
    // ATRIBUTOS*/
    private static BDTalos _instancia;


    /**
     * Contiene la Base de Datos SQLite.
     */
    private SQLiteDatabase _bd;

    /**
     * Clase de ayuda para la creación y modificación de la Base de Datos.
     */
    private String SQLUpdateV2 = "ALTER TABLE servicios ADD COLUMN observacionesDirec TEXT";

    // -------------------------------------------------------------------------
    // CONSTRUCTOR E INSTANCIA*/


    // -------------------------------------------------------------------------
    // MÉTODOS*/

    /**
     * Permite el acceso a la Base de Datos.
     *
     * @return SQLiteDatabase.
     */
    public SQLiteDatabase getBD() {
        return this._bd;
    }

    /**
     * Cierra la Base de Datos. Se debe cerrar la Base de Datos cuando no se necesite accede más a
     * ella.
     */
    public void cerrarBD() {
        this._bd.close();
    }

    /**
     * Metodo para hacer una copia de la base de datos en la tarjeta SD y ver que hay en ella,
     * Cuando queramos saber que es lo que hay en base de datos en algun momento llamar a este metodo
     */
    public static void backupdDatabase() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();
            String packageName = "com.example.carlos.pruebas";
            String sourceDBName = "rush.db";
            String targetDBName = "prueba";
            if (sd.canWrite()) {
                Date now = new Date();
                String currentDBPath = "data/" + packageName + "/databases/" + sourceDBName;
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
                String backupDBPath = targetDBName + dateFormat.format(now) + ".db";

                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                Log.i("backup", "backupDB=" + backupDB.getAbsolutePath());
                Log.i("backup", "sourceDB=" + currentDB.getAbsolutePath());

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
            }
        } catch (Exception e) {
            Log.i("Backup", e.toString());
        }
    }


    // -------------------------------------------------------------------------
    // SQLiteOpenHelper*/


}