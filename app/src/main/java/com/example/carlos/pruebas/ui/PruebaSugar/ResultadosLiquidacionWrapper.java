package com.example.carlos.pruebas.ui.PruebaSugar;

import java.util.List;

public class ResultadosLiquidacionWrapper {

  private List<ResultadoLiquidacion> resultadosLiquidacion;

  public List<ResultadoLiquidacion> getResultadosLiquidacion() {
    return resultadosLiquidacion;
  }

  public void setResultadosLiquidacion(List<ResultadoLiquidacion> resultadosLiquidacion) {
    this.resultadosLiquidacion = resultadosLiquidacion;
  }
}
