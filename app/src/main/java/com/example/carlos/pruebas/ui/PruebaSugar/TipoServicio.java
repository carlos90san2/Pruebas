package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class TipoServicio extends RushObject{

  private String codTipoServicios;
  private String descripcion;
  private String codSituacionReparto;
  private String numDiasBe;
  //indica el resultado OK realizado
  private String codResultadoAsumido;

  public TipoServicio() {
  }

  public String getCodTipoServicios() {
    return codTipoServicios;
  }

  public void setCodTipoServicios(String codTipoServicios) {
    this.codTipoServicios = codTipoServicios;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getCodSituacionReparto() {
    return codSituacionReparto;
  }

  public void setCodSituacionReparto(String codSituacionReparto) {
    this.codSituacionReparto = codSituacionReparto;
  }

  public String getNumDiasBe() {
    return numDiasBe;
  }

  public void setNumDiasBe(String numDiasBe) {
    this.numDiasBe = numDiasBe;
  }

  public String getCodResultadoAsumido() {
    return codResultadoAsumido;
  }

  public void setCodResultadoAsumido(String codResultadoAsumido) {
    this.codResultadoAsumido = codResultadoAsumido;
  }
}
