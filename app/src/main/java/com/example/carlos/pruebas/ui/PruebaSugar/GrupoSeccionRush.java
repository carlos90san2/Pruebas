package com.example.carlos.pruebas.ui.PruebaSugar;

import java.util.ArrayList;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.annotations.RushList;

/**
 * Creado por Carlos Sanchidrián Sánchez en 20/09/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class GrupoSeccionRush extends RushObject {

    @RushList(classType = GrupoSeccion.class)
    private ArrayList<GrupoSeccion> grupoSeccion;

    public GrupoSeccionRush() {
    }

    public GrupoSeccionRush(ArrayList<GrupoSeccion> grupoSeccion) {
        this.grupoSeccion = grupoSeccion;
    }

    public ArrayList<GrupoSeccion> getGrupoSeccion() {
        return grupoSeccion;
    }

    public void setGrupoSeccion(ArrayList<GrupoSeccion> grupoSeccion) {
        this.grupoSeccion = grupoSeccion;
    }
}
