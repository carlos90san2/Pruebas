package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TipoServicioWrapper {
  @SerializedName("tipoServicios")
  private List<TipoServicio> tiposServicios;

  public List<TipoServicio> getTiposServicios() {
    return tiposServicios;
  }

  public void setTiposServicios(List<TipoServicio> tiposServicios) {
    this.tiposServicios = tiposServicios;
  }
}
