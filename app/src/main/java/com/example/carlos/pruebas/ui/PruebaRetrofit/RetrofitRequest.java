package com.example.carlos.pruebas.ui.PruebaRetrofit;

import android.content.Context;

import com.example.carlos.pruebas.model.Contact;
import com.example.carlos.pruebas.model.Http.ContactsService;
import com.example.carlos.pruebas.model.Time;
import com.example.carlos.pruebas.model.dataStrategy.DataStrategy;
import com.example.carlos.pruebas.model.utils.Utils;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Creado por Carlos Sanchidrián Sánchez en 11/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class RetrofitRequest extends DataStrategy {

    private static OkHttpClient httpClient;
    private static final String API_BASE_URL = "http://date.jsontest.com/";
    private static final int TIME_OUT_WS = 5000;

    public void getContacts(final Context context) {

        ContactsService service = null;
        try {
            service = createService(ContactsService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Call<Time> call = service.getContacts();

        call.enqueue(new Callback<Time>() {
            @Override
            public void onResponse(Call<Time> call, Response<Time> response) {
                //Obtiene los datos de la petición convertidos a una lista de obejtos tipo Time
                if(response.isSuccessful()) {
                    Time lista = response.body();

                    //Debido a que los datos de respuesta difieren a los que utiliza la aplicación se obtienen los datos del mock
                    String MOCKS_ROUTE = "res/raw/";
                    String CONTACTS_MOCK_ROUTE = MOCKS_ROUTE + "contacts_mock.json";
                    List<Contact> contacts = (List<Contact>) Utils.readGsonFromFile(context, CONTACTS_MOCK_ROUTE);
                    onGetContactsListener.onGetContactsListener(contacts);
                    //time.save();
                    //Time table = new RushSearch().findSingle(Time.class);}
                }
                else{
                    try {
                        onGetContactsListener.onGetContactsListenerError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Time> call, Throwable t) {
                t.toString();
            }
        });
    }

    public static <S> S createService(Class<S> serviceClass) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        httpClient = new OkHttpClient.Builder()
                .connectTimeout(TIME_OUT_WS, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_WS, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        return retrofit.create(serviceClass);
    }
}
