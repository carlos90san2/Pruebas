package com.example.carlos.pruebas.interactors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

/**
 * Crendo por Carlos Sanchidrián Sánchez en 09/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class SignInInteractorImpl implements SignInInteractor, GoogleApiClient.OnConnectionFailedListener{

    private responseRequest listener;
    private Context context;
    private GoogleApiClient mGoogleApiClient;

    public SignInInteractorImpl(responseRequest listener, Context context){
        this.listener = listener;
        this.context = context;
    }

    @Override
    public void setDataConnect() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .enableAutoManage((FragmentActivity) context, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void requestConnect() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        ((Activity) context).startActivityForResult(signInIntent, 1);
    }

    @Override
    public void checkResultConnect(int requestCode, Intent data) {
        if (requestCode == 1) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result != null && result.isSuccess()) {
                listener.loginSuccess(result.getSignInAccount());
            } else {
                listener.errorLogin("No se ha podido iniciar sesión");
            }
        }
        else{
            listener.errorLogin("No se ha podido iniciar sesión");
        }
    }

    @Override
    public void requestDisconnect() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        listener.signOutSuccess("Se ha desconectado");
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        listener.errorLogin("No se ha podido conectar con Google");
    }

}
