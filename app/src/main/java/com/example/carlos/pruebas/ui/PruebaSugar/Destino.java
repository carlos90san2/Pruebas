package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class Destino extends RushObject{

  private String codDestino;
  private String descripcion;
  private String codPostal;
  private String idRush; //Clave primaria de la tabla, se componede de codDestino + codPostal

  public Destino() {
  }

  public String getCodDestino() {
    return codDestino;
  }

  public void setCodDestino(String codDestino) {
    this.codDestino = codDestino;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getCodPostal() {
    return codPostal;
  }

  public void setCodPostal(String codPostal) {
    this.codPostal = codPostal;
  }

  public String getIdRush() {
    return idRush;
  }

  public void setIdRush(String idRush) {
    this.idRush = idRush;
  }
}
