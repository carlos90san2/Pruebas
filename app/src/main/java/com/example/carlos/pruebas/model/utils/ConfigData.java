package com.example.carlos.pruebas.model.utils;

import com.example.carlos.pruebas.enums.DataSourceEnum;

/**
 * Creado por Carlos Sanchidrián Sánchez en 11/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class ConfigData {

    public static final DataSourceEnum dataSelected = DataSourceEnum.DATA_WS;

}
