package com.example.carlos.pruebas.ui.Dagger2Pruebas;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.carlos.pruebas.R;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class Dagger2PruebasPresenterImpl {

    private Context context;

    public Dagger2PruebasPresenterImpl(Context context) {
        this.context = context;
    }

    public void print() {
        Toast.makeText(context, "presenter", Toast.LENGTH_SHORT).show();
    }
}
