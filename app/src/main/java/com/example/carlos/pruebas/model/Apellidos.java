package com.example.carlos.pruebas.model;

import com.orm.SugarRecord;

import co.uk.rushorm.core.RushObject;

/**
 * Creado por Carlos Sanchidrián Sánchez en 07/09/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class Apellidos extends RushObject{

    String apellido1;
    String apellido2;

    public Apellidos(){
    }

    public Apellidos(String apellido1, String apellido2) {
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }
}
