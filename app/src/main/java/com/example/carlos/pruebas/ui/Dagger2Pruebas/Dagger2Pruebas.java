package com.example.carlos.pruebas.ui.Dagger2Pruebas;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.carlos.pruebas.R;

import javax.inject.Inject;

public class Dagger2Pruebas extends BaseActivity {

    @Inject
    Envio envio;

    @Inject
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dagger2_pruebas);

        getComponent().inject(this);
        Toast.makeText(context, envio.getCodEnvio(), Toast.LENGTH_SHORT).show();
    }
}
