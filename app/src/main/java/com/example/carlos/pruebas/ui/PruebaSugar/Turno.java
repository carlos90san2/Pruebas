package com.example.carlos.pruebas.ui.PruebaSugar;

import java.io.Serializable;

import co.uk.rushorm.core.RushObject;

public class Turno extends RushObject implements Serializable {

  private String codTurno;
  private String desTurno;

  public Turno() {
  }

  public String getCodTurno() {
    return codTurno;
  }

  public void setCodTurno(String codTurno) {
    this.codTurno = codTurno;
  }

  public String getDesTurno() {
    return desTurno;
  }

  public void setDesTurno(String desTurno) {
    this.desTurno = desTurno;
  }

  @Override
  public String toString() {
    return desTurno;
  }
}
