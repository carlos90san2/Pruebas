package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import co.uk.rushorm.core.annotations.RushIgnore;
import co.uk.rushorm.core.annotations.RushList;

public class RespuestaDatosMaestros extends RespuestaGenericaJson {

    public static final String ROOT_RESPUESTA = "PeticionDatosMaestrosVersion";

    private String fechaDatos;

    //    private String codUnidad;
    private DatosUnidad datosUnidad;

    @RushList(classType = TipoImporte.class)
    @SerializedName("tipoImporte")
    private ArrayList<TipoImporte> tiposImporte;

    @SerializedName("producto")
    @RushList(classType = Producto.class)
    private ArrayList<Producto> productos;

    @RushList(classType = Producto.class)
    private ArrayList<Producto> productoAlta;

    @SerializedName("familia")
    @RushList(classType = Familia.class)
    private ArrayList<Familia> familias;

    @RushList(classType = ResultadoLiquidacion.class)
    private ArrayList<ResultadoLiquidacion> resultadosLiquidacion;
    //private SituacionWrapper situacion;

    @SerializedName("situacion")
    @RushList(classType = Situacion.class)
    private ArrayList<Situacion> situaciones;

    @RushList(classType = Ocupacion.class)
    private ArrayList<Ocupacion> ocupaciones;

    @RushList(classType = TipoDocumento.class)
    private ArrayList<TipoDocumento> tiposDocumentos;

    @RushList(classType = Destino.class)
    private ArrayList<Destino> destinos;

    @RushIgnore
    private ArrayList<String> diasFestivosUnd;

    @RushList(classType = DiaFestivo.class)
    private transient ArrayList<DiaFestivo> diasFestivosRush;

    @RushList(classType = Motivo.class)
    private ArrayList<Motivo> motivos;

    @SerializedName("tipoServicios")
    @RushList(classType = TipoServicio.class)
    private ArrayList<TipoServicio> tiposServicios;

    @SerializedName("tipoResulServ")
    @RushList(classType = TipoResulServ.class)
    private ArrayList<TipoResulServ> tiposResulServ;

    @RushList(classType = TiposMatIntCli.class)
    private ArrayList<TiposMatIntCli> tiposMatIntCli;

    @SerializedName("TipoResulValija")
    @RushList(classType = TipoResulValija.class)
    private ArrayList<TipoResulValija> tiposResultadosValijas;

    public RespuestaDatosMaestros() {
    }

    public String getFechaDatos() {
        return fechaDatos;
    }


    public void setFechaDatos(String fechaDatos) {
        this.fechaDatos = fechaDatos;
    }

//    public String getCodUnidad() {
//        return codUnidad;
//    }
//
//    public void setCodUnidad(String codUnidad) {
//        this.codUnidad = codUnidad;
//    }

    public DatosUnidad getDatosUnidad() {
        return datosUnidad;
    }

    public void setDatosUnidad(DatosUnidad datosUnidad) {
        this.datosUnidad = datosUnidad;
    }

    public ArrayList<ResultadoLiquidacion> getResultadosLiquidacion() {
        return resultadosLiquidacion;
    }

    public void setResultadosLiquidacion(ArrayList<ResultadoLiquidacion> resultadosLiquidacion) {
        this.resultadosLiquidacion = resultadosLiquidacion;
    }

    public ArrayList<Situacion> getSituaciones() {
        return situaciones;
    }

    public void setSituaciones(ArrayList<Situacion> situaciones) {
        this.situaciones = situaciones;
    }


    public ArrayList<TipoImporte> getTiposImporte() {
        return tiposImporte;
    }

    public void setTiposImporte(ArrayList<TipoImporte> tiposImporte) {
        this.tiposImporte = tiposImporte;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }

    public ArrayList<Producto> getProductoAlta() {
        return productoAlta;
    }

    public void setProductoAlta(ArrayList<Producto> productoAlta) {
        this.productoAlta = productoAlta;
    }

    public ArrayList<Familia> getFamilias() {
        return familias;
    }

    public void setFamilias(ArrayList<Familia> familias) {
        this.familias = familias;
    }

    public ArrayList<Ocupacion> getOcupaciones() {
        return ocupaciones;
    }

    public void setOcupaciones(ArrayList<Ocupacion> ocupaciones) {
        this.ocupaciones = ocupaciones;
    }

    public ArrayList<TipoDocumento> getTiposDocumentos() {
        return tiposDocumentos;
    }

    public void setTiposDocumentos(ArrayList<TipoDocumento> tiposDocumentos) {
        this.tiposDocumentos = tiposDocumentos;
    }

    public ArrayList<Destino> getDestinos() {
        return destinos;
    }

    public void setDestinos(ArrayList<Destino> destinos) {
        this.destinos = destinos;
    }

    public ArrayList<Motivo> getMotivos() {
        return motivos;
    }

    public void setMotivos(ArrayList<Motivo> motivos) {
        this.motivos = motivos;
    }

    public ArrayList<TipoServicio> getTiposServicios() {
        return tiposServicios;
    }

    public void setTiposServicios(ArrayList<TipoServicio> tiposServicios) {
        this.tiposServicios = tiposServicios;
    }

    public ArrayList<TipoResulServ> getTiposResulServ() {
        return tiposResulServ;
    }

    public void setTiposResulServ(ArrayList<TipoResulServ> tiposResulServ) {
        this.tiposResulServ = tiposResulServ;
    }

    public ArrayList<TiposMatIntCli> getTiposMatIntCli() {
        return tiposMatIntCli;
    }

    public void setTiposMatIntCli(ArrayList<TiposMatIntCli> tiposMatIntCli) {
        this.tiposMatIntCli = tiposMatIntCli;
    }

    public ArrayList<TipoResulValija> getTiposResultadosValijas() {
        return tiposResultadosValijas;
    }

    public void setTiposResultadosValijas(ArrayList<TipoResulValija> tiposResultadosValijas) {
        this.tiposResultadosValijas = tiposResultadosValijas;
    }

    public ArrayList<String> getDiasFestivosUnd() {
        return diasFestivosUnd;
    }

    public void setDiasFestivosUnd(ArrayList<String> diasFestivosUnd) {
        this.diasFestivosUnd = diasFestivosUnd;
    }

    public ArrayList<DiaFestivo> getDiasFestivosRush() {
        diasFestivosUnd = new ArrayList<String>();
        for (DiaFestivo diaFestivo : diasFestivosRush) {
            diasFestivosUnd.add(diaFestivo.getDiasFestivosUnd());
        }
        return diasFestivosRush;
    }

    public void saveDiasFestivosRush() {
        diasFestivosUnd = new ArrayList<String>();
        for (DiaFestivo diaFestivo : diasFestivosRush) {
            diasFestivosUnd.add(diaFestivo.getDiasFestivosUnd());
        }
    }

    public void setDiasFestivosRush() {
        diasFestivosRush = new ArrayList<DiaFestivo>();
        for (String cad : diasFestivosUnd) {
            DiaFestivo diasFestivo = new DiaFestivo();
            diasFestivo.setDiasFestivosUnd(cad);
            diasFestivosRush.add(diasFestivo);
        }
    }

    public void saveDestinoRush() {
        if (destinos != null) {
            for (Destino destino : destinos) {
                destino.setIdRush(destino.getCodDestino() + destino.getCodPostal());
            }
        }
    }

    public void saveMotivoRush() {
        if (motivos != null) {
            for (Motivo motivo : motivos) {
                motivo.setIdRush(motivo.getCodMotivo() + motivo.getNumTipPersona());
            }
        }
    }

    public void saveOcupacionRush() {
        if (ocupaciones != null) {
            for (Ocupacion ocupacion : ocupaciones) {
                ocupacion.setIdRush(ocupacion.getCodOcupacion() + ocupacion.getNumTipPersona());
            }
        }
    }

    public void saveProductoRush() {
        if (productos != null) {
            for (Producto producto : productos) {
                producto.setIdRush(producto.getCodProducto() + producto.getDescripcion());
            }
        }
    }

    public void saveResultadoLiquidacionRush() {
        if (resultadosLiquidacion != null) {
            for (ResultadoLiquidacion resultadoLiquidacion : resultadosLiquidacion) {
                resultadoLiquidacion.setIdRush(resultadoLiquidacion.getCodFamilia()
                        + resultadoLiquidacion.getDescripcion());
            }
        }
    }

    public void saveSituacionRush() {
        if (situaciones != null) {
            for (Situacion situacion : situaciones) {
                situacion.setIdRush(situacion.getCodSituacion() + situacion.getDescripcion());
            }
        }
    }

    public void saveTipoResulServRush() {
        if (tiposResulServ != null) {
            for (TipoResulServ tipoResulServ : tiposResulServ) {
                tipoResulServ.setIdRush(tipoResulServ.getCodResultadoServ() + tipoResulServ.getDescripcion());
            }
        }
    }

    /**
     * Devuelve los festivos de la unidad, o lista vacía si no hay.
     *
     * @return
     * @throws ParseException
     */
    public List<Calendar> getFestivosUnidad() throws ParseException {
        List<Calendar> calendario = new ArrayList<Calendar>();
        if (diasFestivosUnd != null && !diasFestivosUnd.isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            for (String fechaString : diasFestivosUnd) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(sdf.parse(fechaString));
                calendario.add(cal);
            }
        }
        return calendario;
    }
}
