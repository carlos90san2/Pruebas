package com.example.carlos.pruebas.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Crendo por Carlos Sanchidrián Sánchez en 10/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class ContactsModel {

    private List<Contact> contactList;
    private static ContactsModel contactsModel;

    private ContactsModel() {
        contactList = new ArrayList<Contact>();
        for(int i=0; i<20; i++){
            Contact contact = new Contact();
            contact.setName("Nombre " +(i+1));
            contact.setSurname("Apellido " +(i+1));
            contact.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.");
            contact.setPhone("666666666");
            contact.setImage("http://i.imgur.com/DvpvklR.png");
            contactList.add(contact);
        }
    }

    public static ContactsModel createContacts() {
        if (contactsModel == null)
            contactsModel = new ContactsModel();
        return contactsModel;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }
}
