package com.example.carlos.pruebas.ui.PruebaOrdenarObjetos;

import java.util.Comparator;

/**
 * Creado por Carlos Sanchidrián Sánchez en 10/10/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class Objeto1 implements Comparable<Objeto1>{

    private int id;

    public Objeto1(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
//
//    @Override
//    public int compare(Objeto1 o1, Objeto1 o2) {
//        return (o1.id < o2.id) ? o1.id : o2.id;
//    }

    @Override
    public int compareTo(Objeto1 o1) {
        return this.id - o1.getId();
    }
}
