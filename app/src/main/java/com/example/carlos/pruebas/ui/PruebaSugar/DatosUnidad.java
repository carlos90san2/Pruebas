package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.annotations.RushIgnore;
import co.uk.rushorm.core.annotations.RushList;

public class DatosUnidad extends RushObject {

    private String codUnidad;
    private String nombre;
    private String bolPapel;
    private String forzarLista;

    @RushList(classType = UnidadLista.class)
    private List<UnidadLista> unidadesLista;

    @SerializedName("grupoSeccion")
    @RushIgnore
    private ArrayList<ArrayList<GrupoSeccion>> listaGruposSecciones;

    @RushList(classType = GrupoSeccionRush.class)
    private transient ArrayList<GrupoSeccionRush> listaGruposSeccionesRush;

    @SerializedName("seccion")
    @RushList(classType = Seccion.class)
    private ArrayList<Seccion> secciones;

    @SerializedName("turno")
    @RushList(classType = Turno.class)
    private ArrayList<Turno> turnosUnidades;

    private int modAsignacion;

    public DatosUnidad() {
    }

    private ResultadoJson resultado;

    public String getForzarLista() {
        return forzarLista;
    }

    public void setForzarLista(String forzarLista) {
        this.forzarLista = forzarLista;
    }

    public String getCodUnidad() {
        return codUnidad;
    }

    public void setCodUnidad(String codUnidad) {
        this.codUnidad = codUnidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBolPapel() {
        return bolPapel;
    }

    public void setBolPapel(String bolPapel) {
        this.bolPapel = bolPapel;
    }

    public List<UnidadLista> getUnidadesLista() {
        return unidadesLista;
    }

    public void setUnidadesLista(List<UnidadLista> unidadesLista) {
        this.unidadesLista = unidadesLista;
    }

    public ArrayList<Seccion> getSecciones() {
        return secciones;
    }

    public void setSecciones(ArrayList<Seccion> secciones) {
        this.secciones = secciones;
    }

    public void saveSecciones() {
        if(secciones != null) {
            for (Seccion seccion : secciones) {
                seccion.setIdRush(seccion.getCodSeccion() + seccion.getCodCarteroSeccion());
            }
        }
    }

    public ArrayList<Turno> getTurnosUnidades() {
        return turnosUnidades;
    }

    public void setTurnosUnidades(ArrayList<Turno> turnosUnidades) {
        this.turnosUnidades = turnosUnidades;
    }

    public ResultadoJson getResultado() {
        return resultado;
    }

    public void setResultado(ResultadoJson resultado) {
        this.resultado = resultado;
    }

    public ArrayList<ArrayList<GrupoSeccion>> getListaGruposSecciones() {
        return listaGruposSecciones;
    }

    public ArrayList<GrupoSeccionRush> getListaGruposSeccionesRush() {
        listaGruposSecciones = new ArrayList<ArrayList<GrupoSeccion>>();
        for(GrupoSeccionRush grupoSeccionRush : listaGruposSeccionesRush){
            listaGruposSecciones.add(grupoSeccionRush.getGrupoSeccion());
        }
        return listaGruposSeccionesRush;
    }

    public void saveListaGruposSeccionesRush() {
        listaGruposSecciones = new ArrayList<ArrayList<GrupoSeccion>>();
        for(GrupoSeccionRush grupoSeccionRush : listaGruposSeccionesRush){
            listaGruposSecciones.add(grupoSeccionRush.getGrupoSeccion());
        }
    }

    public void setListaGruposSeccionesRush() {
        //this.listaGruposSeccionesRush = listaGruposSeccionesRush;
        this.listaGruposSeccionesRush = new ArrayList<GrupoSeccionRush>();
        for (ArrayList<GrupoSeccion> list : listaGruposSecciones) {
            this.listaGruposSeccionesRush.add(new GrupoSeccionRush(list));
        }
    }

    // Cuando se convierte de JSON a objecto (GSON) hay que rellenar el atributo listaGruposSeccionesRush
    // para que se guarde en la base de datos
    public void setListaGruposSecciones(ArrayList<ArrayList<GrupoSeccion>> listaGruposSecciones) {
        this.listaGruposSecciones = listaGruposSecciones;
    }

    public int getModAsignacion() {
        return modAsignacion;
    }

    public void setModAsignacion(int modAsignacion) {
        this.modAsignacion = modAsignacion;
    }

}




