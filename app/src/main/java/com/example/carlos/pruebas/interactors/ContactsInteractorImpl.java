package com.example.carlos.pruebas.interactors;

import android.content.Context;

import com.example.carlos.pruebas.enums.DataSourceEnum;
import com.example.carlos.pruebas.model.Contact;
import com.example.carlos.pruebas.model.ContactsModel;
import com.example.carlos.pruebas.model.dataStrategy.DataDB;
import com.example.carlos.pruebas.model.dataStrategy.DataManager;
import com.example.carlos.pruebas.model.dataStrategy.DataMock;
import com.example.carlos.pruebas.model.dataStrategy.DataStrategy;
import com.example.carlos.pruebas.model.dataStrategy.DataWS;

import java.util.ArrayList;
import java.util.List;

import co.uk.rushorm.core.RushSearch;

/**
 * Creado por Carlos Sanchidrián Sánchez en 08/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class ContactsInteractorImpl implements ContactsInteractor, DataStrategy.onGetContactsListener{

    private Context context;
    private ContactsInteractor.contactLoadedFinishedListener contactsListenerGet;
    private ContactsInteractor.contactReloadFinishedListener contactsListenerReload;
    private DataStrategy dataStrategyGetContact;
    private static List<Contact> contacts;

    public ContactsInteractorImpl(Context context, ContactsInteractor.contactLoadedFinishedListener contactsListenerGet, ContactsInteractor.contactReloadFinishedListener contactsListenerReload){
        this.context = context;
        this.contactsListenerGet = contactsListenerGet;
        this.contactsListenerReload = contactsListenerReload;
    }

    @Override
    public void getContacts() {
        if(contacts == null){
            dataStrategyGetContact = DataManager.getDefaultDataStrategy();
            dataStrategyGetContact.setOnGetContactsListener(this);
            if(dataStrategyGetContact instanceof DataMock){
                ((DataMock) dataStrategyGetContact).getContacts(context);
            }
            else{
                ((DataWS)dataStrategyGetContact).getContacts(context);
            }
        }
        else{
            dataStrategyGetContact = DataManager.getCustomDataStrategy(DataSourceEnum.DATA_DB);
            dataStrategyGetContact.setOnGetContactsListener(this);
            ((DataDB)dataStrategyGetContact).getContacts();
        }
    }

    @Override
    public void addContact() {
        Contact contact = new Contact();
        contact.setName("Nombre " +(contacts.size()+1));
        contact.setSurname("Apellido " +(contacts.size()+1));
        contact.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.");
        contact.setPhone("666666666");
        contact.setImage("http://i.imgur.com/DvpvklR.png");
        contacts.add(contact);
        contactsListenerReload.contactReloadOk(contacts);
    }

    @Override
    public void deleteContact(int position) {
        try{
            contacts.remove(position);
            contactsListenerReload.contactReloadOk(contacts);
        }catch(Exception e){
            contactsListenerReload.contactReloadError("Error al eliminar el contacto");
        }
    }

    @Override
    public void onGetContactsListener(List<Contact> contactList) {
        contacts = contactList;

        //Comprueba si los datos vienen de una petición web
        if(contactList != null && contactList.size() > 0 && dataStrategyGetContact instanceof DataWS) {
            dataStrategyGetContact = DataManager.getCustomDataStrategy(DataSourceEnum.DATA_DB);
            dataStrategyGetContact.setOnGetContactsListener(this);

            ((DataDB) dataStrategyGetContact).setContacts(contactList);
            //Establece WS para que no vuelva a insertar los datos en la DB
            dataStrategyGetContact = DataManager.getCustomDataStrategy(DataSourceEnum.DATA_WS);

        }
        contactsListenerGet.contactLoadedOk(contacts);
    }

    @Override
    public void onGetContactsListenerError(String error) {
        contactsListenerGet.contactLoadedError("Error al cargar los contactos");
    }
}
