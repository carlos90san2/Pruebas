package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SituacionWrapper {

  @SerializedName("situacion")
  private List<Situacion> situaciones;

  public List<Situacion> getSituaciones() {
    return situaciones;
  }

  public void setSituaciones(List<Situacion> situaciones) {
    this.situaciones = situaciones;
  }
}
