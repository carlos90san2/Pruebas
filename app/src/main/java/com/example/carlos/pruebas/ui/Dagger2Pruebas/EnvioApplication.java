package com.example.carlos.pruebas.ui.Dagger2Pruebas;

import com.example.carlos.pruebas.ui.PruebaSugar.Aplicacion;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class EnvioApplication extends Aplicacion {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .envioModule(new EnvioModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
