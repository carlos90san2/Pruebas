package com.example.carlos.pruebas.presenters;

import android.content.Context;

import com.example.carlos.pruebas.interactors.ContactsInteractorImpl;
import com.example.carlos.pruebas.interactors.ContactsInteractor;
import com.example.carlos.pruebas.interfacesMVP.ContactsMVP;
import com.example.carlos.pruebas.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by X70826SA on 05/05/2017.
 */

public class ContactsPresenter implements ContactsMVP.Presenter, ContactsInteractor.contactLoadedFinishedListener, ContactsInteractor.contactReloadFinishedListener{

    private Context context;
    private ContactsMVP.View contactsView;
    private ArrayList<Contact> contacts;
    private ContactsInteractor contactsInteractor;

    public ContactsPresenter(ContactsMVP.View contactsView, Context context){
        this.contactsView = contactsView;
        this.context = context;
        contactsInteractor = new ContactsInteractorImpl(context, this, this);
    }

    @Override
    public void getContacts() {
        contactsInteractor.getContacts();
    }

    @Override
    public void addContact() {
        contactsInteractor.addContact();
    }

    @Override
    public void deleteContact(int position) {
        contactsInteractor.deleteContact(position);
    }

    @Override
    public void contactLoadedOk(List<Contact> contactList) {
        contactsView.printContactsList(contactList);
    }

    @Override
    public void contactLoadedError(String error) {
        contactsView.printContactsError(error);
    }

    @Override
    public void contactReloadOk(List<Contact> contactList) {
        contactsView.updateAdapter(contactList);
    }

    @Override
    public void contactReloadError(String error) {
        contactsView.printContactsError(error);
    }
}
