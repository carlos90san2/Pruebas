package com.example.carlos.pruebas.ui.PruebaSugar;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Creado por Carlos Sanchidrián Sánchez en 06/07/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class ObjectParameterizedType implements ParameterizedType {

    private Type type;

    public ObjectParameterizedType(Type type) {
        this.type = type;
    }

    @Override
    public Type[] getActualTypeArguments() {
        return new Type[] {type};
    }

    @Override
    public Type getRawType() {
        return type;
    }

    @Override
    public Type getOwnerType() {
        return null;
    }

    // implement equals method too! (as per javadoc)
}
