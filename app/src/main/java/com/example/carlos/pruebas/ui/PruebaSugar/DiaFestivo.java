package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

/**
 * Creado por Carlos Sanchidrián Sánchez en 21/09/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class DiaFestivo extends RushObject{

    private String diasFestivosUnd;

    public DiaFestivo() {
    }

    public String getDiasFestivosUnd() {
        return diasFestivosUnd;
    }

    public void setDiasFestivosUnd(String diasFestivosUnd) {
        this.diasFestivosUnd = diasFestivosUnd;
    }
}
