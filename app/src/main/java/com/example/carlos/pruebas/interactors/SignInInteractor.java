package com.example.carlos.pruebas.interactors;

import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by X70826SA on 09/05/2017.
 */

public interface SignInInteractor {

    void setDataConnect();
    void requestConnect();
    void checkResultConnect(int requestCode, Intent data);
    void requestDisconnect();

    interface responseRequest{
        void errorLogin(String error);
        void loginSuccess(GoogleSignInAccount acct );
        void signOutSuccess(String message);
    }
}
