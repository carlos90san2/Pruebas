package com.example.carlos.pruebas.ui.ConstraintLayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.carlos.pruebas.R;

public class ConstraintLayout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constraint_layout);
    }
}
