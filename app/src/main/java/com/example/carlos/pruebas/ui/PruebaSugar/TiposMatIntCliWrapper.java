package com.example.carlos.pruebas.ui.PruebaSugar;

import java.util.List;

public class TiposMatIntCliWrapper {

  private List<TiposMatIntCli> tiposMatIntCli;

  public List<TiposMatIntCli> getTiposMatIntCli() {
    return tiposMatIntCli;
  }

  public void setTiposMatIntCli(List<TiposMatIntCli> tiposMatIntCli) {
    this.tiposMatIntCli = tiposMatIntCli;
  }

}
