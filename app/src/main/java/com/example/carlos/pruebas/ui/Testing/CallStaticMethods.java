package com.example.carlos.pruebas.ui.Testing;

/**
 * Creado por Carlos Sanchidrián Sánchez en 19/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class CallStaticMethods {

    public String extendGreeting(String greeting){
        return greeting + ", " + StaticMethods.getGreeting();
    }
}
