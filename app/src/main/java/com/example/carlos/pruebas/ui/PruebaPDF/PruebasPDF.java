package com.example.carlos.pruebas.ui.PruebaPDF;

import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;

import com.example.carlos.pruebas.R;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class PruebasPDF extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pruebas_pdf);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        String a = "JVBERi0xLjQKJeLjz9MKMyAwIG9iaiA8PC9UeXBlL1hPYmplY3QvQ29sb3JTcGFjZS9EZXZpY2VS\n" +
                "R0IvU3VidHlwZS9JbWFnZS9CaXRzUGVyQ29tcG9uZW50IDgvV2lkdGggNjQwL0xlbmd0aCAxMDg4\n" +
                "OS9IZWlnaHQgNDgwL0ZpbHRlci9EQ1REZWNvZGU+PnN0cmVhbQr/2P/gABBKRklGAAEBAAABAAEA\n" +
                "AP/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\n" +
                "AQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\n" +
                "AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAeACgAMBIgACEQEDEQH/xAAfAAABBQEBAQEB\n" +
                "AQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEH\n" +
                "InEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFla\n" +
                "Y2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbH\n" +
                "yMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQID\n" +
                "BAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJ\n" +
                "IzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1\n" +
                "dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY\n" +
                "2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP7+KKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\n" +
                "ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\n" +
                "KKKiLeWhklIGBz7DPAHP8u9AH5YeEP2l9V+M3/BV/wAa/AL4e6n4gf4c/sYfsqeIdP8A2ibZtY1R\n" +
                "PDGp/Hv9qPxt8BPHnwM0w6A0Ai1fX/hp8H/hR8VZY/FYkkQR/FzxD4NtGjuvDfjNB+qlfjh/wR18\n" +
                "O2ni34K/G79uS+h1Vtf/AOCh37TXxi/aN0X/AISKxjsdX0X9nOPxprvgf9lXwkoEg/4kK/BbQdA+\n" +
                "JcDGJdt38Q/EZdHVlFfsfQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\n" +
                "FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU\n" +
                "UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABX5\n" +
                "pf8ABWr4oeKfhV/wT6/aJ/4Vp9pX4u/GTw7o/wCzL8FLOyvk07Vh8Yf2q/GWhfAPwHrGiE5I1vwx\n" +
                "4i+J0XipdpLOdBk3YXNfpbX5CftvaTH8dv27v+CXv7NUcOnah4Y8EfE34vf8FCfijbSa0iajpGlf\n" +
                "sqeDY/h38JGbRzv81p/2if2j/AHiG2Rs/aB8PfELqw/4RoKgB+kvwY+E/hT4GfCH4U/BPwPbvD4J\n" +
                "+Dnw48CfDHwfa3JG+y8LfD/wlo/gjw/FkHrHoWjRq2CQXMmeSMerUUUAFFFFABRRRQAUUUUAFFFF\n" +
                "ABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\n" +
                "FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU\n" +
                "UUUAFFFFABRRRQAUUUUAFFFFABRRRQAV+Mf7GHifTf2j/wDgpR/wU4/aYh/snW/CvwEvPg5/wTf+\n" +
                "FPiG12m7stT+BkfiL4wftKaNLuwv9tH4y/G3RvDU/IZovAPhsDJav0P/AGnfjhF+zp8Avi38abjR\n" +
                "pPEmo/D7wTrOseHPCdnGyan478dFRovgD4a6O/mOyeIPih491bQfBnhFShWW/wDEEKN5ed1fBn/B\n" +
                "Ef4R33wl/YWt4vFGqDxH8Q/Gv7SH7WHiX4leODt/4uZ4l8PftC/ED4TaF8S/m4/4rD4dfC/wR4iy\n" +
                "OR5u4dKAP2AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\n" +
                "oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii\n" +
                "igAooooAKKKKACiiigAooooAKKKKACiiigD8xP20/j7/AMFDPg14r8JaR+x5+wBp/wC1p4T8UaAf\n" +
                "+El+J8P7TXwi+HGv/CfxRb6u8FtFqXwQ+LX/AAq6Dx/4evNDkiu4G8OfGbwrc3Mx8QWV0/gxbWDx\n" +
                "P4l+NYP21v23NC2WP7Qfx9+GX7Kuoz5+2QfFf/giz+2S3hjROmFf9oj4f/8ABRz4t/sysUAHzv45\n" +
                "l3bsGFSN9f0C0UAfiN8O/ido3x6uCNC/4L3aBrmtRBIbzQv2VLT/AIJwaJa2OrTbSdF1XQPi38IP\n" +
                "2rvFWiTorLt0G78VN4ngkZzMZTDJEv1PD+x/8e72BTd/8FTv2+tTsrgP5kcPg7/gmtpi3tkFWZNm\n" +
                "q+Hv+Cc/hzxDAsg/1b6DfRyLuAEzB2avrHx/8Cvgn8WoDD8W/g78L/ilERxbfEL4deD/ABoh9wni\n" +
                "Lw/cqCOnIx9DXyxcf8Etf+CflvJJP4T/AGVPhj8I7yYLcXd3+z1aaz+zfqkrDjJ1n4Ca38L7ksMY\n" +
                "y825sF3ByTQAq/8ABPTwZq43/Ef9pr9vv4izAYFx/wANu/Hv4LyLj7pI/Ze8T/s+wuAcZRonQ/dd\n" +
                "SpILv+HbH7Ov/RRf2/8A/wAWx/8ABUn/AOjOqD/h3X4B0kBvAH7SH7fHw9YRERi3/bj/AGlfiTp1\n" +
                "sV5yNK+P3xG+LcS7sElVj27jn5VOBF/wyT+0voO1vCH/AAUv/a2aKDiDRfiR8N/2H/iF4eDJNHGN\n" +
                "zaJ+yZ4N+IsilXMkqnxzhkRo7Y+HFwxALI/4Jn/sp3JVPEcP7SHxHUJj7F8W/wBuv9uL41aYCO50\n" +
                "r4uftIeL7dQB0VY8DpxSn/glR/wTQn3T61+wB+yJ42vtmRq3xI/Z/wDhp8SNXY5HyHXPiD4b8V+I\n" +
                "ByMkGdlOBx0qBfhT/wAFLdBijPh39s/9lrxjbxqrGD4r/sOeNbnV79iAWjj134UftpfCjQdEjB3K\n" +
                "zv4K8VSEBWUAFgF/tD/gqXoEbEeEf2B/iqWU8f8ACxv2kPgHLeGBcjbn4Z/tHDRm8SEYXP2hfBUn\n" +
                "fxqegBZ/4dSf8EtP+kav7Av/AIhp+z9/87el/wCHTv8AwS0/6Rp/sCf+Ib/s4f8Azuapf8Lv/wCC\n" +
                "hOhEL4q/YJ+HfiKOIt9qT4Jftn6R4zLEv839mSfGL4Ifs65PRLbzBF5oB84eGgSKef2wv2kbZTDf\n" +
                "/wDBKr9t24vIVdjL4d+J/wDwTX1LSiETbnTJtf8A2+vCfiFwBnmTwrCxwFRXySoBZ/4dSf8ABLT/\n" +
                "AKRq/sC/+Iafs/f/ADt6P+HUn/BLT/pGr+wL/wCIafs/f/O3oP7Zf7QvJk/4JRft+eVxjd4//wCC\n" +
                "XpA57j/h5EePegft8/2WP+Kx/Yu/b98FD/b/AGcJPiX/AOqC8T/F3/IoA+EfjN/wTZ/4Ju+N/wBr\n" +
                "n4A/Arwr+wL+xH4b8O/Cnw14o/ax/aMvvDv7LHwI0KS98JpH4h+EfwN+FfjU6V8O8p4d+LHj3WvH\n" +
                "nxIt4Z2+y+J0/Zj8RWgieCHypNH/AIJx/wDBMv8A4J1+Pf2Cv2QviH8Q/wDgn1+xZ4w8cfEz9nz4\n" +
                "XfFXxF4g8XfspfATxH4n1C/+KHhy38dk6rrOufDlppTFHrsduqnYYREsaKqhDL8sav8A8FP/ANnH\n" +
                "wj8Hf+Cnv7VHjLUvjd4H8W/GOy+KXgv4HXnxD/Zf/at+H3hy78Efs6fB3XfhZ8KfB+iePPEnwbi+\n" +
                "GSp4l+N0Hxw8UGA+JlbwV4v+I1v4H8ZRw+NI2hb3f4Z/8F1v+CRfwD+D3wA+CHhv9rLwl8RfE/hn\n" +
                "4U/DP4d+EPCnw60vV9ROuf8ACD+C9A0BdLHjXxFb+FfhhoU6KiBf+Ew8ZeEEGWG0gYYA/Q3/AIdS\n" +
                "f8EtP+kav7Av/iGn7P3/AM7eoX/4JY/8E/NLHmeB/wBlf4c/BacL811+zOuufsu6mW/vf2v+z3r3\n" +
                "wpnYnuDKDkn1JryPwH+2r+0l+1BJawfs0/Cj9mvwDoWqMjWXiv8AaA/as+G3xK8bCExLqv8AaOj/\n" +
                "AAV/Y/174weGPEsf9hu11iT9pPwbKpIkci3ic16x/wAMefGT4not5+1P+2T8aPHlncr5l58Lv2b4\n" +
                "v+GMfg4r+W5ZreX4e+IPFX7T7oWkkibQPFf7WvizwpKPmayIdVUA+afjJoHwW/Zh12D4b+E/+Cl/\n" +
                "7fPwt8ea5Y/2jo/7Ofw28a6J+378fNasMyqNd0PwN+05+zr+3L+0y/h5ZY28zxFDcL4RiUfvJIvM\n" +
                "iY/dP7HXiH47eI/2f/Bmq/tFaL4h0X4p3HiH4naNP/wlumeE9A8a+Ifh54Y+Kfjrw/8ABLxv450H\n" +
                "wJM3hHw98Svil8FtH8B/Ejx94a8J2vhXw14X8Y+JfENpD4O8Ex2v/CIWnovwX/Z/+CP7Ofh7UvC/\n" +
                "wO+E3gH4U6Jqlz/bWs23grwzpXh4+INWKuw1jxdq0UR1/wAS+IQomVvEniWW6vJI2VHlZkVD7tQA\n" +
                "UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABR\n" +
                "RRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFF\n" +
                "FABRRRQAUUUUAFFFFABRRRQAVwfizxb4W8DeFfEPjbxv4j0Xwf4Q8HaHqnibxT4r8T6xpmgeF/CH\n" +
                "hzRNOn1zWte17WdX8rQNG0Dw3oqmfUPEEswtre2jmkvJ4khldu8ooA/BPW/+Dir/AIJxXOu6novw\n" +
                "h8d2X7QFhoFzqOna34n8O/Hf9iP4B6RHq+k7w9vokP7cP7WH7JXiXx/BJFGzx+KvhZ4X8aeCWZCF\n" +
                "8XARANz5/wCC2HxB+IciH4D/ALA/x8v9AbaH8afEDwF+1V458NS71O/UNF8Q/wDBOb9kf/gon8Mf\n" +
                "EkLMcARfFq2dgoJKnfn+gyigD+fY/tZf8FYfjNi2+Avwi0HT2uFbz7nxV+w9+0h8OPDfh/8A1kbn\n" +
                "W/H37bv7Rf7Dni1lCXAQzeDP2R/i/IGSGQ+D2DXA8Lxn9m3/AILo/Ejyf+Fiftg/D74VwX+w3Oh/\n" +
                "DL4jeDfEXhuEsBlJo9F/4Jy/B74owExys7t4b/aKV08lrdHWeePxX4X/AKDaKAPyY+Dn7Fv7bXhn\n" +
                "wnH4O+IP/BSf4h2+m3ut6hr17qPwc+G2k33ji7j1na7aJcfFD9sTxV+2zP8A8I+QHiWPwf4P+E4i\n" +
                "l2N4Mg8HKDC3n37Yf7InwY+DH7NXxb+K2ueMv2tvj38ULbw0NF+G+ifFX9t39q7UfC3j347ePdZ0\n" +
                "XwJ8B/CEvwp8OfGbwr8ELceLvjL4w8EeEoz4c+FEMYl1zyhF5pEcn7UV+d3xuaT48fthfs+/s7ac\n" +
                "EvPBn7PLad+2X8dPJkLqdVil8R+Af2VvAmtRgxop8VfEWP4ofHFGLPJDdfszeH4mXyvFChwD4B/a\n" +
                "q/4Jd/sY/sv/APBJb9sDTPAP7OHwT1r4tfDr/gmv+0b4YtfjXqfws8G6p8Xdf8VeH/2avHaS+Lbj\n" +
                "xvrVpc+KI/EHiTxCj+I5ZYb1SLh5UKj9yY/30uYNK8QaYba6h0/V9H1O2y9vPbLqGn3tg6ggMjq0\n" +
                "UgZWUjcGGSMLuAI4H42fD2H4t/Bn4tfC2cqtv8Tvhh48+Ht19oOAsPjXwjrfh9wx6DA1gh+wXIHS\n" +
                "vG/2C/H118VP2IP2N/idfgNqPjz9l74EeLNYef5WTVtf+F/hzVtazyRuXWZHDscgY3DBxQBL48/Y\n" +
                "U/Yh+KYn/wCFlfsb/srfEhriQC5/4Tn9n34Q+MTeNv8A7RUFvEHhe5D/AL4mRTJnLlZCNg3jzA/8\n" +
                "Et/2DrEL/wAIr+z1onww2YFs3wT8W/Ef4JNbEAcx/wDCn/FXg1UJJJO4FfT1r9B6KAPzzg/4J7eE\n" +
                "/DKGb4OftQft5fBjW8Kg1u1/bC+Mf7QFlbLhiG0zwN+27rP7V3wuiPJ+VPBZQZPcmkk+G3/BSXwM\n" +
                "iJ4P/ar/AGbfjFpFpiRrH48/sseLPDnjq9Rhgq/j/wCAvxw8JeEUCkjKxfs7hs4KMRur9DaKAPzx\n" +
                "X4j/APBTDwe8lx4l/ZZ/ZT+Lui2YBvLr4Pftb/ETwV8Rr9drFk8O/C74ufs2x/DmSbG1o/8AhJ/2\n" +
                "jfC0bLvBkSRUR54P+CjfwS8JGGz/AGlvBfxs/Yz1FQ5urz9qL4cv4a+GFgBGJFNz+1D8Ptd+Kn7I\n" +
                "aO4Kjyk/aFknUuu5AcgfoPVGa3guIZ4ZohPBcA/abe4wQVIGRtOQVwAcZI4GCMUAYfh7XtC8XaPY\n" +
                "eIPC+uaT4j8Pa5ai70jX9D1bTdS0rXNNkAA1TSNY0R5YbmMltvnQStEXGEdeHrqq+C9d/wCCdv7O\n" +
                "Mesaz4z+DuleMf2TfiBrt1/a+reNP2SfGOsfBFNd1cKx/t3x18L/AA6knwO+LuutgEzfFv4SeOSM\n" +
                "ldpcDdF4P+Kfxd/Z/wDF2g/C39qrxLovxB8F+L9a0Xwd8KP2r7HRdJ8FDxP438Q6tNofh34YftDe\n" +
                "CNBt4/DXgX4j+JZm0PQPBHxK8HvbfCX4weMZ38GQ+CPgx47ufAngTx6AffNFFFABRRRQAUUUUAFF\n" +
                "FFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUU\n" +
                "UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQ\n" +
                "AUUUUAFFFFAHParq+l6Fpt9reu3lrpukabZ32q6vqd3c/ZLGx0/SFaWfUZWLFVjSJUkdtwGwA5Yq\n" +
                "qt8P/wDBP+x1LxR8LfE/7UfimyuLbx3+2h421H9oye21W3jsNQ8O/CLXdJ0bw9+zb4IW3Ued4ffw\n" +
                "r+zl4e+Fo8b+G3Z4x8aNY+KPjOMI/i6cyVP2+b648eeBfAX7H+h3E1vr/wC2b45HwY8S/YvMV9C/\n" +
                "Z10TTLnxv+1TrsriNE0U3PwV0TxB8MfCPiLYhs/iD8Vvhs5STz0kX77tLG3sbaGzsYoLSwtrZbW0\n" +
                "tbeAKtoqrjaoBICgY+XauSAWJJLEA0a/Or/gnFC3hr4D+I/gPqkI03xd+zH8dfjt8F9W8PXW2PU9\n" +
                "I8I6L8ZPHPiH4G6tMkis39h+MP2d/F3wo+JHhEPEFNl4iiVZC8TrH+itfnf8foV+Af7UfwG/aV0x\n" +
                "Tp3gj4ya54W/ZB/aNCySR6U58ca3rEv7KnxJ1t1MTNrnhX44ayvwG8JR7mhuh+0863HmHwt4UiUA\n" +
                "/RCiiigAooooAKKKKACvzx/4KmCOX/gn9+1BpWnbh468U/D1fBfwVZVL6gf2nfHes6N4H/ZXGk4d\n" +
                "SuuH9ojXPhbHaudwjuWhZklUNG/0f8av2h/gx+zroeja58YPiFpfhBvFF1/Y3hHw+i6p4g8cfEPx\n" +
                "RAkY/wCER+F/ww8Mw+J/iN8RfFU3njZ4Q+GPhPxV4rkjJlis5EhaQ+A+DvDPjb9qD4jfD743/GPw\n" +
                "b4v+G/wq+FGsX/ij9n/4EeM9P0/TvGuu+OJ9Kl0C2/aC+OWhqZToOueF9D1jXY/g38LPNlHgmHxC\n" +
                "3j74jRL8av8AhD/BvwWAPv2iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigA\n" +
                "ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACi\n" +
                "iigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiuU8SeJtE8GeHfEHi/xRqNro/hrw\n" +
                "tpGpeIvEeq3TBNP0bR9F0yfWNX1Z32ljFBAkk0hw74RnVWIYEA+HvgUp+MX7aP7T3xxuGnu/C/wA\n" +
                "t/D/AOxt8HnZlbTRqg07QPi/+1X4t0F1VVEfi34ha/8ACv4WeMIsFl8V/sv7GbMbOf0Mr4I/4Jve\n" +
                "G9b0L9jL4Ia34p0240Txl8Y7LxV+03470XUWVdR0Tx7+1d8R/En7S3jbQNbQIC+teF/EfxSuPDkj\n" +
                "O24yW527o0wv3vQAVxPi7wd4U+IfhXxB4H8beHNI8U+DfFuj6hofiHw7r9mmp6Vrmk62rLrOk6rp\n" +
                "Ei+XKksRKsH3lS+QY5Fy3bUUAfnF/wAJd8X/ANjoHTviRL43/aE/ZPhuWXSvjBpGm6r4z+O/7Pem\n" +
                "rKYm0n43aHo0UviX4yfDTw5FDLEfj54Yhuvi/wCF4Y7e4+Nvg3xgkXjD44SfcHg7xl4R+IfhPQPG\n" +
                "/gDxb4e8b+B/Fej2GseFfF/hPVdM8R+Gdf0nWQo0fVtB1zQ530TXNEkR2a3lgklR1VZFkYOm7va+\n" +
                "CvFP7Dfge68Ua742+CHxd+P37I/ibxdqx8QeLD+zt4r8H6f4G17xdq83n6z421j4HfGD4c/Fv4Cv\n" +
                "8QfE8gifxf4wX4Tx+L/GLCF/Gd5dFllUA+9aK+A/+GOf2iP+krn7fX/huv8Agl1/9LbrNb9guXxi\n" +
                "Vi/aA/bB/bb/AGktHtUL2HhfxV8WfBfwA8MiZmJca7ov7D/wv/ZJXx/azBAJPDnxUbxp4VKsCLPI\n" +
                "3KAe9fGL9qb9nr9n5NNtvi/8YPCPhTX9UXZ4X8Amd/Efxa8e6nEI1GkfDD4UeHB4j+J3xH8QuA2z\n" +
                "wx4K8JeKvFMok3JblF3H5/Hi/wDbS/aSVl+H3hKT9iX4SXDmIfEz4qaTofjb9qrxZpLEBrzwF8FJ\n" +
                "k8R/Cz4MF7ZhP4U8VfHuT4veLlk82Dxz+yx4VuUUy/QPwZ/ZZ/Zq/Z4e/b4Dfs//AAd+El3flf7Y\n" +
                "1v4f/Dnwj4W8Q+IMIzH+3tc0e0i8Q+IdWUHc1z4hmuZGGAThcV9J0AfKXwT/AGTPg58Edb1Pxzou\n" +
                "la74z+L/AIos0s/F37QHxV1/UviP8cPFlmTk6Xq/j7xIJvEWheFjKY7m3+FnhJfCfwf8Fzbj4F8D\n" +
                "+EkJtl+raKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKA\n" +
                "CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK\n" +
                "KKKACiiigAooooAKKKKACiiigAooooAKKKKACvzs/wCCh88ni34R+Av2W7edReftufHjwR+y7quS\n" +
                "DDd/CvVdG8a/F79pHRpjIqqj+KP2Vvgp8cPDNoybQPFOswMCu0iv0Try7xZ8LvBfjrxB8KfFfinR\n" +
                "X1XX/gr471H4l/DO7a/1S1fwz401f4YePvhHqetIsMsUOqTTfDv4pePPDqjXoJYNmv3B2RztDJCA\n" +
                "elQxQwRiGKIQww8Io4UAAkkck457nmp6KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooo\n" +
                "oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiig\n" +
                "AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAC\n" +
                "iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKK\n" +
                "KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooo\n" +
                "oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiig\n" +
                "AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAC\n" +
                "iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKK\n" +
                "KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooo\n" +
                "oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//2QplbmRzdHJlYW0KZW5kb2Jq\n" +
                "CjQgMCBvYmogPDwvVHlwZS9YT2JqZWN0L0NvbG9yU3BhY2VbL0luZGV4ZWQvRGV2aWNlUkdCIDI1\n" +
                "NSgAAACAAAAAgACAgAAAAICAAIAAgICAgIDAwMD/AAAA/wD//wAAAP//AP8A//////8AAAAAAAAA\n" +
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n" +
                "AAAAAAAAAAAAAAAAADMAAGYAAJkAAMwAAP8AMwAAMzMAM2YAM5kAM8wAM/8AZgAAZjMAZmYAZpkA\n" +
                "ZswAZv8AmQAAmTMAmWYAmZkAmcwAmf8AzAAAzDMAzGYAzJkAzMwAzP8A/wAA/zMA/2YA/5kA/8wA\n" +
                "//8zAAAzADMzAGYzAJkzAMwzAP8zMwAzMzMzM2YzM5kzM8wzM/8zZgAzZjMzZmYzZpkzZswzZv8z\n" +
                "mQAzmTMzmWYzmZkzmcwzmf8zzAAzzDMzzGYzzJkzzMwzzP8z/wAz/zMz/2Yz/5kz/8wz//9mAABm\n" +
                "ADNmAGZmAJlmAMxmAP9mMwBmMzNmM2ZmM5lmM8xmM/9mZgBmZjNmZmZmZplmZsxmZv9mmQBmmTNm\n" +
                "mWZmmZlmmcxmmf9mzABmzDNmzGZmzJlmzMxmzP9m/wBm/zNm/2Zm/5lm/8xm//+ZAACZADOZAGaZ\n" +
                "AJmZAMyZAP+ZMwCZMzOZM2aZM5mZM8yZM/+ZZgCZZjOZZmaZZpmZZsyZZv+ZmQCZmTOZmWaZmZmZ\n" +
                "mcyZmf+ZzACZzDOZzGaZzJmZzMyZzP+Z/wCZ/zOZ/2aZ/5mZ/8yZ///MAADMADPMAGbMAJnMAMzM\n" +
                "AP/MMwDMMzPMM2bMM5nMM8zMM//MZgDMZjPMZmbMZpnMZszMZv/MmQDMmTPMmWbMmZnMmczMmf/M\n" +
                "zADMzDPMzGbMzJnMzMzMzP/M/wDM/zPM/2bM/5nM/8zM////AAD/ADP/AGb/AJn/AMz/AP//MwD/\n" +
                "MzP/M2b/M5n/M8z/M///ZgD/ZjP/Zmb/Zpn/Zsz/Zv//mQD/mTP/mWb/mZn/mcz/mf//zAD/zDP/\n" +
                "zGb/zJn/zMz/zP///wD//zP//2b//5n//8z///8pXS9TdWJ0eXBlL0ltYWdlL0JpdHNQZXJDb21w\n" +
                "b25lbnQgOC9XaWR0aCA4MjQvTGVuZ3RoIDEyMDg1L0hlaWdodCAyNjcvRmlsdGVyL0ZsYXRlRGVj\n" +
                "b2RlL01hc2sgWzE2IDE2IF0+PnN0cmVhbQp4nO1di5XjKgxtJ/3QD/3QD/34jTEfCa742GRfJqO7\n" +
                "ZzYzjpEBI4Q+iOPYDW+s9dupKhTfDfd6GRd/V/5RKObgX6+X8otCMQN//Xjn7A/fWO+UdRSKWTjz\n" +
                "wzXm/Gf/76ooFL8F/ke5iXCq3SgUkziNAkHivFTeKBQTCNLF20u/cacpWuWNQjEL/yNt/u86KBS/\n" +
                "CaeEOVUcXaQpFEuwQb9RxlEoJnFKG3sZBoLEUf1GoZiCz/4bZRqFYhLeXdJGQ20UigUU/40b36xQ\n" +
                "/HVc4iXEC1yMY73qNwrFHJyJCzXVbxSKApkdrngBE/Ub234XPpWdFIoKPhrUXsZ4ZRCFIiDusen+\n" +
                "fXh7biLw9Hsf5Uz5JPcrFIoTuhhTKBoEieGdM9ZYG7d1hmvOnles9d6fH2d6jvStD9dMvqaMpfhz\n" +
                "+GEQk7enGeMuRqLXkg8n6TneGfKtVd1H8VeQh7p3hQUyIzTXCAs5ylLxqmt0I4XimxHU/ihNCnO8\n" +
                "wDX6r/7eqHtH8UcQRrp7bUJc3v3PbVIo3g5fwmggJ1SfI2gAm+K7kf0t26TNxTjqx1H8BXhBsqDP\n" +
                "Wqep9Z/zD+UXxR+Af22GGgcUXw+ft0ADCfMCf/e+i3+b/7tRCsV74VtTmgGs0GecpoRV3Ubx3fAm\n" +
                "M4MJZmTvy26bdO3ILs4zKuD0hha9xpxM4tK1yEjKNoqvBrOlpVCZ6lqdH/q4IgUSm1xbcnj0gBqj\n" +
                "Fd8NUxZf+ViozAHmZa9rlizBTrgipbJwoUE5quEovhYpC2ca60GUcI0nXiOX4oZPwiQhKg3TUii+\n" +
                "EyXBU9kCncY/yV7T8A3VgfKijFjmdKGm+GaUBVhyV/qDiJIj7cEpmkuSIvyuazO1LtQUfwLEmpYP\n" +
                "hPJYkiCpFLN4enot8o0u0hRfiJgXgEiIrJGQazlvAFunRfZikqrRcLxuq1Z8LXzZwZmvZZWfXLOt\n" +
                "FnQt1AxXZkymp0yj+FoQeWPyvhkiW8ACLPBIuNfya+F6UXr0XDbF94LwTb7mwDWg3yTrGcsZXTw/\n" +
                "alBTfC+YvEn+y5pHkP+mljdRWqm8UfwFZP3mVc7uLC7NvrwxlG8i+LJPofhOVPa0cInIoMwQSL8p\n" +
                "Ts6cAIrb0xSKL0VcbBm2AAOCpF2neeCrQdcUii9Csp6RWEzfXjMpHzTgmzpe4JRU7TWF4htBYtFA\n" +
                "JEC+lvWbfKK0BVJJ49MUfwQ0rjmHbBYZlDQcVzOJrYUSv/Yq+aUVii+ESRFqRLqYsjU6MpOtJBBl\n" +
                "txT8We4pLKhQfB0u5cU5e/6z5/9Rw/Hp73SNXImFbP7nYilf6FibrW4KxZ+GMoFCQXAd1+HJ3578\n" +
                "z++sM3F68lP/plB8N86Dbyw/vcY7eMUx9joPlHLMcBYOkVJTmuLrkY+8zScJnP+iZcwWeRINATbl\n" +
                "ICjxaeXMz2R1Mxqbpvh6+LxnpvhhkjUtbyRwKbmgrbaAGmI7c+mKBgsovh4lZBMkdIpcQmKfEyuR\n" +
                "kJp0U4kHVTO04stRdnyWHE/lSgyXoanXI98QH08plneKqsBRfDVYvk7bxKxFBiBZb2IeAspJMZ8N\n" +
                "vUlPl1Z8Nxzx8kfpQmIBXvmmLIOuVVk5MyfnsyFnS2t4muLLweRNywCtvGkCbZL9jOaHVlGj+G7Q\n" +
                "2P+TJTzjkkq/MZkliBEgsRstpnyj+Gr4oxEuVAS5mksySxAuSfY0ym7+0Pxpim+Gz5axHA0Q/TeG\n" +
                "+W8iRzhS7CqXt6i5mrcUii/FGS5jsnaT46HjlaLeW0PZJuTmjI5Pm6MDQrGTkTS3gOIP4DzyyRhH\n" +
                "B/sPUzRXjOGxZz8MZ17VlR9K1irXKL4ZJO45+1uy38XTiOh0D4979lW5g1FSKL4auwa5MoviD6FI\n" +
                "Hfo32k3TWsjUaqb4w8hD35ct0/G38nvYEs2+t/mqTRuoFYo/Ab6DkwYPrMIBSaVQ/AWQ0z6TK4Z8\n" +
                "1t/x75VTFH8VFnDGvLxRKP4mnqzTVN4o/h526DcKxd8E5htTfeLvVd4o/ipU3igU61D9RqFYgeo3\n" +
                "CsVduJcxpv9D/9FreoC04uswu4byDC78m8aN+ujaTqFQKH47aAyzb+Ka+5+pXHutfLaR0bgW9fN9\n" +
                "dU2h+DjQWOZ0oJPwE2ObMZ2waAuRz2HhNl8B9PwSXb2nkQrFXnjJUSmiPgU65BFwltMxBgx5tDdn\n" +
                "UAHN3qH4PISEtauM0+RD9xbSgKwD6tCNE9Wj2hUfiSfyJugirsd4PG0HPJtN5Y3iN8KQ3M9psL56\n" +
                "e2mYvLH1fQ2d0R5PzymUv3jeNYXik3BP3kTb11ToQJI5Av/4fmG1qSk+EqaWFvVnzVhZ3hDNxICf\n" +
                "UrJ7mqfHz4q/qbxRfCT6ajkUAVc5b+clVczXCX06fXmjmTwUnwnb6iR9XPLG5/M9eblW4oT75PHv\n" +
                "mT5TSzg9zFDxmVgObz7PEQg51Jc0I9Ew1n++8o3iM0HGren840P5SrPeyih6by2NBCXHVpKJP03j\n" +
                "BRSfCXrmM4lvrn8v2ozBdrQzoXosYbEj1NX6TfibnkKdntucVa1QfBiKftG1XfksQSw7WS17aSqX\n" +
                "JjUbJCkEmSBLLrI5x2RNR80Cis8EsWf1fCVULDV5Bx0q55v7LsapGKzcFL/w4JpC8WEgC6XeIC3s\n" +
                "ZSqTQMc74wzTW5D48EUfyl8SHn3UNIXifSAKxiHvrpTMXsbVZxWw/M9VsfaUNeI7zWXpNRU4is9E\n" +
                "Gdo9oy+RN/H/gV8mFLr8PKRkI0AseLrN96sZWvGpICIh/I05AcubHtuQA3RpCX6HB98Q9Ub5RvGp\n" +
                "IJKkY/X1lZcmSI+RlTgmjmIlOacRdizqjZoFFJ8OZr1y2KIWdnS2wiawmVyi6DksBM7S6GgaGppz\n" +
                "Cjh6TaH4UBQPSmdZVPw3WXLM+yQdLct4oXhTiXqTL+oyTfGRqDz2nfm9lTf1oD5Pjr4Sc/BzQE9Q\n" +
                "iUNNA47JulhmU7RALxOPQvEYU/pEE+7fsI0zJkceNCE1NkmpVzqskF5mLJsk25JEUyj+OSgzzPpv\n" +
                "jGdaTL1tNLAOPcfT8KJHbU0z+V4im26KiJKDjV7r7jpVKJZR/CWyf57KG1Of1BmXemwPjrGX2SCX\n" +
                "L99mMUKkUKFXot5UvVF8MmZm+ErexCHdGJqZzHHkHhpDbdpyUAbJ0q8PrzqN4h/AF2khTvGeW9Po\n" +
                "gHQopw1hnEiA3BWvE35F1jT13ig+FkEXaOb99h4ubyzJFdBLUEDZkFC4fDh06Zfj1uq63OEdn217\n" +
                "ITmvNSFF75V8V3lRsQ0TCzWm31SmZCm3gOEeoeInMumpKfLAkufk0vfUm8Aw1tBdo5niubXuFlGF\n" +
                "AoBsRJN0CiZvqIemn2XAuGJVIyTOhRrVitKC7o41jdrNXDd76FUjK0RFKBSLmHB9MnlD7qGWNDRk\n" +
                "LWOxnFEtPDOXMvSeRGlV3HiHt2czpnkxe55C8QD+4KIA6QFU3phcrnXrNMzj8r3Uh8Pztrn8PKoE\n" +
                "5XK4zuxnJZsbyTiqUDzCK++oEQQOlTdkvjZUpzGn8p2P9IgsRHQhGrDpiWywgN5STKezmV0l9qF6\n" +
                "l/qFFHvgoUmYgrpfyojmkc6JnxyTQmUXKd1YQ8e3Q/SmM3W6GxngFIotIEo6HrDEf1NpI/E6ddaQ\n" +
                "wz0MpVdI1IxF6LXkOoh5cxYWaSpvFA9BvOrULAbznLnq+3CdLd5YPJp7IXrY1eNz7BiLKZjRQdxy\n" +
                "dutEW6HYAaJ7wGFF9t+QZVXJ6VyLB1eyb5bpne38TJ9UWyrX545q4/RqHUf6VHmj2AUqcCywqKEN\n" +
                "zb04A/5d/BadPWCL1CPCo2MVSHW7J2sC8ce9pVBE2A4THCwLQfm2XGun8DCqK8tY5SQNf0H/zlgk\n" +
                "1IZnwyXOD8K51AHWcImj8kaxB57rKrbnvyl5zpAMSvRQug9PdvrUDOK59uPrGlT+mp4NLYTT8P03\n" +
                "/LQe9d8o9sHl3JoGhLgUNrDNNbxvB+UBbexeZOonUdkj7aY66Y3a0+QQNJ9P+lF5o9iDKiqa7OZM\n" +
                "QLlnacbCdrCWsV324TR8Q6RCuyMU11Pc73MWbGyBtGxiN9VvFBvhyAzeTMkeWKMK3wD54MG3LLiG\n" +
                "7/JkjtWuQPAkOw5noO45oldRo/JGsRN1DgBXfUflTZrTUW7n65N/a4+0J6YSFC7d6wfSrtSjoUGq\n" +
                "THUgVib/XKVVv1HshCPaAlU8wn95di/+/35maZuDPDM/WS4hHLs7SRCkXZW6ILa5YuMmG2lU3ig2\n" +
                "g8Uo89m76BRIv6F6SkRzrk5zSpslJ0xbfr1GjihofTaG0hrjtBuqfqPYCjaZV3bgMlCLOwZnErhQ\n" +
                "G6mDm5L6WKjdi2YD7bk8220LOFpBKn89S+WNYic8z6t5jbL0HUiXUccERJmAcha0yyub8kEf1bft\n" +
                "vhtQB849/mjlndjGw6p+o9iMFNwc/kPfvISw/1qVJ74dW/Z2ChLCkHPZ5EVUE1jTxL/Nwet+T8U+\n" +
                "XNYqJhLot8wdA7z2XDqxcLcmJKZapDF+EAWBlKWtvy8UtTPJRYViF1y2ghls8ALZAF71PtHANnSH\n" +
                "ZT3W2YxPc0fLmoorFjcmb1aFh7KLYjM88akniePT9eZUGu6jeVFvjOzQJzIlxUi7wgym1ZMSmuSG\n" +
                "sYLuIFY5sV31pzKPYjO8IRKHzOY+ayAshJnqLSE0zPs8xOufSJNrMJFtTGIbDMaJuXbzm6kVijeD\n" +
                "HfJkyqEbVAolAMPwaMuySfkGfHoa+Y6lYk/POETd5p+wTXuyQVu/aVve0Uq/uVLj+/sxfYhOG3H+\n" +
                "TqzQv3PvrvojelO0mb5RJA7MsRalU5FFkFVoHIJjleBnf0qWscbdOZBOit+P0TTw3mcvL+WbqOV8\n" +
                "imeVbvNgkmAyMUYdQ1Zlnfac2wd+G3bewU6wZ58LzyvXdPz385f3zX0zNCM9WE76u+6P3nCSfiQ6\n" +
                "yzMqeF5dNp/S2tx3z4p5WXlj8uL873oPnO5Yz5Xqn2senpKeg3OKd+kTLw6RAmWupxqK5fmh4QBP\n" +
                "P/W6ytHvcV6DI0qbWpr9E2njva1zTV81teW1rZJ0Hs1lcbAFnGwa4Vee4s7C6JGZtiu0hXvvwge6\n" +
                "tsDRof2I8vUG2vH1o93uecKFkPevfY5dSSleTfCRcchCrezg7J9HwMnUJmNeMvFBzd3Eacpuf6/3\n" +
                "8odlRmlzzzSLy2TPWefcxB1ntHhegolopwcTd6+OLYbXkpnTNpn2q6Yd2X9tlm6fGgZcPbVsesbE\n" +
                "OyAbFe+0Idt0u4PYOKR3IzjD9rhcckLKscb0IfHZbeYOUq7y//C6vJC0eWuQ2fnCuu3JfWMWWWeQ\n" +
                "hl58zoQFpE56MvekeBbrPbCB3dFuzQ25ELa2/4M2HClVRfd9d59A1qSVncyExQW5wPZVTjQwc2z+\n" +
                "qXX97P2pIGQSuHShGwpcrwOPa5W7kmsaHQLce8QC5arBTpy1ka640gKqV87308TZD7z+0+cPhX1S\n" +
                "ixOMhcvfiWeNfI35CXP1L7N8yRpN9ueQ1OpHYyaua4L2YRaJNogTAB3YjWJ7CuJgrWdSHqmwJg0i\n" +
                "mLyRZuo6li8+bSBhXZfOi9Gqnrs+X+fJxQjPaes/sR83Ee8Ym5D72yxRZ2BR+q2vkb3zqV6Cu2Uo\n" +
                "EdjStplpdV7pLfV6UjyTRqC7VaktuJ2TbXIN7+/Lm1dnKRtwO53cien9S1crFqUBeQ62gdK/H+TF\n" +
                "m2T/Mh4XH2VnlAOq48QmW8KJFQmfjiEga38rHdHU7sWRGggWH6f8e4tNoDQh12wkEciVOX0Ltgfq\n" +
                "b3C27Y0LNAakGbvFgsisBvasVIjfDAY3XySPJGj9+5oj3GW9htOVNbbxS251nNNyQSnR9falsRQb\n" +
                "ezJCspzRsv4gShspTuA45lbKswir0N7k09qj2ltcTRE85bYSkp4ha3R3RUBEZ8hRX4yb1Qc6jaCP\n" +
                "4k/tvINsKTQ9G1vW1YaSH+0dfgUHg3NOsuO5qVF3RTWXeIAXyQd93xBcuDx9OqmZ0Pr0Hs9Ne65B\n" +
                "kgMmTwHBA2JtU//cjnG9YJZRPiw6uqLp5F4AfZXeXaYM/CCl/lNzdRMniHQZpPOwMpJ2Cnjy6uxz\n" +
                "5VKOTk52PEh7VuS0MShcEp5+o6otc1q1H1lNadDn0XqkD/CJtgN0+E+Yfea4fgWifmZbA6qX51w7\n" +
                "svOjOS657/I/tl7kPd7RcZDmZFJ0Q5T0hxNJv2CWVt5uMXHdxQzXXB1dq13PCz61XNxghd6BLJvm\n" +
                "5vPmTTCzQlxF1auDGZtduIErgPXqb13itEu01q9D7sZLI7vV+Hyi0bfig+S5y9drfDPXI6g5ogZY\n" +
                "9VRqvVCjSZ0rnE0EZ+rpLKnNk9DQPq4JAK4Y0IPyIOWSqvMOAB/HdoxHh+P5xEVhyzIwjbSbpLMM\n" +
                "JI6hp0WnctLfYbZDK0qwHk3Pl2xpx9GfGVdw1gvPwckvg2OthDxuVbR38zRQyIC29yyUWNpCS53j\n" +
                "9U8Qxdkh+ND6pZzz7TtPtYITALCw4ubyeMZ2FdNIhAtW1gKj76ma9qw/jrYNvmr1UjxkJ/5sbW8/\n" +
                "0iC687PFz91tS2MZD0vXjx/j6/qF9vXLAfkpFQCyOZQWuhxkMRXrcid0ydb0L7/euJ/Ke0/lQBFL\n" +
                "zS75/olX7bP0pFJ0IHFi+3PJ3nOKZFqMUOkbUIyvZwGhpnBlL77YQxR1G40C5zQpyA3RwscbherX\n" +
                "6140PYoLLyFuQegARFqUzGv7mUQ7YPDSj1Evahtpg9/BnIaPe6lbtlpwwl1fBbnyqwPP21c959Ne\n" +
                "mFlN4hHQb5wUYLNY+1HVkHVpljdhHXuzF1pMdfS7ee0A2lB6qiN+n0IB3M7XdGwe0V0MFbC+VKZ5\n" +
                "C/MxDA5pwePZayiVE6KkXRt4YeYfiBy6OmzWiFjS5I5BlpKLnPA0sDa+g1ge1i3FvvXLttkYyouQ\n" +
                "SsuDG6jVUh/A6QZrTnIL8EvhulN+l/Dm2bi8cEeV+YjqjDiGwkzGm/lDcCaTPWL8/vp56CwNRt+n\n" +
                "vkH0BrUj0UgIwoLL8xBzZt8fSWE0xF4dX88t8HN0Jla7HL7IYvYj1hC94bn6kXpi8tDw2AG0qsES\n" +
                "68tRRCQ/rHYPt5Pysi4BdHDzksbJVZN834RPxk7e1zzpGGk5MajGZw9EiB2QF3d8Bw96orBK26rd\n" +
                "YLcqjdzu9UiUE1Bvk8quDm5B+Zq8szuT4g5GNy6bEKqaHVxDotpEtohVz1hhm2D1QlU88Dus7u1G\n" +
                "OUfrW6j2zZHn23hb7vMo26ZMsy6vLWlDlU+w+GwypsUhj2SonVt8EFr1ytzInYw0xf4QgVIX8g2Q\n" +
                "HmIzRLMLmJagb+tGQhR/RdjzZJZU1yASY1mHhRoSHivMBD13NkXwIy60mOsqD6JgwdspOgJ6rmQU\n" +
                "kE7GWYegmxCP/8CUmX/gfAwLSz6WzlNgPyQfEYVkTsP6o5e8Y40fA7bOcp9WH/lNX8xBtBvJTlcy\n" +
                "sI50nNxCSKduP9BIh6uLNMU8yinu6HxZOpTb59Ed9O+pfRJs9V3o7/TdWEJ32t+PCTXtFlbD3jSz\n" +
                "4mAeazgt6ngNXE13oDlRSyL5rEn7WtJ05djggaaZ3339/DBAbtCGNl9IiPl5Jvdx2XUjdFW9tT1+\n" +
                "AGbK4i+FKm3kmyV/UhfQHgQpQTN0tz8g7XY8QPvYYExAIxmptiht7u7nD9O2pZJqsnUz6NWUtsne\n" +
                "eZiXNsSsVJCdaI4a/pK+n8+vKZiuN/puBAPyA1pckuCoM6AhDuQNXCbNtWe4PxSA9wB8+v25y1V1\n" +
                "4uQXfWc1oLrW3FW16M3JXdJDk0fmnsxh+9h760nRd7OhlcS607RidZ99oojnOtKehI5QkIymsCPB\n" +
                "jaAKXQuQYLF09P1gDXBsbcTPC9OGy7RFy8TN7DdzUr/Jm7FS/+cozsx6/Yt1G9OPa61hgR6w1Mph\n" +
                "/V/Ax4zU7cn64vE1c99AKDRjS9Bv0BgczDKuoVuXqeXoute8fiRjZXvR5PTvv+IpyVzF9//Lk/eS\n" +
                "dU2KA0DVt0tefikIu+/ZXWnCpA12jhhe8oAlAPax9IlP9sNaAE8AfH2kDPSsmHvSwB9cSolR6A9W\n" +
                "FKJJrXPPLN9sdLUfYWP0OC3cjdxZ0mbibbW3YJH2hPqkHnBjcPeWgOy+9q6RgIfMTvnm9Wqk8k63\n" +
                "M1qpPEqUD9pTd1Xd5n9z8h7350R7uLyz74wh8PVMcxxj7hV40cxY9GeAI2hnalYjSVFppc6BomwG\n" +
                "vgNRkvHZGwfw9PUbWZGWd165YzUPM+2l8ulFw8wt3eYQ/XGsf9vY9/7O4Xkf1U34K/G4rdNy3IND\n" +
                "KTDueJGl2iJ/07OEbHMLNXpqdnzu6LH8mBO5DMgCMw7zbTmDzPfgcMmN0zNzAZs99C2jFcEGYW1N\n" +
                "27eCuYHtz37jMk20Ej00SE7YdLGPZaTeTEa/ofv6pD1c1pTdqqNV3BPItrT47R2K3diDiOaOrzoh\n" +
                "GXTpY0sOwUZnW8GU92Cdb+aMRPi+wfiDI9cL35oZiiuwjeR9Tn/s62pewGfwDdJl1iEmi8nPeFZH\n" +
                "HJf1TG+S19alzvDJo1QYU7FvOLJ7YMEUBErsh257ngOLhidj5ywH3yz5vu0ly8r/bqC4641rUXLm\n" +
                "Tvl8lM/+xMioewJZkEZ8U/WBNHzBfSPNqetzamXyzdi9qYc/iA1sqDZ6Me0t6On+FvSsac+x1XVT\n" +
                "MFhbi1EQN3wsiWopCrWR/iZ03M155M4FrdwG7K1HEccBOOKO9FOnxb8ektNzz2uDu4g2qE5jnbSR\n" +
                "dOGO/kCByz9U5IbFAfRDnj5cXc/X1lXaxZb76beOWm6F9c33/zRgQMRTe7dsFXk92iBNvNTCXvab\n" +
                "hEd0s2Yj+VgGNi+YgxPpLVhz6vnNZEkm7IO1LY27gPkAHtL3mC7LQoqeK+eC+GUAvvyNsWn5dBuq\n" +
                "3+wIF51Y/gF36+DJVVYHI2pEyxaHZr8KjQ1DmTN3RgrA/Eg7vAygTXSdgoJ5v0XDEROEbmlfb3g/\n" +
                "egCO4OZONzDF9wY3nvdhFMCS5iTqWmkI4U660SnC80Xt5ilwJgTiykUPtnX0xe8EWnfvi1x9i+8m\n" +
                "EMbWbXbPso8FZbOBnLa6r6dxwTKJAvOR7ffdcMmwgz6evMr32FJ7b7Pip0FSb+zxTCpcM4o8up++\n" +
                "N4ky0a2mB3ec/TraTYVOeimk3wD7myHazVt9N8JuJbNDy+hNXuf/vUR+v1vayKeGbZkUegrpQ4x2\n" +
                "HUM26C9OgD40l6tzHF3R7m8iXlr0BrbutF33/04CRpoUjhAP7frlSo6YZeW16cyb8eC+DVhvsAts\n" +
                "/n0t5JJeNUN3c0nPuHDv4iSDyL9ReyUvobNh7Jdzznu9N1B32qTwwnr3g6P6gxssl8SckYuakwf6\n" +
                "RR5dOPHTQk8M8Eb6PBNn2xNCcvjws3Am+CdCyizQ3acyCykZ9POYtxl5I/tYKJ30CU+f9ML9guYk\n" +
                "aE9w22mOTJvO2HYPwtEDb6RdVir9w1XN8LS8Dwa2UW5aX4/H9n2MNycs2byYsTZJB+mlLmpOeZth\n" +
                "+SSTrQG+lY0hNu+U+TXfNFHWL9A2ev/vlTnSEnQD37wnViDSHvPkdO60ICmwa1t4NjanSbTRerGQ\n" +
                "liOVEa2ZaxziARAbMNJv8LkKU5386Xh01ukAMzFkdzFOgA7W9eJwqdjmkjdyF6wE8NSny5mqHjM7\n" +
                "8J7gDZsGM4bO5/5C7eqN+XN3PgLRyv7GaAF5aD/VnQRfC8+hJvhYmE6Tfhccnrie/X09nv+PtHJb\n" +
                "TnbBu0CpH6quK/17oh/Fof2muLcX9d/gFUfb1zN5/j4M+ISxLaspGAGMDq64RRzWm86jNj2xAOVB\n" +
                "O2JG1HJfkArDU3Kq50vnEVXaSy1thICKfYMHngD9zp28rOekUy6q+piJU0s/C+IpOxto40PDNhA+\n" +
                "IcyjZNaaiE675kTJXS8HnKEn13RPoN6tliWTa9l+EE8HWL3plVgANirR+gxOcWLFJiXoR0ASpDt6\n" +
                "9o1Oz5lX1hqp0OLTpRmRSYW+gRR0GkrdkXLdUUlmatViIswO1SAeFDbuTWCt25dVZiJgdGAZoJbG\n" +
                "3yNzxFPWBrnAxnQFr8UuP7V0Xo8n634UNJPOp7v+SRlQbVqhCxYsmJaNUw4JIkENSv6aixaej61z\n" +
                "Z1ovH87Tu34uRnEh8VehPPSFjaeXJ8ArirE8lXAvX/j/ARyxumeHDJ9Nrs9tr2y4bq+FQvj+XEmH\n" +
                "fHMWJG+M0mFcxSaO7Tr3zlx07XX+HY0QSL+3lqOuR/0V6Jgrux2q7dgu1tI3G71DNd+EOoHz3epx\n" +
                "8Kr+Lp+/ReS80X3ztl0fJ4QTl8v30FY8kWB7YrEAc6dNeCos8B7Np/yG6LK4GPu2LYgHG0wry99a\n" +
                "EzdEk/wLdDZJP8Vb1ZtxeBpomeMWPv77NVdO5TxF0WkoQwClbxxs+jO+GYrGd6R8JNSRvbQ1viSJ\n" +
                "M2ci+B1azvvkzSh26SFEvvHyDa7rvw4J6Y9jtMKW9vV0KBOPjSd0pv0bcpU7te3Q757oPAvRh+bL\n" +
                "82Ob1yaHvBv4k+WOZF/fwPSpG1CnbqPOn0Goo3NsPN1ZyT7NpKS5SLev2uAcAdd3WNLgWs4j6Euj\n" +
                "qtanNl3/v03HlKjXXiypPRd+Q9Ca1JqnfCNY6va9shF1NBceUnvN4f38DIf39QgsYLunCSxZm1oM\n" +
                "pfdbdcxZ6kLURKdZny1rTkgi9Hm9oaXu370yFJ0mlVtkZ2FfDxwag7jyR+rNhB8G+LA26pgrRoel\n" +
                "ozY/lWvK2vM98ka25DzrkVJvmXq8R4pOE6RC8agMVW1pX4/AkrULlcXF3V+mBdo93Uamvyfvt2TT\n" +
                "RDuHQjDTQlufZxJ9N6QJ7/mcBHN2/bupTjqfxtZ+lfyipgF8KYEZWl3RjLIpgOhAZPEThty41rDY\n" +
                "fh2T1lOm7gxqG2r7528ueJd+s7pnrZ6RRxhVWsydJliwFvZXQs1JlLDdYfpQ3gyNnt2g8cdY1WD9\n" +
                "GdQ02bIPj1MTTYSP+3Ycr/yYOpz1E5CPJXyBW7yg4aDcaeIz+955PK57zMS+u8k3b7XNjBIJm+LL\n" +
                "6fl0PphnAqR6PztJWrTtC4H814/30/vNO2aBqKeI70IwYWUv9zDyWDjZU4yZ60wWj81pg/pOxI/d\n" +
                "xsSO26ZEaPOUzNmXX+E9eJu8mbbtF/h5cYQynY02Sad536T7adkFDUfe14NlbGcKlqLT6pm4jnOL\n" +
                "v0+5b1qT2nRLB7gbt+uB7tn0wK5KvglvixfobQUEuGz8s7PMeJO0lDsNRzNfLZ6b3+STPUXrpEj4\n" +
                "ndFpIv1th4bCOh2DCIbw48aC9pOFzdHLcvsOwj3DqZuZQNPtQ99QJ3cajtmaH04gjnpgVxY7U9C1\n" +
                "XjEa+oUjoYe92ae/ayp/Fn84Mkt/uEVNmvHu7b8hOkJ3fwy9P+o24f7RnJ++lazQhR6MTjt6loFZ\n" +
                "LzV64bmcMBiQD0e83VhnnfPu2nNz/XNg742cpipjGPx6E6GPF9SbYh0r0RO+zzkfcb6UDOlsz8fC\n" +
                "HPqH+yv9+d0XQ9+QF3wsF0B7X4OMAoQ06C76JVq7S+0a7l8Bj782sf2w0USKO3TmzrYRadq6r8ky\n" +
                "35yNVGjNvo3/C+JC08vr1ClMLhHiLBTvnnylwllrhGY/d5okcKYe3s8BJcS/wdOzBX1oqhPm/BuY\n" +
                "/h7VYT7vm4xOpPSW8xLeCMkV/XR9uRJmk7tv8qHjlTXYDUrkDZJG0y+9n3W641adbci7TyLYRH/L\n" +
                "zp4m308htaeW74LosX4L3+C4o2LRH71SL9PmrwzleibPvqPhdHQSR/Q0SeBAij15M4w9m+irO/RH\n" +
                "iG0dyPxpWt385B8M2fH5DPNTKdkTM8es9amSwP8Czqep1J+70dqIMQZZQsWWwXM39558A9q5h/62\n" +
                "NKOpw+p6bqnl+yAa1B7y++zOD+YEm3ulEydGiYM7SoX7Gs4wd9q8D+eOfrP2Vt6n38jntk56wcrP\n" +
                "jq0d/x7C/DiR1WVAd64z0ggmEcsj5EFvRMrIv8NuwNEMM0MKmQX84IaXJEXer9+8i75HGXbuURYm\n" +
                "sR21fCPEHe7PItTG8QLIhj/Wb6QoMJt3znj8cFNmuM4pc2PGFaLTSA2Ficixuy5I8mDc83Pv5n3n\n" +
                "D+Ez4vKZPsPyB3lfN0KyPgBwjT3aNzJGts13OiNbU9LPlLyxbbxWnXW6Htxt/JnP5dl9Yw0HRAtU\n" +
                "PWXb+LKX0Dbkv9lpSQL9sIc+yt5wf8SYOiZvKV7wf8LeiIGMjrzJFqHmnpmen0lEPj6hULIjjjgX\n" +
                "Hp3G75AkOOjPt8aPHe/K1inI/PuuC0jt0+UNjPV61A0XWSh8meWplXQzj+Q+CQM7eeJkT0GvG3Iu\n" +
                "KFO/YSzBEWkcrTHRCZMQQy+f0jVAot6XEPCsk+e1fCfE9TiL97oDKco8rYFRIE732NqrtnCnZy1M\n" +
                "wC1NilnJ7kX1pPb54sHQBFIMRksX3+ma++6itwnqCf1dKSVjHaD589PlDdpPckXhbiGbaXOPfDE/\n" +
                "0+dOdH3zykwrbkY+lgC4OBhqOEBCtCWwAxy0rvVE0ejqDYB+qsf0G7/TQ30Y0Pv0eOijY1F7UnVk\n" +
                "cTHFH393lYSlY30y2oxQuKnhSPt6OGmhQyfr8MmG6MsKOtW4aXqwmh8vbjoLtYeGgcqnT8iVfuLr\n" +
                "5CGnQlt/U88J9eYQM3p1FdKxWUCogBCJ/M7AyxNvOWEVdcKzKIe2vz43Z2eJcxKDUm8LHN9a5cmZ\n" +
                "ltBE3X2jua7wYDxLvc8HZi7D6YT/1gUOfD7NohN/eixJB4Tg8tuSv1ms7wNLaVgvYGOaf1JjwDe3\n" +
                "af07tBa1aKV6ZkMnW6wMPZyPDKr6tyGjNroYtPSblj72ntSeFjNqNTh1ANodq0OjxPg716zrzzrt\n" +
                "W9vHuaHokuEJz1jSt217qpPVESC/4xwccRdE8HHf7hGyGCNSt7vLb7RlCx4W1SREn1xMiVXp1GJO\n" +
                "cxJNaqb2qGNN6OFkSyskWb4ejPOppfIKfRC98fmJB084aQ/7o1Wmq6jEwBphCfOamLXgeSvYSlUD\n" +
                "Uhbmi86WKbQCRPdKa8CaI5opa3fEQGdhexOure8GCcbp/Ypl2iFvJmA7shboeULVeBI31hM28aRl\n" +
                "7DcJP92Zrq+vQVYYHJrVloAaMegfWWf0B62vdN90HjkB5CnbYr9SmU6HkfvaWDyJpkfM/SvEDfYj\n" +
                "3O/hhJAzgOWehTFrcdU9PmerjcHFeRBEs0BDEGe26cz3075aFC8MYtnes1A76aZ+sXWPT8cBYmR6\n" +
                "JK6wra6zK01g0+kGDewfIFZQ3uf9RKIzc8Agf4nrnhMTAIuB+9DghoupbhYsVGIhIKRzQAqTTLhT\n" +
                "WBzfDbjc4i37MinhGWpnq+Y143b6ehiL/++wbhqeokpa309vOvMUuEoD982aBU7AM6XlbD5o1Ahz\n" +
                "t7ividdVMFlvCEYnAqdp4221G+XGBiZov2YRa17Cb+Ca3jo7vEK/HqdW+VOudNoy8hkx0nO8UMNa\n" +
                "txC92VK0k5jml98vj0Gui5RPcYdPPWSF1Or37Jixv12i4CWZurgHp6stufa+UIFjbsyAGtrr+m/g\n" +
                "Himmaos3ISg2WLcJvp0ZGi7pQYWO4Gmp9uegwVpqxjNiFscPfmcoH5nYaNifIDAbdfwjxdhxncOg\n" +
                "7D33BqWtqEg1tXIXwvpyir9C3ER0Tja+FUJBrCq+e0TdMM2ebIlD/StY3cRDb3v5sVvHDKq/RFkU\n" +
                "sZXuImk4a4OHSOprGObhjN+sXZzPA30obhEVI36DSDckF+r1/wPvBQlD40nguXS4RfyZ7Fzk55BG\n" +
                "7CSHRUhtnjULyCq2ZAepS2zWcGxT//qk66jBrb5P9iZzBATsp4FVktBMGh6Ja/j4nZ4Xsl7RUdyp\n" +
                "jjPqbmYtGsmaVqWE9OAQrKOg492QT2XaosDhfpZDigKA4/sSKVLTLau3JOkdpTXon/KZ85yMJJrx\n" +
                "Vfke/aApCd4zyoDcOtufbqkuxABzm34yeCxT+j383AscYNkY23itsccm0QF6hRhIiIZhL3JGqt+c\n" +
                "Y2gclNO2u55QW0l/24cRB3dlzXIpMo3SX1wN+XSWDv0RhsUlNiYT9vLYNPMLNqzV6DlYzFJMU9Rr\n" +
                "ZHLBfDlzdrNULwOljWQZlN+gfKI29ko06EVPSyFFVd2FjlpinKqniCFEskWuMY4sbToSf9gAIMV+\n" +
                "k1HgQg6bRLPf6sbVgZdzXtYQSxqpUSdsXbIVi+Rh/QwoBKNseqTncjrCREid2VzENQYbHeGk0jyg\n" +
                "ox+KpGsi0ojIPTrWVNgi1Yz68+OQZo2+NuIm9ZvxucEpjmCCFtSQLrZpS0v5B6TzZ8LqXBQ49Wy6\n" +
                "vM1Rzk3n09O7/T70sjAdjDYk54aI/wsqzsz7DC2HkrbNluYrJnXC9JZ1RmBLk/I7fDSqDipr/8vT\n" +
                "MmUcZTHPTV6s15KsgUbs7g4ZOEK61Xatb8nAUq5qRy+yIJOGaEqReWZCf4Dttrm30azdsrCZ3jVT\n" +
                "4gqp1Ut8iWRP0UikpZeVaf+SOOgWw+UV9pnkT2eFjQL5ddH46N7MEr6HifO60Uvrubh6AofVrxex\n" +
                "IFRGNKlVM72472AcGX3twaymFyCKIX1HZ3fhPYjmOOF+2k25/kX6Qcrxl3RO6i9EtshgGANZJ5T0\n" +
                "AtPQa3ZusUfq0mhby4kzRutsnNnmVWs4WDXukzZSTzb2DPt6tXpct7XE/OuqfGZoG5mrBym+s34C\n" +
                "W3JnedNpNmfQDv1qUjRbT2L4d8izwkA3Oa3SlHf8pYWM5EwomGapS5ZYcm5MZWE6RMuC7a1/BS/D\n" +
                "YLkgCxzHZAKsTr9TxePs2OlwQWL0pB7WzZKO0C5lbXW/l5/AXAwztF+vEk/Y9GRrn7Suopm7Btb6\n" +
                "t/luKOrZi3VZihg+mcem41pb3aD9m20qiPNYR9Mh2TzZqnrUrajao3lM2vfJC9458UWOZ4XWulYX\n" +
                "DNXvtTnLeKqL4kVUkmmFvmnfC73fXXGFVEcdal2+Hjt4fZIYksQc7ku3+D+gsczsgmXdUp4QV8Dc\n" +
                "MiTZ44wnNCCgkOqcMHYRFGvN8vKObkDUReFdp5iXBb15STthf6S8RLu9H2hBpYAr/e9H76B/Kh3a\n" +
                "NNhElwPaogz7VYA7M/NrFK9g/8+rjjfwdP46Jdd5RHJ6Xaf8wpRn7HD3En7Lo3t00/BNSyMVeT9E\n" +
                "N8DZS875bFa++kl6Rz0pLuqf5/ohH/7OZRhbTYxkN9Tocu39pQWDOo/MEx+OVP2xljMNU+kxWG8x\n" +
                "Fzp0yI5QqXslz/jodchWRJKbTTIrib0Y0N1Jy9vj5QQp8Wk/sKavSRrkG8tv9el7tS3tqt1yVyKz\n" +
                "R4JLu4V+P2DOeXhFtr6B1XPWWySdqF7jxzlv0pXRnqA88rEEiEORlIUNn+hGZCV7CTM3sWai3pf6\n" +
                "reiQE1kamHdNflahm71Cc/YuZxu6rc7G6M/5BX8FGhPkHbQq583pDq/vEWDxcTHR7lVMA9K+yX4f\n" +
                "SmfcvITl4yg+qYufyWWmj+494mepNSkPnJTsC8P+aoMAhGCGnOtn7CPtaE7oCVGvme1U385wU6d2\n" +
                "emkWL7YpFLU8MwP7qkwBrpdnErknmXk/LczaYhY7jOsdrIxr0ajQ9uDYQvqrkO0r9yanU9WvfMRE\n" +
                "v1l6adb319S0xr7ecnv9OlQ3SQxvW7fIOAZ934uGTp9oc3EsDa1e0la/fje5NGsPfFXxx608Iy+S\n" +
                "Z4Z4fFtz4yZyzVexTgI2gMj6SXqFPYqmpyQW2LmFR4EL8QzRduOjdW4mHM6fBXNJWtrmgRAsTtkq\n" +
                "FL6bIX2mwyIlE+XTaCWXsY206egf9+Zsbw2iX32e1s4bxI+jjmK4fqe+oNuUPxh8jp+KBwjWHkcG\n" +
                "es/WNF4DX+s836WFqXNL0n0rTbFCcTrU17FSp8b/Aq5lyv2dsnU33eqn4Vug5uMluulukX4eKd/H\n" +
                "ORXiDAuzUBqT5uJFiq1R1WSPzv2a/qMyT8pN0Zbi/fLQM8t9jh5zvVdjiES49T7FVpzNMIXys7f7\n" +
                "K9DoJqEbzo6OCKE2rYyp/SzUR8FnxkLPBHqFGp3t79QZ1WOmnfUnrXuvzAxd6XdZEoHJShjWs7pH\n" +
                "oU2jCq73EOBd3omL+6FPvy0Xl6b+cqvWz1+pv0KxgDjsXBx6OsQUijGUTRQKhULxfkjSRqWQQqFQ\n" +
                "KBQKhUKh+I1A/h2FQqFQKBQKheL/xn8mwENTCmVuZHN0cmVhbQplbmRvYmoKNSAwIG9iaiA8PC9U\n" +
                "eXBlL1hPYmplY3QvQ29sb3JTcGFjZVsvSW5kZXhlZC9EZXZpY2VSR0IgMjU1KAAAAAAAMwAAZgAA\n" +
                "mQAAzAAA/wArAAArMwArZgArmQArzAAr/wBVAABVMwBVZgBVmQBVzABV/wCAAACAMwCAZgCAmQCA\n" +
                "zACA/wCqAACqMwCqZgCqmQCqzACq/wDVAADVMwDVZgDVmQDVzADV/wD/AAD/MwD/ZgD/mQD/zAD/\n" +
                "/zMAADMAMzMAZjMAmTMAzDMA/zMrADMrMzMrZjMrmTMrzDMr/zNVADNVMzNVZjNVmTNVzDNV/zOA\n" +
                "ADOAMzOAZjOAmTOAzDOA/zOqADOqMzOqZjOqmTOqzDOq/zPVADPVMzPVZjPVmTPVzDPV/zP/ADP/\n" +
                "MzP/ZjP/mTP/zDP//2YAAGYAM2YAZmYAmWYAzGYA/2YrAGYrM2YrZmYrmWYrzGYr/2ZVAGZVM2ZV\n" +
                "ZmZVmWZVzGZV/2aAAGaAM2aAZmaAmWaAzGaA/2aqAGaqM2aqZmaqmWaqzGaq/2bVAGbVM2bVZmbV\n" +
                "mWbVzGbV/2b/AGb/M2b/Zmb/mWb/zGb//5kAAJkAM5kAZpkAmZkAzJkA/5krAJkrM5krZpkrmZkr\n" +
                "zJkr/5lVAJlVM5lVZplVmZlVzJlV/5mAAJmAM5mAZpmAmZmAzJmA/5mqAJmqM5mqZpmqmZmqzJmq\n" +
                "/5nVAJnVM5nVZpnVmZnVzJnV/5n/AJn/M5n/Zpn/mZn/zJn//8wAAMwAM8wAZswAmcwAzMwA/8wr\n" +
                "AMwrM8wrZswrmcwrzMwr/8xVAMxVM8xVZsxVmcxVzMxV/8yAAMyAM8yAZsyAmcyAzMyA/8yqAMyq\n" +
                "M8yqZsyqmcyqzMyq/8zVAMzVM8zVZszVmczVzMzV/8z/AMz/M8z/Zsz/mcz/zMz///8AAP8AM/8A\n" +
                "Zv8Amf8AzP8A//8rAP8rM/8rZv8rmf8rzP8r//9VAP9VM/9VZv9Vmf9VzP9V//+AAP+AM/+AZv+A\n" +
                "mf+AzP+A//+qAP+qM/+qZv+qmf+qzP+q///VAP/VM//VZv/Vmf/VzP/V////AP//M///Zv//mf//\n" +
                "zP///wAAAAAAAAAAAAAAACldL1N1YnR5cGUvSW1hZ2UvQml0c1BlckNvbXBvbmVudCA4L1dpZHRo\n" +
                "IDIyMS9MZW5ndGggODM3Ny9IZWlnaHQgMjI1L0ZpbHRlci9GbGF0ZURlY29kZS9NYXNrIFsyNTIg\n" +
                "MjUyIF0+PnN0cmVhbQp4nO1dObrrqrJWpMk4ItJkTkREdGbiiIjIkyEi8mQcOXmieiRkI69m73e/\n" +
                "g7285FYU1f8U6Pn8jVZysfdfOedPt5JT8Mtlvsxumma8X+rjdFm8Dynf/3QPP2orWd6tRE1uqrTB\n" +
                "gXMXtz7O9QjIXAldiUz5T3f2TCvRu8oeB5S5+UK39TUgyCGhld5K6PryEtOf7vRQWxk2QZ+RXSst\n" +
                "RMN8oReBtmlGGutRJXj9VPi7CcxhJWylBDrMnQdigIfrMb10AUr5BhQDS5f0+NM09NrjWQL2HZQM\n" +
                "/jshEfm1vrx4uTmgdUbWIf8qB5f419nSkozlmC3j6m21i6n0+ryanRg8UUbDUPn36/1/0ZKfVN6k\n" +
                "o/VgCWmEESUGRxys/9bh+EvYV6IVQOgc6Nhq48/1MKfF6Rgtf4GDKB5soeEX2PizhHFbTRKzf3J/\n" +
                "WDqLr70QXwaG0s3+S4N+jwu7iyl+V0fPt+InMQJo+lbyvkYZtpU+tqF/wHbWMxawIiSONNR+I0mf\n" +
                "e61HWCiUiX8g/vSoYxJirAfhe0cZZP5P6F6UcBHd83r7iS4ECNqm7xD14ZadcWcQHm8M9/dJUoSE\n" +
                "afK/JZsiLvNEBHZdbtW3+/MreoctLRB//o5sJvGzFJFsrci3twzOffl5u7naSLYf1fCvjBvRhy9y\n" +
                "7xEq79xPe71EEZbjTPOYayqS36Evoaa6PxZxlurcvImNa1pzMJY/kZRhxJDQx37bj+ZbrndCrjYN\n" +
                "Xk7rQ6pH9VMJ7vCF9RGeZP4NeDnhi8Pnl6NcxcV/I2kIyiFVmYgxtxuTXP/dSrll/Wwu+lbZH3/S\n" +
                "l8q85TvFwlC1vRdkG763+8hN3r6Z9/i4JW6U1ORIzb9JNoFBudyK8umGnU74FAgzx/IxIi8bIuH4\n" +
                "BvfPenOvzPs+17NlnHRd5O6WE7aCZBJh9JnUY7mK5Wkhq8z7NsVrtKXpXgE8GQDI6TJVdGvNvkPO\n" +
                "RWQ1l+ADU9pB1D/yEdVsflc0hnLEUnVj+7j+RUQFZsAVHMNz81RxExTfktdXeTTo6zeidfXMMcZw\n" +
                "8rbeU/XoLjTf/TSw3pg95ksFy6dJIQGBV1dOThfnUxXHXDGxzJLaMu9ZcN6AEPfend6nU0yEf9Y0\n" +
                "a3IGs6l47uecE7JyEdIQZmW4kVArwIkgfKlzHW6piNbUJW3lXHIMaBKoubvxq/Lb9FINjcx4roR+\n" +
                "GphZH3YjzvmZcdbqeCKaElBA7uzFIaHriT1JMYkjCzj/PM6XtNT0aDXIocDUcvvUfmZ2zWLSo5sU\n" +
                "b43q2JHCRSY9CHX21rTurGVtacuvZjJhRyJjpJOgG7P7jDafjHGEe2UbdcClTvBSZ0JgZLF/l8Vo\n" +
                "rXi+1okXIIEFcEbIYt80+ludj1+ExE99Q5rQMhRxcd7JCC7qutXrgdiyBAECwcKovE9l48SLc5O9\n" +
                "LUOdy57h4LHPb1qcXGIzjv33JC4rX7xoEmokdBzcXJVclSLx/o1FaU+0GDj+ONvYtbU7OIRnCSuV\n" +
                "b2usI3yrnUrSgSpuJnABt1YP7jUJuOeFTED9BXEjGt1sicst50aCM/Tja4I5r705S9x6Rsig2EgC\n" +
                "bYIIVWnLrAmqciqAHmc0cHiKaO7NRiimidKBQzvRyeQu576ArcDIk82AfzfPFsxNQUJOeCtZCuEV\n" +
                "ADYrmwMbHWHbrRFLCC9NDlwZcSLkrAHLeeKcjDsPe1JrNhUhmkQWjaFYw5I9ddcjYaxzt13KUwXM\n" +
                "q9Ktj6f6WQfxbJKx4KkSD/naKX9h7zUtuYguNZ7ApGweZ04dDwHztHTEkuICmkY/1VGI8c7R5iek\n" +
                "JOmYMzo0AVTZCJrYk5sKXi4YRjjhJkdie4PyXGyIes74VS95gnMFrRCcTzi3GvhZw6JgOMERjCGV\n" +
                "XBpGGBR26dsm/JKxbCKUU8Q9l5Ocy0JGUlZ4Cn0A30ZZ4/4mfmTBpIASOKGhl7K2yzmJvc4RF8cd\n" +
                "I55LXE5SdngbvLKeiW03dDLr0GSuP8EMNrzdnNBLalG/cY6422UeznlKY7uSIAsI2rO5TCZkNMwz\n" +
                "Qsn21QUJzm5smnqcM0HKOeLOtYQVMReML6SzUDpyoTlPCCwTBR7Cvxsbw0Lfq8PkzRDI0ZY4p0H+\n" +
                "KZ07C8JQLIQnShwzg/GbahyBb0eJlHPT8VsWg18fq9swhvLG7N6c04seuJ/l3MLFWjMyjvvqJX8C\n" +
                "VxsQlzWkaXoEf0BI/dLG6vSs5dLkbD9FWXmGi46if7JFvIF1kHdmyrlq/29qKNUL8LPqDqwWvubc\n" +
                "PGwtPwOeizMUlKexD8lmJVVuvUnTkE/gBZKhE8fGQLL8hS1xM5vKUZ2rQdtpaBeEjwoUMmIoN8qx\n" +
                "G6UHAUqZ0z3rAArHLIY+6+NS11qaQR3raZxOgc+UwxFOcKneg7p2I6VTKONSK2rW5OCmVuVmFY4p\n" +
                "yxZASYUdy3ZIG/xkrLcJe3iCPPE3a8QLLwg/NLgk5YBayjniWxoaG8d+y9anN15wc17GlYb93AO+\n" +
                "cw7TCwhZQT6aiTh1vcGZ4a3hO5pN48Qaj6bgCwGD5kOb8xpXMMy51SedIq5MVNXqmOWl6alvkbYZ\n" +
                "cVkk75bVltzUpbV6WC1O6nPOGKuRrt7zdBKw/MdAokRuJieOimUrPy3QGAyftjy0jlA+tTmxtzDs\n" +
                "GOcqs4d17mGsySSzYMo1TFqwBNEkX5dJyDM0iM7dWmBIuLk5+TJxbYsjZX/X0ut6g30z8CHjnNpZ\n" +
                "6pqn6soW+QaP30wL6BN6luxbXc7xud9xrn45w3TFKOfQDfDwSW6rKRohmCU6UjVdEcGmroFpG6si\n" +
                "sQzTvDn7Ir9Uf/lFNzEyqYa7jsMJg9Jh3NNMkor+LIJ887IH6llF2JMCCoSn6Jy4eME952i8sM9v\n" +
                "5hbLdZnQEY37uWSqxOXnjV+mgDGBaGoIL4YTpweWaBAxQ1SSICZ1Occ/CEO7uMW79eGf9c+Zo2VZ\n" +
                "YOkCRBCnpnekeHI2/gN7dKPYsTBWsnCMa2fpENtehUXi/0IRiYrrAczgJW7mKSqKkijDoqM6sTk7\n" +
                "wcnG/VyaxDgYc6W2oUZSmoxGmtyccZ7TBE/1BS+mpMnLjSoecY4TSQM6mKN6NhxEXJgxLJZOzZUZ\n" +
                "kG3cJPkbrgCh6ja7kgUfAkSVrICtiemhXyZAEaHgXzQq0CrCMOeSjpNVaDPwTb1QvXkWorlhHYXV\n" +
                "UZSMp4IkjOtyjqdrIWvccMwe0amsM37bFpUzOx400CZzY2NReRJYEre32hFwDLeyCb5ex5Y4RC87\n" +
                "uqrEgusXhmPLwgO/8rv9KVGzwlaPOVdgmliLEqRMnbhYodu8EUlm/HZoG4f5vrc4iTQPco4xhHkz\n" +
                "CavqdlOrZ81e4jxT+saDBNmsyGRRaPq2F0uv+c5Y4Jy2xuF4IOqcjqhxO19tZjaIe02omBE1b5IF\n" +
                "FbE142JOt6D0IefA9A50GcOpMc6lKsQOJtTa2XM2CjZOYT6qX1g0zmSFn0kTgwlLVKx71hK90DQP\n" +
                "QnvLcGzpL7CidNpF2sYBmEoUIOom6EqBmiUTh1GiDsfLBvzqiqXxc/PgXEEajlCAcTjy8FyQM/VT\n" +
                "3EXlIb8CD74VTV6TNQE4TQpqvPh2cI0vG83ERznH+rx3HUYaS3OsYQe+ek+Mm7VOeJU0b+nqi+XS\n" +
                "+LYx4vxgVuA0gtqcdkOF8QQSq1DFYa2vAVBlY1zmaQpiJI9cAc0gEa/HiItjEUqZJArYFuMQWTei\n" +
                "jHXmZqJgw08oF5wllhbmiWRz/dch+gVEjhGXxsr1oti6nYpa7TJqxoKpbo/Mqp8REtOlnpcqpnYE\n" +
                "DjhnhmOMuOfeLvWaGbXtW8VQZQafeyqVeIUhyujmxqHjOnEINI+LbJ5LE6AOEjfUiqoISqUNnBWN\n" +
                "U+yO4OXUMo9GAYox1V7SOrSiE329ZNU3IcB3EmdAr93MuTGOSsiumJ5tBapicGr7ONtM/OFja6lp\n" +
                "03cSt4i071WZOZVSspBxG3aoo8cXQ5seQKDSOJLDaoZTOjfSHpok7QsXmYC6hJRKXtW7bX2z9D+Y\n" +
                "LBP/e6kzQgZuTmNiyw+JO7AthTOWSyeF4HryKrqRecQiWBrAlftdIE2fLCvq3HEuOhx9nRPufULc\n" +
                "QQuqcvv5Su52qhrRTNpYZtnpRXwnbMqs/+G3DjLxbxDL7r44XsVn/6YYwfr+IhbRatvNsk16v9kY\n" +
                "ZCmlqXzb9kH2SfmcOHfprI5UsLfj8EWnqlaAYG6W9GxNKR9Htiaic8zvl7jliQhl09LUMxkCXfRK\n" +
                "qIQ/AT7SWSVA7ttWrcGRwploLW+qrnuxpMh9+oJYdpdzR42VOl8RIsgZ7mQxmQwhKwKbVlab7Ydc\n" +
                "Mkra83N2quAjzsVu//1Muyt0i8K1EgPQsdkTFIlcauYRpIQPiWgkbTLa2MUtmw+fogp/6t5fneUm\n" +
                "jit75fkqgtjZKSaWPFNvvl3yWAl2tHAXHWgxn+xZSyfw0HnOPYAKN++16q4JZi9pV88cETqY4sYZ\n" +
                "tBUmJMPrn+P9NjT6Mh/bEieEfVSPDZXOHVufJ/fKvbBa3bTsPtiYxM6CC9RSXwmT6vLsrU7CB3rE\n" +
                "iaM7TRuY5k7mmueXom5iKt7SZfIsXzfDi8xZHsnlYlddJ9FJtqd6BggcTCi6Dkc5WfyEOwV1ArAg\n" +
                "cEB3RYzRInL0qEGWJl57S68BZwKhshPXqqgQlw7n2H3jOfKJRZH3ZwkwKj1z6HWrli5xOjlHCBUI\n" +
                "T0Im6QJO4pzg0Artzc4bfeMPtWfJtjjqZO1MxnSpC/MpLNMFAY0VSJNR+n9loQTrkbUvVU5k1d8i\n" +
                "FOmsiBIHPAoSepmUeajVclUcQ/kx0zBTvsyXjiV9qs7VfhlE61LRyEbZbGUeVRohwhCkYtOEL+2i\n" +
                "iccyNTqHceB7xSO8bZYZju1XeAJqvnTl3HT/1ojOhSoS2fLrB9NiKjZdC6ezPS1NT5Z2Fkx2Nbs/\n" +
                "qMcr/ZUrD65DvD/uOcV6HixhmvqLm8pEgeVl7o6UWc9J4aXR/NUrCLrXkCZ4Ey+XuZngrBHLR5Ws\n" +
                "C5XqsJLWr9d2mahdNn/CaJy07ic0ui4PEfr7jkCxluTGXbOP48o+D8UZuDw8Bc+rgqF/nYUj2Yjl\n" +
                "vd5Jspx2F59BcMPmGR70KZUm8G6FVVC6NigJF/rLB03IURKBWaIavHbTuVpDYWuK0JLUuiErkUbG\n" +
                "n48QvIeF1/p7pqDF4NZko/WhCbLJxnVRhsgYw8HCz2bAYRwWv/aqgRCATab+GTy987IgVCeLUTpB\n" +
                "LDPv62kqp/nHTDZhwVqlV8Ueg7wD+xqYtQfQtMKwWMK9OuRHQRH8h/hmx5FrLPy/uTGO1umjWBbv\n" +
                "Fw9/+3tzxDdzKE/54WAfJC3t7b8v3gvixfV3oJMsXil6LhTgpfeVtUkLvsSWGBCiGpXuyb7athZD\n" +
                "AcN+WCASpZFIY9vhnlYiQww+hJSS6NVucsEw8He20PMiV/3zFZZL7R1Np6pv1gTVfAw8iACb+oVb\n" +
                "N5/7MeJeBShPYVs78Lci6iRsbUZAuCWlOTfzhR/ahHsbhui+H3C+3Um5Pze5F2FDbutMeRRSOxCW\n" +
                "ecz53+LczBMWBwal5A0FuYGZSdZ4nxPmoKngZptSOEj5Oc7tiBPndKxzr9od/z3ob7Tl9DO3HXEv\n" +
                "de6HWgmrdW3+vnKHP3hsz7JIiPGrG5wm6BX37ou0Xes9+tUdXTvE/T7ncqh9WXuD/yMfyeP2+cEj\n" +
                "HBj+tWfxUncAG8X+HnFvWDbGNI+PAQitgUR7FgGpzq6S/1qrm2ZFHPn+/T3bICjCxytzbhM/6/ro\n" +
                "39a5tVsdbnhRo7dcq2pG7Iv8pD2Lztcecu7O2yR2b/SvU6z2qmXgXCQWGPXxww2Y5ZF/9Fs7zkly\n" +
                "dNi3jPneNJsHufN/LNQDAKBe/iO+cdPIOS+WILKUTXRBinl6das2EHhFgumZ5e1ZokzOHZb3Ierl\n" +
                "6AGTfLnDvltOs0mH2Q9Q7MMhiZmtBtnwQAoEeygTmAF1hPhXX+RHuASHqxcKqN8SAY4dnUsEPL0y\n" +
                "KLj7nF/M9k77R0mKyfhyqVx/6+oEQhSVb2T6VrGsSaMzV2lY73hATyumUT9lOHcVqd5xjtd9HxKn\n" +
                "XZL1RA0wYEgzuBfjLN2tubMxCihTHjnHPsvjTP0GVpgvC0kgSiG6AGNnNmeZeR0nWJo3RuHBFbFC\n" +
                "j6tXLsmEmq2BZgyLWQCDWfplv9VtYh9FXAPpwoSX3HL0TREZywELINoP5LxXw9SeBXHLcQw7yI7i\n" +
                "hKp1rpqQZHcTEtKa57fkZY2bWHG86g46Mb9YgAZuPrIUehmXa1Ct20qfbFs3tjPYg2eDWfy66EQJ\n" +
                "jBXh6ptqaJoIKKkXDpEPPB+wlSdrwEjwzMyST7Mwe7K9286IWRic8guiYKgKB4J895OsvCHoz159\n" +
                "ImsPyZlfzSv4EIIz4B4AdOSzWUXJwirrttK3SB3pe8JgPaXBRF/OX8fKNUFV64AYAU4RnQBmKhhd\n" +
                "MC85UrkGvmgIDedylVDrqlYyUFgJ0r3pg5dZnsEIgySFTdjxB3XN/Iw4takTKRQOgrEDGpVzGumb\n" +
                "crf6Dw0OksbCG4253FnLOp1M8xuDxE3tStUXn4ybMnUz2Z2CWkUKD9lbUYwJWiTyARSiWvGHvciy\n" +
                "hG9ba1nXHtCpB+czFzGV8K1XH22uJwRA9BPnojTlERspquMl0g/Rerg14JIozUbMV+H43loW+YHB\n" +
                "ZYROts98vxxMh53oY+lIauA8ZjiBlTAG0qyVIWb+oE6te7Yo4iV5OAJxvtsBnjEfKCNodvd4UzOi\n" +
                "q2DokbWOraWOunTWKxeDBL6gCmz1A2te9GRgPLu7DnHTmfIPT1vWvjUoa3voTBR2kZ1p5hRM8jfP\n" +
                "foECFnhF4zgQy9hyjgaBTew+5ZFyy2lsW8nNOr531T6LiG/L6KwOigmSMNoHecd+tXLuSqRc5YHz\n" +
                "8dhLeapF4fEZAxp0fcNLa4kCHtqJNEFHU0Dzf2WtYQspoSb0dTIxymrx1PQwjVcNbIDX214UCVDH\n" +
                "okvJfEYWOCRROhRjrhbJRiChh5oQSC60vtOGzYSYsQtA+9iq7a4DXOszuO+sb074Jq7JxvRAIAbE\n" +
                "3ZFzApSwG1c/5/FNw/M+50hLfSQF3rOCzz5WLScwLvLi+INQV9HoXC0kJqLFLjC0w6HYjnM6PS5v\n" +
                "qZvT4K2HWz4hRqHzD1kUndEbMCjZ5ul1bp/EMhEZUdwa2XUjnOsr8mU4FcuxhiYM6l05att1oOjQ\n" +
                "DBFnhPKFztHeHtaLzxo8Z+qkx+ApSFomgAFInklA1gN+h/McRidece4pCjHk6ezy0nc6d49qWPEr\n" +
                "dyaOeCDZqaRlYmdiCFrwwn5O3lX+CtN3cwXAC2LDZWjjWc+h/kCEItGlcI9CIIIZPKPObRZK0Zjf\n" +
                "RN6x4ataFoIw+5xLvDB/CFJvC7MPmY0sMjBB420k/FJM3AhY5KxGrCywXkfg2poSDQM6/ZAN8kc8\n" +
                "ndkA47W1fMqFs3QoOLZMBOgYj+1N+sLJkKpcJZAp4M9fg6Q7bEY7fVBAboC4RTOC+aCgDFoVQG/H\n" +
                "oe5FwW9mk2r6oKm4WktjUPhGEDwTRm5PhqeDodQWtyP7SvcaV9Dbc1m/LBsb4HUvTDVrIhccAltK\n" +
                "zsmJVKS0xXwjeXedJREXTl/vmfsicu3fUPbcLgzuiyUhXbqaZ9pIfcYeCkhwNXy48sRG8FonBzpn\n" +
                "OEdSGTmzpa8fcWNULjX3f2ctPa3qrA/w4zpsGbvoOS1QiOdKPa59NutNZ8rngnEXkaA+spXVZ/a6\n" +
                "kQg6GNkDrV0CsCXOsJ3iC0T25oY2RL/q9WeIgRIuhqihdPCNyk3KXOsSPZufI87dLwgyjlyRot0J\n" +
                "/JBzRXckoCkDO248V0DdYwtx1SlJYIlNKDBCkZROieSEPOwBooYdI2vXeC52eunEI17lheqGq6Es\n" +
                "FsRIHIOYCXumy3PEEkIzWeDURNosPga2SteeE3/uMbAXWIpvoYPeR4psvS4x6NJeRTALYcw5Juhq\n" +
                "OSdnAsMsEYqEkgx/CRDf77Jju/ZWLu21o+dOJk67SlFVOI7AsnVAObTBRcs5NipXG7shtIeqKlNy\n" +
                "4hQoVep3Wf1ler6+BJeXFMuKJVZk5+TrjkuKoxLguPuRHKJM7RAPtbwhCntmkzleZp1o1lKOazN3\n" +
                "cMC5uxD3Lu9ZDNsqzupgYzxfLw41TaRhDRLRM8CJ7aLtsLWaqFjtLoycwl4D2yDJWonMI7HznHJd\n" +
                "ymv00psE8oJzrNIBRxCodujgCpvZlGpIj68yx80c1LgU/ZwPakaMWIc3nJOl8G+3QZPN7zVSMXqB\n" +
                "iSHNTx5fMDsFnqdRtID7fJUYRHRl5nyOwi0fVIJt8nModXrpltfxl7d0WUutqwKwiPvVj2RLTyAi\n" +
                "PUcrYjLtD5Ofs7mOTORxdntoDXll4+UN6+w2WOtjiustwCPe3pSe4zWPEynblVMYRmSVc/WtVkBU\n" +
                "gA37FP28vuDcpsbtNXGvsoKRlsX9Ek2BStOuQTlnZnng4Nr4j8AlHiYWOPZjiTfOfM063wrkGDFb\n" +
                "E5WRBUzP1XCOomEQuMajTo3jjwwuSQL0Sufq7g/8Sw/sT5eDRiwPU563LRv3y3UXUWWOUvSg8TkG\n" +
                "zkzY3mJSfHN8xiSXWXzl6wbCr/ctmXpE8XMS3QcWU7uCCbICEViZTRXOXY/DL2gy+Q9ad3Cp83YW\n" +
                "/mPOWe90NeJGPUeuGFdQwwvBphna4x+RNPDVKVUCjj/EWYEbRl2aRpdWUwBPlYaU76pPGnCJOafO\n" +
                "ral/qCHAy5lvz26FkPX+9ik6Tf1VztnRVy7KYxThp0ycUbFr4/XNJOarcxYZpWMGGwxlfoV+vSGO\n" +
                "ZXGHMvN0osysimVmgEi57AOHpiSdL0+qs9DpKKlrN3r6AucaBFbmByQsE85RxrtmYzr9I/PmlLlS\n" +
                "jvcmXxPrdHjhbr20wOVzziWbUceWc8y/lV4b4DG0J8y1GN+AtXyKYFaX+crPfewKaMiywiUIW1nk\n" +
                "X42MOF71c1EQzijwLH/hjVjSFH39raMgzOv5xiOUbUuKeyjQvOMcXyKIzhdY5wLx0Av/Aw3Vm9MW\n" +
                "qlyejqy8XLYKT/kZcUVKSIhITyGYF+ahnyO6NLYkNSPOMf8pj3+LkSSog611dm9WuaJgfkZcYqEy\n" +
                "ZryZ5UEaVefqUWTwFfMk8eVXnf96e2IPFeD157r7knwtQuHJR60/UYMQQ1AbD69au0wRCknglaMw\n" +
                "QZPg/vb8RfCfrmBqQdYXwq+k4uUZPGfDGYNkCzbLB4AoKue4vs+w+rUTpzPDhcDBYnaMCuvcl8Qy\n" +
                "R1pLoJyDGR8vBUQ2tiTubUIaLoWQCb331rI2T4sNukVF7S5WH/m5u3KOc/H6zBY9oTXUyBLDrzak\n" +
                "aTjnx3TuSRcVQ7XDXToem/c0pfuEOLCW7WSjVvRyVBxwrsAU4EethuaCRgJe8OMD1rK2bNRua1TM\n" +
                "Rdg/4BxP+AdOcNh6BHZbBvEJNlUF9MuE1VakJXR5d3qwZwn3465Xn96+v+zL8Ma3Q+JmraW4BHYL\n" +
                "RK+dNubK+zau9mZWDxk4dvZ/J0eXtdwqqWY7cHSaLmjMuShxRlBnJVWVfm7025T/Gr5xIBAHIhSW\n" +
                "Qi8528aXm+xYXcHpi2aKXWAtExfGKUJlCUs/qoHgRoGL7QllouRnUOdqW6g65bIpCWsM2NHy8nct\n" +
                "mVHXZROWJfVA1zyIWJJCKgNlLmvYWtYmG/hUh9BWKGhUNH4NkrblyHb9yk4hqg/gRXXOTHJW9We/\n" +
                "wfXCwWBmfsjP3VnGiuuhtMHAzVz/cKKR7Ul2ikCi+qtoD77CUyw4nAA5Gy6xBzBPTlwmMbkWBsJd\n" +
                "8cxGQPCWIXxcRLOEhqI/3GOF2b31p1WOeGU5zQZxWKIB95mRTuLPYGup+pKXkhXewGagdKXzywTH\n" +
                "GaiHLSeFYuvDsl1r6HzCaCSKgZW0aTxCkT6gqYJ96591UzG5lodJenC4BumTTFztt29sCYWclbZm\n" +
                "5yX0dpLIgRjqZCUXco8SBt3A67xVGpYHzuNzNY7Z1mmOp714wmTFBomek2uInf0iRY/iVms5ifdB\n" +
                "l3Jq3MW/crIXZDMvkyxzMRWek7zpY0o5p1H55JJEqSAVgK62xTlaU6jzErUedAaz4mBbKS+kcSo+\n" +
                "HqEodTBmsk9am2GRi51gVW898ejQZYMV0xHO8U9clcP61qT9buK9qiDS9CKUbDcHTy+SBqsBp8mc\n" +
                "DMllfyBrx+v/UaHPPIEoYF1EwICM/owojsEYRF5YK3QKlpl+nnNoM1mfebOlzVZK/Di8X2qyFUAK\n" +
                "pK9EosTxI29nEPiZHGj4ZsrATupcbWWR5befxSOm3fEvq+v28g/jSQ2G6TGQqQ9qYeVTMtNPBveD\n" +
                "HmW8FOPF9bYn/6gl5RgnpkHmN2gmMmr9OmmW+HsvkFDD+1NOXJuXnLgWqJ3P37TReoNAmAK5AwNE\n" +
                "KtYnswcWrZXZEp4y0OU9R4Vt71rRWur9dUQ+oC8FnkKNJizRcPNqeEqG46qV3uIcGaj4Gufocruw\n" +
                "BffZWLnXYMKfINlAfkCcFSfZPnCKIJMJnuMZz1LsOSHwH3GOIyaIwy7V0/gv71d0f9wf5Vke99W+\n" +
                "lsf6vz4v8Eq91Tfq64We1M/CC0/6SHniO/Teo8APPD7oE6uYYmIuPewbH/zW39eihif9bU7et7N4\n" +
                "xC+2bNbcf9nl/XXtzlfcqXiNbun7v9KylCS6Kf3FQnauCSFcY7DeaaeXn6fx9zYlq1tm41oI3sjm\n" +
                "BwWz7gCUfl5EDAXJOcEADgp9v6MhSflTy/xxi7Trd7Wb/k1V9KcNRjOH3zLLhoY7bOfO4OX3yyVG\n" +
                "Cflg++mfb8GUNUMncK8TXmDGVuBzoYq7xRa/2IrWUkz18jQvPnnmZykr8sufjhMCxprVra/pQq4d\n" +
                "+0j/tl9Ky18RAiUKpmvUcrS13kjT2tVV0+YftMHn2kNLB3e+YX95h923NwfJOxf/qtAnLaZ4sF6r\n" +
                "4IxwqqmFS6//NUzTdo92B8E1dGk2tzwmVSgrK8vI6v7x1pG3e9QdasF+Oh+6F2LctgJXalkV9q+g\n" +
                "7LilgOsOkMIJJkTrPAKWu8NVaOhaNPVZySl4D1dsWY5XNf1N7Z4DL3kkGhGQX7xbQiS4qm4/OgPt\n" +
                "FcgenkD5O9qqQYvbXKYFaaQJGwq868VE/nRfP2v1Oinem5qEOulQGelmv8T0/5WsbbvrdUL+V0j6\n" +
                "r/3X/mu79n++TBPpCmVuZHN0cmVhbQplbmRvYmoKNyAwIG9iaiA8PC9UeXBlL1hPYmplY3QvQ29s\n" +
                "b3JTcGFjZS9EZXZpY2VSR0IvU3VidHlwZS9JbWFnZS9CaXRzUGVyQ29tcG9uZW50IDgvV2lkdGgg\n" +
                "NDg0L0xlbmd0aCA0NTQ1L0hlaWdodCAzMC9GaWx0ZXIvRENURGVjb2RlPj5zdHJlYW0K/9j/4AAQ\n" +
                "SkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\n" +
                "AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\n" +
                "AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAeAeQDASIAAhEBAxEB\n" +
                "/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQID\n" +
                "AAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RF\n" +
                "RkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKz\n" +
                "tLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEB\n" +
                "AQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdh\n" +
                "cRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldY\n" +
                "WVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPE\n" +
                "xcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+/iiiigD+AP8A\n" +
                "4PnP+cXX/d7P/vo9fv8Af8E8P+U6/wDwcVf94jf/AFjzxtX4A/8AB85/zi6/7vZ/99Hr9/v+CeH/\n" +
                "ACnX/wCDir/vEb/6x542oAP+DXH/AJQUfsM/93M/+th/tBUf8E8P+U6//BxV/wB4jf8A1jzxtR/w\n" +
                "a4/8oKP2Gf8Au5n/ANbD/aCo/wCCeH/Kdf8A4OKv+8Rv/rHnjagA/wCCeH/Kdf8A4OKv+8Rv/rHn\n" +
                "javwB/53rv8AP/SHiv3+/wCCeH/Kdf8A4OKv+8Rv/rHnjavwB/53rv8AP/SHigD8Af8Agof/AMoK\n" +
                "P+DdX/vLl/62H4Jr+/z/AIOjv+UFH7c3/ds3/rYf7PtfwB/8FD/+UFH/AAbq/wDeXL/1sPwTX9/n\n" +
                "/B0d/wAoKP25v+7Zv/Ww/wBn2gA/4Ojv+UFH7c3/AHbN/wCth/s+1+AP/B85/wA4uv8Au9n/AN9H\n" +
                "r9/v+Do7/lBR+3N/3bN/62H+z7X4A/8AB85/zi6/7vZ/99HoA/AH/gof/wAoKP8Ag3V/7y5f+th+\n" +
                "Ca/v8/4KH/8AKdf/AIN1f+8uX/rHngmv4A/+Ch//ACgo/wCDdX/vLl/62H4Jr+/z/gof/wAp1/8A\n" +
                "g3V/7y5f+seeCaAPwB/4MY/+cov/AHZN/wC/cV+/3/BPD/lOv/wcVf8AeI3/ANY88bV+AP8AwYx/\n" +
                "85Rf+7Jv/fuK/f7/AIJ4f8p1/wDg4q/7xG/+seeNqAPwB/53rv8AP/SHij/gr/8A87iH/evn/wC8\n" +
                "zo/53rv8/wDSHij/AIK//wDO4h/3r5/+8zoAP+DGP/nKL/3ZN/79xX4A/wDBPD/lBR/wcVf94jf/\n" +
                "AFsPxtX7/f8ABjH/AM5Rf+7Jv/fuK/AH/gnh/wAoKP8Ag4q/7xG/+th+NqAP3+/4MY/+cov/AHZN\n" +
                "/wC/cUf8Ff8A/ncQ/wC9fP8A95nR/wAGMf8AzlF/7sm/9+4o/wCCv/8AzuIf96+f/vM6APwB/wCD\n" +
                "o7/lOv8Atzf92zf+sefs+0f8HR3/ACnX/bm/7tm/9Y8/Z9o/4Ojv+U6/7c3/AHbN/wCsefs+0f8A\n" +
                "B0d/ynX/AG5v+7Zv/WPP2faAD/gof/ygo/4N1f8AvLl/62H4Jr9/v+d67/P/AEh4r8Af+Ch//KCj\n" +
                "/g3V/wC8uX/rYfgmv3+/53rv8/8ASHigA/53rv8AP/SHij/neu/z/wBIeKP+d67/AD/0h4o/53rv\n" +
                "8/8ASHigA/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKP8Agxj/AOcov/dk3/v3FH/BjH/z\n" +
                "lF/7sm/9+4oA/f7/AINcf+UFH7DP/dzP/rYf7QVH/BPD/lOv/wAHFX/eI3/1jzxtR/wa4/8AKCj9\n" +
                "hn/u5n/1sP8AaCo/4J4f8p1/+Dir/vEb/wCseeNqAP4A/wDgnh/ygo/4OKv+8Rv/AK2H42o/4J4f\n" +
                "8oKP+Dir/vEb/wCth+NqP+CeH/KCj/g4q/7xG/8ArYfjaj/gnh/ygo/4OKv+8Rv/AK2H42oAP+Ch\n" +
                "/wDygo/4N1f+8uX/AK2H4Jo/4KH/APKCj/g3V/7y5f8ArYfgmj/gof8A8oKP+DdX/vLl/wCth+Ca\n" +
                "P+Ch/wDygo/4N1f+8uX/AK2H4JoAP+Do7/lOv+3N/wB2zf8ArHn7PtH/AAUP/wCUFH/Bur/3ly/9\n" +
                "bD8E0f8AB0d/ynX/AG5v+7Zv/WPP2faP+Ch//KCj/g3V/wC8uX/rYfgmgD9/v+d67/P/AEh4o/4M\n" +
                "Y/8AnKL/AN2Tf+/cUf8AO9d/n/pDxR/wYx/85Rf+7Jv/AH7igA/4MY/+cov/AHZN/wC/cUf86KP+\n" +
                "f+kw9H/BjH/zlF/7sm/9+4o/50Uf8/8ASYegA/4PnP8AnF1/3ez/AO+j1+AP/BPD/lBR/wAHFX/e\n" +
                "I3/1sPxtX7/f8Hzn/OLr/u9n/wB9Hr8Af+CeH/KCj/g4q/7xG/8ArYfjagA/4Ojv+U6/7c3/AHbN\n" +
                "/wCsefs+1+/3/BID/nTv/wC9gz/3plfgD/wdHf8AKdf9ub/u2b/1jz9n2v3+/wCCQH/Onf8A97Bn\n" +
                "/vTKAP3+/wCCh/8AynX/AODdX/vLl/6x54Jr/IFr/X6/4KH/APKdf/g3V/7y5f8ArHngmv8AIFoA\n" +
                "/f7/AIOjv+U6/wC3N/3bN/6x5+z7R/wdHf8AKdf9ub/u2b/1jz9n2j/g6O/5Tr/tzf8Ads3/AKx5\n" +
                "+z7R/wAHR3/Kdf8Abm/7tm/9Y8/Z9oA/v8/4Ojv+UFH7c3/ds3/rYf7PtfgD/wA713+f+kPFfv8A\n" +
                "f8HR3/KCj9ub/u2b/wBbD/Z9r8Af+d67/P8A0h4oAP8AgkB/zp3/APewZ/70yv4A6/v8/wCCQH/O\n" +
                "nf8A97Bn/vTK/gDoA/1+v+Do7/lBR+3N/wB2zf8ArYf7PtH/AAa4/wDKCj9hn/u5n/1sP9oKj/g6\n" +
                "O/5QUftzf92zf+th/s+0f8GuP/KCj9hn/u5n/wBbD/aCoA/gD/4Ncf8AlOv+wz/3cz/6x5+0FR/w\n" +
                "UP8A+UFH/Bur/wB5cv8A1sPwTR/wa4/8p1/2Gf8Au5n/ANY8/aCo/wCCh/8Aygo/4N1f+8uX/rYf\n" +
                "gmgD/X6ooooAKKKKACiiigD+AP8A4PnP+cXX/d7P/vo9fv8Af8E8P+U6/wDwcVf94jf/AFjzxtX4\n" +
                "A/8AB85/zi6/7vZ/99Hr9/v+CeH/ACnX/wCDir/vEb/6x542oAP+DXH/AJQUfsM/93M/+th/tBUf\n" +
                "8E8P+U6//BxV/wB4jf8A1jzxtR/wa4/8oKP2Gf8Au5n/ANbD/aCo/wCCeH/Kdf8A4OKv+8Rv/rHn\n" +
                "jagA/wCCeH/Kdf8A4OKv+8Rv/rHnjavwB/53rv8AP/SHiv3+/wCCeH/Kdf8A4OKv+8Rv/rHnjavw\n" +
                "B/53rv8AP/SHigD8Af8Agof/AMoKP+DdX/vLl/62H4Jr+/z/AIOjv+UFH7c3/ds3/rYf7PtfwB/8\n" +
                "FD/+UFH/AAbq/wDeXL/1sPwTX9/n/B0d/wAoKP25v+7Zv/Ww/wBn2gA/4Ojv+UFH7c3/AHbN/wCt\n" +
                "h/s+1+AP/B85/wA4uv8Au9n/AN9Hr9/v+Do7/lBR+3N/3bN/62H+z7X4A/8AB85/zi6/7vZ/99Ho\n" +
                "A/AH/gof/wAoKP8Ag3V/7y5f+th+Ca/v8/4KH/8AKdf/AIN1f+8uX/rHngmv4A/+Ch//ACgo/wCD\n" +
                "dX/vLl/62H4Jr+/z/gof/wAp1/8Ag3V/7y5f+seeCaAPwB/4MY/+cov/AHZN/wC/cV+/3/BPD/lO\n" +
                "v/wcVf8AeI3/ANY88bV+AP8AwYx/85Rf+7Jv/fuK/f7/AIJ4f8p1/wDg4q/7xG/+seeNqAPwB/53\n" +
                "rv8AP/SHij/gr/8A87iH/evn/wC8zo/53rv8/wDSHij/AIK//wDO4h/3r5/+8zoAP+DGP/nKL/3Z\n" +
                "N/79xX4A/wDBPD/lBR/wcVf94jf/AFsPxtX7/f8ABjH/AM5Rf+7Jv/fuK/AH/gnh/wAoKP8Ag4q/\n" +
                "7xG/+th+NqAP3+/4MY/+cov/AHZN/wC/cUf8Ff8A/ncQ/wC9fP8A95nR/wAGMf8AzlF/7sm/9+4o\n" +
                "/wCCv/8AzuIf96+f/vM6APwB/wCDo7/lOv8Atzf92zf+sefs+0f8HR3/ACnX/bm/7tm/9Y8/Z9o/\n" +
                "4Ojv+U6/7c3/AHbN/wCsefs+0f8AB0d/ynX/AG5v+7Zv/WPP2faAD/gof/ygo/4N1f8AvLl/62H4\n" +
                "Jr9/v+d67/P/AEh4r8Af+Ch//KCj/g3V/wC8uX/rYfgmv3+/53rv8/8ASHigA/53rv8AP/SHij/n\n" +
                "eu/z/wBIeKP+d67/AD/0h4o/53rv8/8ASHigA/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fu\n" +
                "KP8Agxj/AOcov/dk3/v3FH/BjH/zlF/7sm/9+4oA/f7/AINcf+UFH7DP/dzP/rYf7QVH/BPD/lOv\n" +
                "/wAHFX/eI3/1jzxtR/wa4/8AKCj9hn/u5n/1sP8AaCo/4J4f8p1/+Dir/vEb/wCseeNqAP4A/wDg\n" +
                "nh/ygo/4OKv+8Rv/AK2H42o/4J4f8oKP+Dir/vEb/wCth+NqP+CeH/KCj/g4q/7xG/8ArYfjaj/g\n" +
                "nh/ygo/4OKv+8Rv/AK2H42oAP+Ch/wDygo/4N1f+8uX/AK2H4Jo/4KH/APKCj/g3V/7y5f8ArYfg\n" +
                "mj/gof8A8oKP+DdX/vLl/wCth+CaP+Ch/wDygo/4N1f+8uX/AK2H4JoAP+Do7/lOv+3N/wB2zf8A\n" +
                "rHn7PtH/AAUP/wCUFH/Bur/3ly/9bD8E0f8AB0d/ynX/AG5v+7Zv/WPP2faP+Ch//KCj/g3V/wC8\n" +
                "uX/rYfgmgD9/v+d67/P/AEh4o/4MY/8AnKL/AN2Tf+/cUf8AO9d/n/pDxR/wYx/85Rf+7Jv/AH7i\n" +
                "gA/4MY/+cov/AHZN/wC/cUf86KP+f+kw9H/BjH/zlF/7sm/9+4o/50Uf8/8ASYegA/4PnP8AnF1/\n" +
                "3ez/AO+j1+AP/BPD/lBR/wAHFX/eI3/1sPxtX7/f8Hzn/OLr/u9n/wB9Hr8Af+CeH/KCj/g4q/7x\n" +
                "G/8ArYfjagA/4Ojv+U6/7c3/AHbN/wCsefs+1+/3/BID/nTv/wC9gz/3plfgD/wdHf8AKdf9ub/u\n" +
                "2b/1jz9n2v3+/wCCQH/Onf8A97Bn/vTKAP3+/wCCh/8AynX/AODdX/vLl/6x54Jr/IFr/X6/4KH/\n" +
                "APKdf/g3V/7y5f8ArHngmv8AIFoA/f7/AIOjv+U6/wC3N/3bN/6x5+z7R/wdHf8AKdf9ub/u2b/1\n" +
                "jz9n2j/g6O/5Tr/tzf8Ads3/AKx5+z7R/wAHR3/Kdf8Abm/7tm/9Y8/Z9oA/v8/4Ojv+UFH7c3/d\n" +
                "s3/rYf7PtfgD/wA713+f+kPFfv8Af8HR3/KCj9ub/u2b/wBbD/Z9r8Af+d67/P8A0h4oAP8AgkB/\n" +
                "zp3/APewZ/70yv4A6/v8/wCCQH/Onf8A97Bn/vTK/gDoA/1+v+Do7/lBR+3N/wB2zf8ArYf7PtH/\n" +
                "AAa4/wDKCj9hn/u5n/1sP9oKj/g6O/5QUftzf92zf+th/s+0f8GuP/KCj9hn/u5n/wBbD/aCoA/g\n" +
                "D/4Ncf8AlOv+wz/3cz/6x5+0FR/wUP8A+UFH/Bur/wB5cv8A1sPwTR/wa4/8p1/2Gf8Au5n/ANY8\n" +
                "/aCo/wCCh/8Aygo/4N1f+8uX/rYfgmgD/X6ooooAKKKKAP/ZCmVuZHN0cmVhbQplbmRvYmoKOCAw\n" +
                "IG9iaiA8PC9MZW5ndGggNDQxMi9GaWx0ZXIvRmxhdGVEZWNvZGU+PnN0cmVhbQp4nMVby3LbyJLd\n" +
                "6ytq2R1BwXg/fFdsie1Rhyz5imzHxEzPogSUaDhAgAYIueXPmJ+a7/DSCy86endX92RWAQQfAvtq\n" +
                "ImbaHWKJLFRl5ePkySzq09lPizMvFLEdikV2Nluc/f3MFb/Qu46w8Y9+xr4rFquzVz87wsHo4eyH\n" +
                "HxcfaS5+x796uT+4e3NmW474fPaf/4XHs7MksgIR2QF++m4gzj3bFrU6++nsk/Btm/ehtxw7oRk8\n" +
                "L12JV/lqaYvLSvx9KI7j2DSFBNLPQuzzfjQbym1jleGjru9ZjiN8J7HcuDtRSAfSMkD+H6apWm8q\n" +
                "8STSqqy+pJUoZEPjLE/zqlSNyJRoVP1Iv2IWfVqrZlPnqfn8U0sThFwXeSpLIQuhysfvFT23UjXe\n" +
                "Sr/jmbUq8mVdNbIhVdpieUTy5w/i+Y7lRcL3HCsIuoPEuwf5Oa9XUtypVb5R5UaJSkzbTVXnX2RW\n" +
                "Pb+nfh72G1i0N6NnC9cRq7Mg9GhQdIPdl5TfDlzbTKDR/mtKa+kpZrD7kpq9CvM6/JmezU+KGfRy\n" +
                "Br2ggRFx+KpljV3XzKHR/ivLoqeYwe5L2m1YdIPdlz15t2I6IgptFtPnUfHMRNeOeaMVj/ROzywZ\n" +
                "h2YmjUbX9LFnkNCafjC+pi0i1+bNIx6NTAzjbiKNxib6trEPjWjige8bHDlmYNd3GLAcRP2562so\n" +
                "2YlzqDaOrbiPDWcvOG6mF1e3N9PrsUBwxDmDXxgj0Nx9KMGKoRdbQbSLjNstLmfzxdXNdDG9u7od\n" +
                "bGPTqrTKOS1Kiw83i4JQeO4Bbjlk9nBks7vZ26vF7GYxO7oTrUgr78b3jj3s3nD2qOGCsJsYhEcN\n" +
                "t4NSIvJDy++tkOwK/a6usjbdVK9fBoFIKCOLX/yZ5ctKY+9LNwjD6PkNbh+QAkpJuC6zVd7kf5Yv\n" +
                "3scd2ednlX6Qr/6tquWLlnfgZaPrv1PNmIKO+YGD7KkjmEe2c9oRgnjEVu9lUSGJCvmHzPKsaoQS\n" +
                "+Wpd1Rv1QoUGY253pZcWUqzlUtajWjXY8wmsw+V3vdByE2xBfCYKLIJ3TVScA6Li0RTfihySQz9P\n" +
                "VKUfDfkTeJBv6R0gtxPw+mG45UHuIQ8KeArzIP0sE6F+OMqEKHrcxLKfIw8X07vFVFzM7hZXP19d\n" +
                "TC+nL/XseGyTS/vSW9j4L4xdL8Fr7CbBv790q8B+fis39mw/8cW5eDu9vLu6FPNfL4Rrv3QrZ2Qr\n" +
                "23vl2K9c24mEnby27deB++K4HdvHjSb2Xz/AvxQ9Y57hTdzgJedxfbAHP3h+4V9u5zNY5+5qKqZ3\n" +
                "0ze3N+Li7tf/ePFWgLwR776+nomb6d3F1Vy8vb1Z/IocfS08Id45L97QHlEaO7aYT28W4v3Vxezm\n" +
                "f8TlTFxM54sZBFm8bEsArzOy5U+3Vzfzi6mYj3CcE8vbzikVzq9mN1PxUocAqYm9sZi13cjE6wsD\n" +
                "1UvivtJz9yo9gORn+pHiMLc3l1dECGdz8WZ2M7ubXmN0ObsW89nde/qk23/70M4uqMaeKSe7B4fT\n" +
                "Y9/yuKA+Jss1isMmX7Y5FW27lSfKzq8l8w2uLVMwDiov+2oUcze13KC+m4i0fapE+Q9UnBWmy1ql\n" +
                "ClxIqEKskWm5IDTTK2v8cFFk4WVHWstJfHoPsy07QpJjyacFLfmQy7pECm9azb0mom02KuNCWVJt\n" +
                "LUkOfLquqzWELqvVfU3VqZYOp0tbWXzC+WtRQT5UynVTgWdRXb1uM7wvBSpafJzjFPW3xjypq+y8\n" +
                "zNRaQWd0xBWfkwr2XDxwOUz78fyGCEBxL0mjNyfMG9mWF+5rwLXdTgN+7LACbmZvbi+upj9dzyxx\n" +
                "01JTAHrYfKtXOSnkadeWVWnMeF8Q89k/dVqt1vKP75IN2fQ2bkS7yfGUWmEIs0roFeVPWi3zTE4E\n" +
                "niwbzW7omJtaLZmgdsawxGx4cHIo8fi1yLcdgeMaCD3Li/+CBliii6quVdXA8LDao/rSdUQKJT7I\n" +
                "J9nJlcExjW/QCCLKJdsLtvyI9/MH6p9o+3UEG2Nj6BNOG4SWl+wIfDQS/cTyneci0bHOxe19kS9l\n" +
                "3/kpoGvTTxnf33ct39tTmO0HfdA4seObTWib2WBhaietFYUJ4qldwUdW5MzcQIKxywp+vMkf5dZ9\n" +
                "xGNuNFcKfKbqXO51m3SHqiQnIUdE6H3I7/kXthh2GngOfnY2tHYla8iCqZawKhveOzuhCS8wTHyg\n" +
                "CdeNAf5aE4lDhoImTJ+tUWmryjSXupOGc7RcZKUFJGXIy8uUpEs3cHgaQ0WKxg+yaCjCH2rZZm0B\n" +
                "iSmMylTVJaPBthXHnlcS/6+1f/XBx4r7Bqec9FiTQZ+Psm7wlvodfgBFsesaFZ1UgBtbfnSAn7br\n" +
                "dvgZ2loBWwTo9YtQz2RmiYvbu7vZ7RzHfSDzs2Zg7ALbp7LBzwIBVWi4g6nJMCkXUoy63PbDOpxO\n" +
                "VvAtnVrWVUYgIO/rvKaKC/ZX3MSUNbRjVNXkJVxOByNrH+njEf4lB35xIhZdx/KTPQV4tmNCATAS\n" +
                "hCYUXITComK5qQgkARodA+awtYKCmnzDahdZjhM3HJswF8pYuLbsQVaasNFedI8VatWWOCq7POCm\n" +
                "8yKR5nXaFvp4v/2wblXGybFpiw1UAX3u7kyywHe+Lqn6/6zux0/vUCm3BwRATmN8O7ZjPvznz5+t\n" +
                "1AQdHQebDDLCttXQeR15IGL28ZuGpY0qvj1UJYMEEKDsuEFaaNeHvzlJhP9/+9Hq18B0AtZNjixg\n" +
                "lA2XU/WGwJsMr1b3EmFHYda0JMM2qQJCNmrJPoZVoOMC9hnXhB1ZgfsXVIGMJ4X2c86ZOA72oVFR\n" +
                "qEcoBs5Z56pZ8mlT2TawUib/IJUU3FpnwkCCaUxpZHPCRW3bCvyT6cJNPCsIn0sX5Lx335bkkyTX\n" +
                "3W4Mj+7vxqj09/Or7QaO16ULm1gyNpkzBSSEJpTcyN2cxDFCmiCQhxxVg/zAXOyhZlNlbS1Nih0A\n" +
                "/hNQHaBBXt1xBt4ByEJ22LRwe8Brq+ovEgnmCZ+BR8kNFivUF7KS5p+G0XGe6vQuLmiVjB2Gm33j\n" +
                "TuJGiRXsg4UbQvMGLewAdTVTDUWcFuBMzgpPbhRzv1WZfwG34QNQlH6rM6hfkAiUGKtaywddgFEQ\n" +
                "iQIiVjt5x+RZmXIMwd+xlCwoIIYK6okIbazPRjCDTyv+bF3IL5VYStI3X7lgAbX8R3ni9K4VOvtQ\n" +
                "mQTEPvn0QUcbem0yvMnmO46kY4XBcifGh2ohRkXuCGxYUgYExlA26w8DsPvWr0MUjc+r4fj3tGjZ\n" +
                "ixhADWjWu3RhIaEuujSrNLmkD9khx88dBla4T5eOnntoKOLNEPB3eHIFKyNt5ewLuTZ+m8IUqWqk\n" +
                "vvzaryqMY0A5uc4ChqVyulfavrQ0lzB0hAniDL831aR3gi0T6QqLQ8Y9fvAgtsKD9BDYURf4gMfE\n" +
                "oAvxRCoOtU9pVlcwE2wY+LojwdHAnmvtv5OB35MnHBBKMie5KifUoXN3HjaoKvlak6g5Fw/y4Gb0\n" +
                "id0JoqHY+iiJXR5k0xPacKzwgCqFdthpI6TLY9IGcZyBJibHxThM1eL/Ms3OWMihZEqrblwJvm+F\n" +
                "+wh4VAlN+1FtjPGJB8LkMPV3VE4pZUECY9noFAlH7+x5wiG9yNq3gO05QZeHXE3V7hSoQVU01Wsx\n" +
                "Z1iEZ+WZhs8BHqZU5Jba/zTqHIdQSXU/5EZOoSCe6BUNarGOGUwKqb0Z9TSfyJQuvadiGUZDXTtL\n" +
                "WIAbA027pmJcZ6EuH5zQgm1FB1TlUA2DQDNZo8dXCEXZydSvdaev7tJhhz9PenczT+GsUp/2sQKO\n" +
                "meXhRh+q7mLkhBldz4r2mza2F/RFh+ObooOvXrryKoM9j6ueENXszHZ75McMZHJVL5HeWloCQa/p\n" +
                "BGja6uvv+YoV74Hg2UK1SNcUG3OajN1godQQBBDOnHsmz6Ry4jN1Ry62HbJxPTihFe23bmyU/VGn\n" +
                "By8xxZcxnGpeC+e3H3nT/py9eibCNZ/Val01edr1JKp7xKJJRiiPsupvgCFvO7erXQdzM7rtqixg\n" +
                "OuLkHqy7JXemJPvEBC2rCso8qq7lijLdIMV1XkwOjbLnFXkJ0U5sNq4OO7GiA555TB0rRfTbcDwc\n" +
                "peXiCtpZyka7NGUOFAu6L3LPvEDu8MATHmq71kFHKbE7kItCuuHmvMe8mgywMn0YxhyIomTXy+SC\n" +
                "lftgetafmiL3CZGLg6rL38P0POHSkOqfB6TxVtaMT3v9gJJ7eVRQ6AQBCxS0EfcBii3o72aL0eM7\n" +
                "SWDF+0yPLsL6asg2KH9F+FmqDcJqN3kNKznKr8Df2nQGBtT0w7DfhpPdt7r7yc6sg6rpHhpisA7q\n" +
                "D4ogh1YnHQPuqDWh6kedWrVeqr6duGUQOrFqXY/rgb6lsc/8nMALOj2Ejq9bi6aseVX13o+gIhYo\n" +
                "uh6oWqe7LUKmS/XQdYy1GdS4xASDzThJGuJ+hNMYeqShjAur7yWPdhotmhPvJFpY7B2xnxMKcMzX\n" +
                "VE4pYL8jYeT6/6Q44yeLfCuOThbVThhZcfJcUe0h+N9Rq6FPspfyVA0JsmQlB7GVxG7XcvQST6v0\n" +
                "kikK+QHnbKIrRaU7XMQX9L2CrDffUelUIjA5/lo9idt6+bUEzxJO8MpJkoRwRKBc12k6V3Snwe8d\n" +
                "EZ6Vj4Ahtibe6WYF1Zdqp72nS3ma/gDnLXKK2O6qo7vCGVdD4FnJPolxbD/sm/ZOrCEmq1JKH5vK\n" +
                "CAHvoZpXk3xkcfjMB7pH4julYe9XUYNRzCscmPRHhHcDV+k860ks4EzLWj7gl7k1tSb4+daaITDM\n" +
                "FEqQphvHNYYpTMgQD7n5siUddrPNnkduu074oR9ayQEX8v2ezEW67TRtcm4JQAmyeKw6cgOYzx9y\n" +
                "vi8quk2Zo77PLI27XeudH2g7q1HhIzXQ8JXNF9nDhBp8JdSYnI/H4NbeFzgeqbOp6GJsW+k/DW6B\n" +
                "tllJoFKHhgC5T+Nq8BIrOaBCLpWXOuEkjia1RnDmoY3m8MS+oBqq6+CqPZNvur01cryp23U1aIsj\n" +
                "EBhcqZeCMtV0oAdfn9XmpeYh1S2S40UTLvNFoJ3bTfCtDXMRzAJIKbD8tjRd6Qm3QLiVI09owbWS\n" +
                "w07boRYqTvj9+ajU5qZypvMDh2cDysDfWzqQSLekSVvAT4TORCAKSl0Gaw8rWYsAkps3/32iO8nV\n" +
                "xh5RcsItUwp1FINKbyrqk3KLolB8LVhncmUSvL5q46Y/PnqsUskMlzsq4L4m9CqciLkC4O+jQoKl\n" +
                "hMNYRP22D9pmKHlUww2RdHszMKHiJ1Wmj/4kqp4gm+Z9fxPaAWyXvACo4wqg7zkclGOOHXeAbvue\n" +
                "1sGzsPwiTAY7ynKpr8q3vobF6pxu0jTx+NfR73ze3nPTqZPgQqfW5vyEFlzLPrhTPKaF99+luASO\n" +
                "fC9FhIoltiNbvJUZSvATjmZ3X88Z+lrsdBtEbqRJeXfEvr3ZY59JFfp6plNs2il23Sm2u9OhSOJm\n" +
                "TYXSAbUE6h9SbWUcY3vXyfdb/T0n35Byxc/cpzcn91jb2oQWfxlBNyU0ZumqZVQFSYxj/gUN6Obx\n" +
                "BLUc4dNXVAJ0P9jLpSuMYUrjEmqiD0uVxPBemY9eCpS0/R0eq0GzbCr7B6oYN2HiWPbpe+/Ypz+D\n" +
                "eIZy+dRnBMV5Eh+hyibLWbWju0aR5XjPrXdN1nsa3FZ3vRwoC5y+kPxXFDub6d4Hg8Yv7ZclZ06E\n" +
                "U53fo8wrNOBys2Wdb7h3ZNzgMR808N+BH2MMLq//HoNuura5d1yNEazuP3egd0z1e0qdtYCCpqKs\n" +
                "vdnJ50e2GPmK6fE/nPFQHwUidujvYpzIE+dB2P3hDP1Ki9A3xnlWFHrb74t6h19HBcuOdJddP8pf\n" +
                "Ru1G/8svEP4TYZTZgAplbmRzdHJlYW0KZW5kb2JqCjEgMCBvYmo8PC9QYXJlbnQgOSAwIFIvQ29u\n" +
                "dGVudHMgOCAwIFIvVHlwZS9QYWdlL1Jlc291cmNlczw8L1hPYmplY3Q8PC9pbWczIDcgMCBSL2lt\n" +
                "ZzIgNSAwIFIvaW1nMSA0IDAgUi9pbWcwIDMgMCBSPj4vUHJvY1NldCBbL1BERiAvVGV4dCAvSW1h\n" +
                "Z2VCIC9JbWFnZUMgL0ltYWdlSV0vRm9udDw8L0YxIDIgMCBSL0YyIDYgMCBSPj4+Pi9NZWRpYUJv\n" +
                "eFswIDAgNTk1IDg0Ml0+PgplbmRvYmoKMTAgMCBvYmpbMSAwIFIvWFlaIDAgODU0IDBdCmVuZG9i\n" +
                "agoyIDAgb2JqPDwvQmFzZUZvbnQvSGVsdmV0aWNhL1R5cGUvRm9udC9FbmNvZGluZy9XaW5BbnNp\n" +
                "RW5jb2RpbmcvU3VidHlwZS9UeXBlMT4+CmVuZG9iago2IDAgb2JqPDwvQmFzZUZvbnQvSGVsdmV0\n" +
                "aWNhLUJvbGQvVHlwZS9Gb250L0VuY29kaW5nL1dpbkFuc2lFbmNvZGluZy9TdWJ0eXBlL1R5cGUx\n" +
                "Pj4KZW5kb2JqCjkgMCBvYmo8PC9UeXBlL1BhZ2VzL0NvdW50IDEvS2lkc1sxIDAgUl0+PgplbmRv\n" +
                "YmoKMTEgMCBvYmo8PC9OYW1lc1soSlJfUEFHRV9BTkNIT1JfMF8xKSAxMCAwIFJdPj4KZW5kb2Jq\n" +
                "CjEyIDAgb2JqPDwvRGVzdHMgMTEgMCBSPj4KZW5kb2JqCjEzIDAgb2JqPDwvTmFtZXMgMTIgMCBS\n" +
                "L1R5cGUvQ2F0YWxvZy9QYWdlcyA5IDAgUj4+CmVuZG9iagoxNCAwIG9iajw8L0NyZWF0b3IoSmFz\n" +
                "cGVyUmVwb3J0cyBcKGltcHJlc29VbmlmaWNhZG9OYWNpb25hbENhc3RlbGxhbm9cKSkvUHJvZHVj\n" +
                "ZXIoaVRleHQgMi4wLjggXChieSBsb3dhZ2llLmNvbVwpKS9Nb2REYXRlKEQ6MjAxNzEwMDMwOTAw\n" +
                "NTIrMDInMDAnKS9DcmVhdGlvbkRhdGUoRDoyMDE3MTAwMzA5MDA1MiswMicwMCcpPj4KZW5kb2Jq\n" +
                "CnhyZWYKMCAxNQowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwNDI2MTAgMDAwMDAgbiAKMDAwMDA0\n" +
                "Mjg2NiAwMDAwMCBuIAowMDAwMDAwMDE1IDAwMDAwIG4gCjAwMDAwMTEwNTkgMDAwMDAgbiAKMDAw\n" +
                "MDAyNDA5OSAwMDAwMCBuIAowMDAwMDQyOTUzIDAwMDAwIG4gCjAwMDAwMzM0MzIgMDAwMDAgbiAK\n" +
                "MDAwMDAzODEzMCAwMDAwMCBuIAowMDAwMDQzMDQ1IDAwMDAwIG4gCjAwMDAwNDI4MzEgMDAwMDAg\n" +
                "biAKMDAwMDA0MzA5NSAwMDAwMCBuIAowMDAwMDQzMTUwIDAwMDAwIG4gCjAwMDAwNDMxODMgMDAw\n" +
                "MDAgbiAKMDAwMDA0MzI0MSAwMDAwMCBuIAp0cmFpbGVyCjw8L1Jvb3QgMTMgMCBSL0lEIFs8Y2U1\n" +
                "MTA2NjZkNzEzNGNhYmNmMDAwNjQwMjAwODAwOTk+PDZiZTFlNWI5ZWM0YWUzMDUyOTliM2Y1NDc5\n" +
                "NTgxNWQ5Pl0vSW5mbyAxNCAwIFIvU2l6ZSAxNT4+CnN0YXJ0eHJlZgo0MzQzNQolJUVPRgo=\n";
        a = a.replaceAll("\n", "");
        String b = "JVBERi0xLjQKJeLjz9MKMyAwIG9iaiA8PC9UeXBlL1hPYmplY3QvQ29sb3JTcGFjZS9EZXZpY2VS R0IvU3VidHlwZS9JbWFnZS9CaXRzUGVyQ29tcG9uZW50IDgvV2lkdGggNjQwL0xlbmd0aCAxMDg4 OS9IZWlnaHQgNDgwL0ZpbHRlci9EQ1REZWNvZGU+PnN0cmVhbQr/2P/gABBKRklGAAEBAAABAAEA AP/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB AQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAeACgAMBIgACEQEDEQH/xAAfAAABBQEBAQEB AQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEH InEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFla Y2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbH yMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQID BAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJ IzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1 dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY 2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP7+KKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA KKKiLeWhklIGBz7DPAHP8u9AH5YeEP2l9V+M3/BV/wAa/AL4e6n4gf4c/sYfsqeIdP8A2ibZtY1R PDGp/Hv9qPxt8BPHnwM0w6A0Ai1fX/hp8H/hR8VZY/FYkkQR/FzxD4NtGjuvDfjNB+qlfjh/wR18 O2ni34K/G79uS+h1Vtf/AOCh37TXxi/aN0X/AISKxjsdX0X9nOPxprvgf9lXwkoEg/4kK/BbQdA+ JcDGJdt38Q/EZdHVlFfsfQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABX5 pf8ABWr4oeKfhV/wT6/aJ/4Vp9pX4u/GTw7o/wCzL8FLOyvk07Vh8Yf2q/GWhfAPwHrGiE5I1vwx 4i+J0XipdpLOdBk3YXNfpbX5CftvaTH8dv27v+CXv7NUcOnah4Y8EfE34vf8FCfijbSa0iajpGlf sqeDY/h38JGbRzv81p/2if2j/AHiG2Rs/aB8PfELqw/4RoKgB+kvwY+E/hT4GfCH4U/BPwPbvD4J +Dnw48CfDHwfa3JG+y8LfD/wlo/gjw/FkHrHoWjRq2CQXMmeSMerUUUAFFFFABRRRQAUUUUAFFFF ABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU UUUAFFFFABRRRQAUUUUAFFFFABRRRQAV+Mf7GHifTf2j/wDgpR/wU4/aYh/snW/CvwEvPg5/wTf+ FPiG12m7stT+BkfiL4wftKaNLuwv9tH4y/G3RvDU/IZovAPhsDJav0P/AGnfjhF+zp8Avi38abjR pPEmo/D7wTrOseHPCdnGyan478dFRovgD4a6O/mOyeIPih491bQfBnhFShWW/wDEEKN5ed1fBn/B Ef4R33wl/YWt4vFGqDxH8Q/Gv7SH7WHiX4leODt/4uZ4l8PftC/ED4TaF8S/m4/4rD4dfC/wR4iy OR5u4dKAP2AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo oooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACii igAooooAKKKKACiiigAooooAKKKKACiiigD8xP20/j7/AMFDPg14r8JaR+x5+wBp/wC1p4T8UaAf +El+J8P7TXwi+HGv/CfxRb6u8FtFqXwQ+LX/AAq6Dx/4evNDkiu4G8OfGbwrc3Mx8QWV0/gxbWDx P4l+NYP21v23NC2WP7Qfx9+GX7Kuoz5+2QfFf/giz+2S3hjROmFf9oj4f/8ABRz4t/sysUAHzv45 l3bsGFSN9f0C0UAfiN8O/ido3x6uCNC/4L3aBrmtRBIbzQv2VLT/AIJwaJa2OrTbSdF1XQPi38IP 2rvFWiTorLt0G78VN4ngkZzMZTDJEv1PD+x/8e72BTd/8FTv2+tTsrgP5kcPg7/gmtpi3tkFWZNm q+Hv+Cc/hzxDAsg/1b6DfRyLuAEzB2avrHx/8Cvgn8WoDD8W/g78L/ilERxbfEL4deD/ABoh9wni Lw/cqCOnIx9DXyxcf8Etf+CflvJJP4T/AGVPhj8I7yYLcXd3+z1aaz+zfqkrDjJ1n4Ca38L7ksMY y825sF3ByTQAq/8ABPTwZq43/Ef9pr9vv4izAYFx/wANu/Hv4LyLj7pI/Ze8T/s+wuAcZRonQ/dd SpILv+HbH7Ov/RRf2/8A/wAWx/8ABUn/AOjOqD/h3X4B0kBvAH7SH7fHw9YRERi3/bj/AGlfiTp1 sV5yNK+P3xG+LcS7sElVj27jn5VOBF/wyT+0voO1vCH/AAUv/a2aKDiDRfiR8N/2H/iF4eDJNHGN zaJ+yZ4N+IsilXMkqnxzhkRo7Y+HFwxALI/4Jn/sp3JVPEcP7SHxHUJj7F8W/wBuv9uL41aYCO50 r4uftIeL7dQB0VY8DpxSn/glR/wTQn3T61+wB+yJ42vtmRq3xI/Z/wDhp8SNXY5HyHXPiD4b8V+I ByMkGdlOBx0qBfhT/wAFLdBijPh39s/9lrxjbxqrGD4r/sOeNbnV79iAWjj134UftpfCjQdEjB3K zv4K8VSEBWUAFgF/tD/gqXoEbEeEf2B/iqWU8f8ACxv2kPgHLeGBcjbn4Z/tHDRm8SEYXP2hfBUn fxqegBZ/4dSf8EtP+kav7Av/AIhp+z9/87el/wCHTv8AwS0/6Rp/sCf+Ib/s4f8Azuapf8Lv/wCC hOhEL4q/YJ+HfiKOIt9qT4Jftn6R4zLEv839mSfGL4Ifs65PRLbzBF5oB84eGgSKef2wv2kbZTDf /wDBKr9t24vIVdjL4d+J/wDwTX1LSiETbnTJtf8A2+vCfiFwBnmTwrCxwFRXySoBZ/4dSf8ABLT/ AKRq/sC/+Iafs/f/ADt6P+HUn/BLT/pGr+wL/wCIafs/f/O3oP7Zf7QvJk/4JRft+eVxjd4//wCC XpA57j/h5EePegft8/2WP+Kx/Yu/b98FD/b/AGcJPiX/AOqC8T/F3/IoA+EfjN/wTZ/4Ju+N/wBr n4A/Arwr+wL+xH4b8O/Cnw14o/ax/aMvvDv7LHwI0KS98JpH4h+EfwN+FfjU6V8O8p4d+LHj3WvH nxIt4Z2+y+J0/Zj8RWgieCHypNH/AIJx/wDBMv8A4J1+Pf2Cv2QviH8Q/wDgn1+xZ4w8cfEz9nz4 XfFXxF4g8XfspfATxH4n1C/+KHhy38dk6rrOufDlppTFHrsduqnYYREsaKqhDL8sav8A8FP/ANnH wj8Hf+Cnv7VHjLUvjd4H8W/GOy+KXgv4HXnxD/Zf/at+H3hy78Efs6fB3XfhZ8KfB+iePPEnwbi+ GSp4l+N0Hxw8UGA+JlbwV4v+I1v4H8ZRw+NI2hb3f4Z/8F1v+CRfwD+D3wA+CHhv9rLwl8RfE/hn 4U/DP4d+EPCnw60vV9ROuf8ACD+C9A0BdLHjXxFb+FfhhoU6KiBf+Ew8ZeEEGWG0gYYA/Q3/AIdS f8EtP+kav7Av/iGn7P3/AM7eoX/4JY/8E/NLHmeB/wBlf4c/BacL811+zOuufsu6mW/vf2v+z3r3 wpnYnuDKDkn1JryPwH+2r+0l+1BJawfs0/Cj9mvwDoWqMjWXiv8AaA/as+G3xK8bCExLqv8AaOj/ AAV/Y/174weGPEsf9hu11iT9pPwbKpIkci3ic16x/wAMefGT4not5+1P+2T8aPHlncr5l58Lv2b4 v+GMfg4r+W5ZreX4e+IPFX7T7oWkkibQPFf7WvizwpKPmayIdVUA+afjJoHwW/Zh12D4b+E/+Cl/ 7fPwt8ea5Y/2jo/7Ofw28a6J+378fNasMyqNd0PwN+05+zr+3L+0y/h5ZY28zxFDcL4RiUfvJIvM iY/dP7HXiH47eI/2f/Bmq/tFaL4h0X4p3HiH4naNP/wlumeE9A8a+Ifh54Y+Kfjrw/8ABLxv450H wJM3hHw98Svil8FtH8B/Ejx94a8J2vhXw14X8Y+JfENpD4O8Ex2v/CIWnovwX/Z/+CP7Ofh7UvC/ wO+E3gH4U6Jqlz/bWs23grwzpXh4+INWKuw1jxdq0UR1/wAS+IQomVvEniWW6vJI2VHlZkVD7tQA UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABR RRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFF FABRRRQAUUUUAFFFFABRRRQAVwfizxb4W8DeFfEPjbxv4j0Xwf4Q8HaHqnibxT4r8T6xpmgeF/CH hzRNOn1zWte17WdX8rQNG0Dw3oqmfUPEEswtre2jmkvJ4khldu8ooA/BPW/+Dir/AIJxXOu6novw h8d2X7QFhoFzqOna34n8O/Hf9iP4B6RHq+k7w9vokP7cP7WH7JXiXx/BJFGzx+KvhZ4X8aeCWZCF 8XARANz5/wCC2HxB+IciH4D/ALA/x8v9AbaH8afEDwF+1V458NS71O/UNF8Q/wDBOb9kf/gon8Mf EkLMcARfFq2dgoJKnfn+gyigD+fY/tZf8FYfjNi2+Avwi0HT2uFbz7nxV+w9+0h8OPDfh/8A1kbn W/H37bv7Rf7Dni1lCXAQzeDP2R/i/IGSGQ+D2DXA8Lxn9m3/AILo/Ejyf+Fiftg/D74VwX+w3Oh/ DL4jeDfEXhuEsBlJo9F/4Jy/B74owExys7t4b/aKV08lrdHWeePxX4X/AKDaKAPyY+Dn7Fv7bXhn wnH4O+IP/BSf4h2+m3ut6hr17qPwc+G2k33ji7j1na7aJcfFD9sTxV+2zP8A8I+QHiWPwf4P+E4i l2N4Mg8HKDC3n37Yf7InwY+DH7NXxb+K2ueMv2tvj38ULbw0NF+G+ifFX9t39q7UfC3j347ePdZ0 XwJ8B/CEvwp8OfGbwr8ELceLvjL4w8EeEoz4c+FEMYl1zyhF5pEcn7UV+d3xuaT48fthfs+/s7ac EvPBn7PLad+2X8dPJkLqdVil8R+Af2VvAmtRgxop8VfEWP4ofHFGLPJDdfszeH4mXyvFChwD4B/a q/4Jd/sY/sv/APBJb9sDTPAP7OHwT1r4tfDr/gmv+0b4YtfjXqfws8G6p8Xdf8VeH/2avHaS+Lbj xvrVpc+KI/EHiTxCj+I5ZYb1SLh5UKj9yY/30uYNK8QaYba6h0/V9H1O2y9vPbLqGn3tg6ggMjq0 UgZWUjcGGSMLuAI4H42fD2H4t/Bn4tfC2cqtv8Tvhh48+Ht19oOAsPjXwjrfh9wx6DA1gh+wXIHS vG/2C/H118VP2IP2N/idfgNqPjz9l74EeLNYef5WTVtf+F/hzVtazyRuXWZHDscgY3DBxQBL48/Y U/Yh+KYn/wCFlfsb/srfEhriQC5/4Tn9n34Q+MTeNv8A7RUFvEHhe5D/AL4mRTJnLlZCNg3jzA/8 Et/2DrEL/wAIr+z1onww2YFs3wT8W/Ef4JNbEAcx/wDCn/FXg1UJJJO4FfT1r9B6KAPzzg/4J7eE /DKGb4OftQft5fBjW8Kg1u1/bC+Mf7QFlbLhiG0zwN+27rP7V3wuiPJ+VPBZQZPcmkk+G3/BSXwM iJ4P/ar/AGbfjFpFpiRrH48/sseLPDnjq9Rhgq/j/wCAvxw8JeEUCkjKxfs7hs4KMRur9DaKAPzx X4j/APBTDwe8lx4l/ZZ/ZT+Lui2YBvLr4Pftb/ETwV8Rr9drFk8O/C74ufs2x/DmSbG1o/8AhJ/2 jfC0bLvBkSRUR54P+CjfwS8JGGz/AGlvBfxs/Yz1FQ5urz9qL4cv4a+GFgBGJFNz+1D8Ptd+Kn7I aO4Kjyk/aFknUuu5AcgfoPVGa3guIZ4ZohPBcA/abe4wQVIGRtOQVwAcZI4GCMUAYfh7XtC8XaPY eIPC+uaT4j8Pa5ai70jX9D1bTdS0rXNNkAA1TSNY0R5YbmMltvnQStEXGEdeHrqq+C9d/wCCdv7O Mesaz4z+DuleMf2TfiBrt1/a+reNP2SfGOsfBFNd1cKx/t3x18L/AA6knwO+LuutgEzfFv4SeOSM ldpcDdF4P+Kfxd/Z/wDF2g/C39qrxLovxB8F+L9a0Xwd8KP2r7HRdJ8FDxP438Q6tNofh34YftDe CNBt4/DXgX4j+JZm0PQPBHxK8HvbfCX4weMZ38GQ+CPgx47ufAngTx6AffNFFFABRRRQAUUUUAFF FFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUU UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQ AUUUUAFFFFAHParq+l6Fpt9reu3lrpukabZ32q6vqd3c/ZLGx0/SFaWfUZWLFVjSJUkdtwGwA5Yq qt8P/wDBP+x1LxR8LfE/7UfimyuLbx3+2h421H9oye21W3jsNQ8O/CLXdJ0bw9+zb4IW3Ued4ffw r+zl4e+Fo8b+G3Z4x8aNY+KPjOMI/i6cyVP2+b648eeBfAX7H+h3E1vr/wC2b45HwY8S/YvMV9C/ Z10TTLnxv+1TrsriNE0U3PwV0TxB8MfCPiLYhs/iD8Vvhs5STz0kX77tLG3sbaGzsYoLSwtrZbW0 tbeAKtoqrjaoBICgY+XauSAWJJLEA0a/Or/gnFC3hr4D+I/gPqkI03xd+zH8dfjt8F9W8PXW2PU9 I8I6L8ZPHPiH4G6tMkis39h+MP2d/F3wo+JHhEPEFNl4iiVZC8TrH+itfnf8foV+Af7UfwG/aV0x Tp3gj4ya54W/ZB/aNCySR6U58ca3rEv7KnxJ1t1MTNrnhX44ayvwG8JR7mhuh+0863HmHwt4UiUA /RCiiigAooooAKKKKACvzx/4KmCOX/gn9+1BpWnbh468U/D1fBfwVZVL6gf2nfHes6N4H/ZXGk4d SuuH9ojXPhbHaudwjuWhZklUNG/0f8av2h/gx+zroeja58YPiFpfhBvFF1/Y3hHw+i6p4g8cfEPx RAkY/wCER+F/ww8Mw+J/iN8RfFU3njZ4Q+GPhPxV4rkjJlis5EhaQ+A+DvDPjb9qD4jfD743/GPw b4v+G/wq+FGsX/ij9n/4EeM9P0/TvGuu+OJ9Kl0C2/aC+OWhqZToOueF9D1jXY/g38LPNlHgmHxC 3j74jRL8av8AhD/BvwWAPv2iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigA ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACi iigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiuU8SeJtE8GeHfEHi/xRqNro/hrw tpGpeIvEeq3TBNP0bR9F0yfWNX1Z32ljFBAkk0hw74RnVWIYEA+HvgUp+MX7aP7T3xxuGnu/C/wA t/D/AOxt8HnZlbTRqg07QPi/+1X4t0F1VVEfi34ha/8ACv4WeMIsFl8V/sv7GbMbOf0Mr4I/4Jve G9b0L9jL4Ia34p0240Txl8Y7LxV+03470XUWVdR0Tx7+1d8R/En7S3jbQNbQIC+teF/EfxSuPDkj O24yW527o0wv3vQAVxPi7wd4U+IfhXxB4H8beHNI8U+DfFuj6hofiHw7r9mmp6Vrmk62rLrOk6rp Ei+XKksRKsH3lS+QY5Fy3bUUAfnF/wAJd8X/ANjoHTviRL43/aE/ZPhuWXSvjBpGm6r4z+O/7Pem rKYm0n43aHo0UviX4yfDTw5FDLEfj54Yhuvi/wCF4Y7e4+Nvg3xgkXjD44SfcHg7xl4R+IfhPQPG /gDxb4e8b+B/Fej2GseFfF/hPVdM8R+Gdf0nWQo0fVtB1zQ530TXNEkR2a3lgklR1VZFkYOm7va+ CvFP7Dfge68Ua742+CHxd+P37I/ibxdqx8QeLD+zt4r8H6f4G17xdq83n6z421j4HfGD4c/Fv4Cv 8QfE8gifxf4wX4Tx+L/GLCF/Gd5dFllUA+9aK+A/+GOf2iP+krn7fX/huv8Agl1/9LbrNb9guXxi Vi/aA/bB/bb/AGktHtUL2HhfxV8WfBfwA8MiZmJca7ov7D/wv/ZJXx/azBAJPDnxUbxp4VKsCLPI 3KAe9fGL9qb9nr9n5NNtvi/8YPCPhTX9UXZ4X8Amd/Efxa8e6nEI1GkfDD4UeHB4j+J3xH8QuA2z wx4K8JeKvFMok3JblF3H5/Hi/wDbS/aSVl+H3hKT9iX4SXDmIfEz4qaTofjb9qrxZpLEBrzwF8FJ k8R/Cz4MF7ZhP4U8VfHuT4veLlk82Dxz+yx4VuUUy/QPwZ/ZZ/Zq/Z4e/b4Dfs//AAd+El3flf7Y 1v4f/Dnwj4W8Q+IMIzH+3tc0e0i8Q+IdWUHc1z4hmuZGGAThcV9J0AfKXwT/AGTPg58Edb1Pxzou la74z+L/AIos0s/F37QHxV1/UviP8cPFlmTk6Xq/j7xIJvEWheFjKY7m3+FnhJfCfwf8Fzbj4F8D +EkJtl+raKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKA CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK KKKACiiigAooooAKKKKACiiigAooooAKKKKACvzs/wCCh88ni34R+Av2W7edReftufHjwR+y7quS DDd/CvVdG8a/F79pHRpjIqqj+KP2Vvgp8cPDNoybQPFOswMCu0iv0Try7xZ8LvBfjrxB8KfFfinR X1XX/gr471H4l/DO7a/1S1fwz401f4YePvhHqetIsMsUOqTTfDv4pePPDqjXoJYNmv3B2RztDJCA elQxQwRiGKIQww8Io4UAAkkck457nmp6KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooo oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiig AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAC iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKK KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooo oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiig AooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAC iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKK KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooo oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//2QplbmRzdHJlYW0KZW5kb2Jq CjQgMCBvYmogPDwvVHlwZS9YT2JqZWN0L0NvbG9yU3BhY2VbL0luZGV4ZWQvRGV2aWNlUkdCIDI1 NSgAAACAAAAAgACAgAAAAICAAIAAgICAgIDAwMD/AAAA/wD//wAAAP//AP8A//////8AAAAAAAAA AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAADMAAGYAAJkAAMwAAP8AMwAAMzMAM2YAM5kAM8wAM/8AZgAAZjMAZmYAZpkA ZswAZv8AmQAAmTMAmWYAmZkAmcwAmf8AzAAAzDMAzGYAzJkAzMwAzP8A/wAA/zMA/2YA/5kA/8wA //8zAAAzADMzAGYzAJkzAMwzAP8zMwAzMzMzM2YzM5kzM8wzM/8zZgAzZjMzZmYzZpkzZswzZv8z mQAzmTMzmWYzmZkzmcwzmf8zzAAzzDMzzGYzzJkzzMwzzP8z/wAz/zMz/2Yz/5kz/8wz//9mAABm ADNmAGZmAJlmAMxmAP9mMwBmMzNmM2ZmM5lmM8xmM/9mZgBmZjNmZmZmZplmZsxmZv9mmQBmmTNm mWZmmZlmmcxmmf9mzABmzDNmzGZmzJlmzMxmzP9m/wBm/zNm/2Zm/5lm/8xm//+ZAACZADOZAGaZ AJmZAMyZAP+ZMwCZMzOZM2aZM5mZM8yZM/+ZZgCZZjOZZmaZZpmZZsyZZv+ZmQCZmTOZmWaZmZmZ mcyZmf+ZzACZzDOZzGaZzJmZzMyZzP+Z/wCZ/zOZ/2aZ/5mZ/8yZ///MAADMADPMAGbMAJnMAMzM AP/MMwDMMzPMM2bMM5nMM8zMM//MZgDMZjPMZmbMZpnMZszMZv/MmQDMmTPMmWbMmZnMmczMmf/M zADMzDPMzGbMzJnMzMzMzP/M/wDM/zPM/2bM/5nM/8zM////AAD/ADP/AGb/AJn/AMz/AP//MwD/ MzP/M2b/M5n/M8z/M///ZgD/ZjP/Zmb/Zpn/Zsz/Zv//mQD/mTP/mWb/mZn/mcz/mf//zAD/zDP/ zGb/zJn/zMz/zP///wD//zP//2b//5n//8z///8pXS9TdWJ0eXBlL0ltYWdlL0JpdHNQZXJDb21w b25lbnQgOC9XaWR0aCA4MjQvTGVuZ3RoIDEyMDg1L0hlaWdodCAyNjcvRmlsdGVyL0ZsYXRlRGVj b2RlL01hc2sgWzE2IDE2IF0+PnN0cmVhbQp4nO1di5XjKgxtJ/3QD/3QD/34jTEfCa742GRfJqO7 ZzYzjpEBI4Q+iOPYDW+s9dupKhTfDfd6GRd/V/5RKObgX6+X8otCMQN//Xjn7A/fWO+UdRSKWTjz wzXm/Gf/76ooFL8F/ke5iXCq3SgUkziNAkHivFTeKBQTCNLF20u/cacpWuWNQjEL/yNt/u86KBS/ CaeEOVUcXaQpFEuwQb9RxlEoJnFKG3sZBoLEUf1GoZiCz/4bZRqFYhLeXdJGQ20UigUU/40b36xQ /HVc4iXEC1yMY73qNwrFHJyJCzXVbxSKApkdrngBE/Ub234XPpWdFIoKPhrUXsZ4ZRCFIiDusen+ fXh7biLw9Hsf5Uz5JPcrFIoTuhhTKBoEieGdM9ZYG7d1hmvOnles9d6fH2d6jvStD9dMvqaMpfhz +GEQk7enGeMuRqLXkg8n6TneGfKtVd1H8VeQh7p3hQUyIzTXCAs5ylLxqmt0I4XimxHU/ihNCnO8 wDX6r/7eqHtH8UcQRrp7bUJc3v3PbVIo3g5fwmggJ1SfI2gAm+K7kf0t26TNxTjqx1H8BXhBsqDP Wqep9Z/zD+UXxR+Af22GGgcUXw+ft0ADCfMCf/e+i3+b/7tRCsV74VtTmgGs0GecpoRV3Ubx3fAm M4MJZmTvy26bdO3ILs4zKuD0hha9xpxM4tK1yEjKNoqvBrOlpVCZ6lqdH/q4IgUSm1xbcnj0gBqj Fd8NUxZf+ViozAHmZa9rlizBTrgipbJwoUE5quEovhYpC2ca60GUcI0nXiOX4oZPwiQhKg3TUii+ EyXBU9kCncY/yV7T8A3VgfKijFjmdKGm+GaUBVhyV/qDiJIj7cEpmkuSIvyuazO1LtQUfwLEmpYP hPJYkiCpFLN4enot8o0u0hRfiJgXgEiIrJGQazlvAFunRfZikqrRcLxuq1Z8LXzZwZmvZZWfXLOt FnQt1AxXZkymp0yj+FoQeWPyvhkiW8ACLPBIuNfya+F6UXr0XDbF94LwTb7mwDWg3yTrGcsZXTw/ alBTfC+YvEn+y5pHkP+mljdRWqm8UfwFZP3mVc7uLC7NvrwxlG8i+LJPofhOVPa0cInIoMwQSL8p Ts6cAIrb0xSKL0VcbBm2AAOCpF2neeCrQdcUii9Csp6RWEzfXjMpHzTgmzpe4JRU7TWF4htBYtFA JEC+lvWbfKK0BVJJ49MUfwQ0rjmHbBYZlDQcVzOJrYUSv/Yq+aUVii+ESRFqRLqYsjU6MpOtJBBl txT8We4pLKhQfB0u5cU5e/6z5/9Rw/Hp73SNXImFbP7nYilf6FibrW4KxZ+GMoFCQXAd1+HJ3578 z++sM3F68lP/plB8N86Dbyw/vcY7eMUx9joPlHLMcBYOkVJTmuLrkY+8zScJnP+iZcwWeRINATbl ICjxaeXMz2R1Mxqbpvh6+LxnpvhhkjUtbyRwKbmgrbaAGmI7c+mKBgsovh4lZBMkdIpcQmKfEyuR kJp0U4kHVTO04stRdnyWHE/lSgyXoanXI98QH08plneKqsBRfDVYvk7bxKxFBiBZb2IeAspJMZ8N vUlPl1Z8Nxzx8kfpQmIBXvmmLIOuVVk5MyfnsyFnS2t4muLLweRNywCtvGkCbZL9jOaHVlGj+G7Q 2P+TJTzjkkq/MZkliBEgsRstpnyj+Gr4oxEuVAS5mksySxAuSfY0ym7+0Pxpim+Gz5axHA0Q/TeG +W8iRzhS7CqXt6i5mrcUii/FGS5jsnaT46HjlaLeW0PZJuTmjI5Pm6MDQrGTkTS3gOIP4DzyyRhH B/sPUzRXjOGxZz8MZ17VlR9K1irXKL4ZJO45+1uy38XTiOh0D4979lW5g1FSKL4auwa5MoviD6FI Hfo32k3TWsjUaqb4w8hD35ct0/G38nvYEs2+t/mqTRuoFYo/Ab6DkwYPrMIBSaVQ/AWQ0z6TK4Z8 1t/x75VTFH8VFnDGvLxRKP4mnqzTVN4o/h526DcKxd8E5htTfeLvVd4o/ipU3igU61D9RqFYgeo3 CsVduJcxpv9D/9FreoC04uswu4byDC78m8aN+ujaTqFQKH47aAyzb+Ka+5+pXHutfLaR0bgW9fN9 dU2h+DjQWOZ0oJPwE2ObMZ2waAuRz2HhNl8B9PwSXb2nkQrFXnjJUSmiPgU65BFwltMxBgx5tDdn UAHN3qH4PISEtauM0+RD9xbSgKwD6tCNE9Wj2hUfiSfyJugirsd4PG0HPJtN5Y3iN8KQ3M9psL56 e2mYvLH1fQ2d0R5PzymUv3jeNYXik3BP3kTb11ToQJI5Av/4fmG1qSk+EqaWFvVnzVhZ3hDNxICf UrJ7mqfHz4q/qbxRfCT6ajkUAVc5b+clVczXCX06fXmjmTwUnwnb6iR9XPLG5/M9eblW4oT75PHv mT5TSzg9zFDxmVgObz7PEQg51Jc0I9Ew1n++8o3iM0HGren840P5SrPeyih6by2NBCXHVpKJP03j BRSfCXrmM4lvrn8v2ozBdrQzoXosYbEj1NX6TfibnkKdntucVa1QfBiKftG1XfksQSw7WS17aSqX JjUbJCkEmSBLLrI5x2RNR80Cis8EsWf1fCVULDV5Bx0q55v7LsapGKzcFL/w4JpC8WEgC6XeIC3s ZSqTQMc74wzTW5D48EUfyl8SHn3UNIXifSAKxiHvrpTMXsbVZxWw/M9VsfaUNeI7zWXpNRU4is9E Gdo9oy+RN/H/gV8mFLr8PKRkI0AseLrN96sZWvGpICIh/I05AcubHtuQA3RpCX6HB98Q9Ub5RvGp IJKkY/X1lZcmSI+RlTgmjmIlOacRdizqjZoFFJ8OZr1y2KIWdnS2wiawmVyi6DksBM7S6GgaGppz Cjh6TaH4UBQPSmdZVPw3WXLM+yQdLct4oXhTiXqTL+oyTfGRqDz2nfm9lTf1oD5Pjr4Sc/BzQE9Q iUNNA47JulhmU7RALxOPQvEYU/pEE+7fsI0zJkceNCE1NkmpVzqskF5mLJsk25JEUyj+OSgzzPpv jGdaTL1tNLAOPcfT8KJHbU0z+V4im26KiJKDjV7r7jpVKJZR/CWyf57KG1Of1BmXemwPjrGX2SCX L99mMUKkUKFXot5UvVF8MmZm+ErexCHdGJqZzHHkHhpDbdpyUAbJ0q8PrzqN4h/AF2khTvGeW9Po gHQopw1hnEiA3BWvE35F1jT13ig+FkEXaOb99h4ubyzJFdBLUEDZkFC4fDh06Zfj1uq63OEdn217 ITmvNSFF75V8V3lRsQ0TCzWm31SmZCm3gOEeoeInMumpKfLAkufk0vfUm8Aw1tBdo5niubXuFlGF AoBsRJN0CiZvqIemn2XAuGJVIyTOhRrVitKC7o41jdrNXDd76FUjK0RFKBSLmHB9MnlD7qGWNDRk LWOxnFEtPDOXMvSeRGlV3HiHt2czpnkxe55C8QD+4KIA6QFU3phcrnXrNMzj8r3Uh8Pztrn8PKoE 5XK4zuxnJZsbyTiqUDzCK++oEQQOlTdkvjZUpzGn8p2P9IgsRHQhGrDpiWywgN5STKezmV0l9qF6 l/qFFHvgoUmYgrpfyojmkc6JnxyTQmUXKd1YQ8e3Q/SmM3W6GxngFIotIEo6HrDEf1NpI/E6ddaQ wz0MpVdI1IxF6LXkOoh5cxYWaSpvFA9BvOrULAbznLnq+3CdLd5YPJp7IXrY1eNz7BiLKZjRQdxy dutEW6HYAaJ7wGFF9t+QZVXJ6VyLB1eyb5bpne38TJ9UWyrX545q4/RqHUf6VHmj2AUqcCywqKEN zb04A/5d/BadPWCL1CPCo2MVSHW7J2sC8ce9pVBE2A4THCwLQfm2XGun8DCqK8tY5SQNf0H/zlgk 1IZnwyXOD8K51AHWcImj8kaxB57rKrbnvyl5zpAMSvRQug9PdvrUDOK59uPrGlT+mp4NLYTT8P03 /LQe9d8o9sHl3JoGhLgUNrDNNbxvB+UBbexeZOonUdkj7aY66Y3a0+QQNJ9P+lF5o9iDKiqa7OZM QLlnacbCdrCWsV324TR8Q6RCuyMU11Pc73MWbGyBtGxiN9VvFBvhyAzeTMkeWKMK3wD54MG3LLiG 7/JkjtWuQPAkOw5noO45oldRo/JGsRN1DgBXfUflTZrTUW7n65N/a4+0J6YSFC7d6wfSrtSjoUGq THUgVib/XKVVv1HshCPaAlU8wn95di/+/35maZuDPDM/WS4hHLs7SRCkXZW6ILa5YuMmG2lU3ig2 g8Uo89m76BRIv6F6SkRzrk5zSpslJ0xbfr1GjihofTaG0hrjtBuqfqPYCjaZV3bgMlCLOwZnErhQ G6mDm5L6WKjdi2YD7bk8220LOFpBKn89S+WNYic8z6t5jbL0HUiXUccERJmAcha0yyub8kEf1bft vhtQB849/mjlndjGw6p+o9iMFNwc/kPfvISw/1qVJ74dW/Z2ChLCkHPZ5EVUE1jTxL/Nwet+T8U+ XNYqJhLot8wdA7z2XDqxcLcmJKZapDF+EAWBlKWtvy8UtTPJRYViF1y2ghls8ALZAF71PtHANnSH ZT3W2YxPc0fLmoorFjcmb1aFh7KLYjM88akniePT9eZUGu6jeVFvjOzQJzIlxUi7wgym1ZMSmuSG sYLuIFY5sV31pzKPYjO8IRKHzOY+ayAshJnqLSE0zPs8xOufSJNrMJFtTGIbDMaJuXbzm6kVijeD HfJkyqEbVAolAMPwaMuySfkGfHoa+Y6lYk/POETd5p+wTXuyQVu/aVve0Uq/uVLj+/sxfYhOG3H+ TqzQv3PvrvojelO0mb5RJA7MsRalU5FFkFVoHIJjleBnf0qWscbdOZBOit+P0TTw3mcvL+WbqOV8 imeVbvNgkmAyMUYdQ1Zlnfac2wd+G3bewU6wZ58LzyvXdPz385f3zX0zNCM9WE76u+6P3nCSfiQ6 yzMqeF5dNp/S2tx3z4p5WXlj8uL873oPnO5Yz5Xqn2senpKeg3OKd+kTLw6RAmWupxqK5fmh4QBP P/W6ytHvcV6DI0qbWpr9E2njva1zTV81teW1rZJ0Hs1lcbAFnGwa4Vee4s7C6JGZtiu0hXvvwge6 tsDRof2I8vUG2vH1o93uecKFkPevfY5dSSleTfCRcchCrezg7J9HwMnUJmNeMvFBzd3Eacpuf6/3 8odlRmlzzzSLy2TPWefcxB1ntHhegolopwcTd6+OLYbXkpnTNpn2q6Yd2X9tlm6fGgZcPbVsesbE OyAbFe+0Idt0u4PYOKR3IzjD9rhcckLKscb0IfHZbeYOUq7y//C6vJC0eWuQ2fnCuu3JfWMWWWeQ hl58zoQFpE56MvekeBbrPbCB3dFuzQ25ELa2/4M2HClVRfd9d59A1qSVncyExQW5wPZVTjQwc2z+ qXX97P2pIGQSuHShGwpcrwOPa5W7kmsaHQLce8QC5arBTpy1ka640gKqV87308TZD7z+0+cPhX1S ixOMhcvfiWeNfI35CXP1L7N8yRpN9ueQ1OpHYyaua4L2YRaJNogTAB3YjWJ7CuJgrWdSHqmwJg0i mLyRZuo6li8+bSBhXZfOi9Gqnrs+X+fJxQjPaes/sR83Ee8Ym5D72yxRZ2BR+q2vkb3zqV6Cu2Uo EdjStplpdV7pLfV6UjyTRqC7VaktuJ2TbXIN7+/Lm1dnKRtwO53cien9S1crFqUBeQ62gdK/H+TF m2T/Mh4XH2VnlAOq48QmW8KJFQmfjiEga38rHdHU7sWRGggWH6f8e4tNoDQh12wkEciVOX0Ltgfq b3C27Y0LNAakGbvFgsisBvasVIjfDAY3XySPJGj9+5oj3GW9htOVNbbxS251nNNyQSnR9falsRQb ezJCspzRsv4gShspTuA45lbKswir0N7k09qj2ltcTRE85bYSkp4ha3R3RUBEZ8hRX4yb1Qc6jaCP 4k/tvINsKTQ9G1vW1YaSH+0dfgUHg3NOsuO5qVF3RTWXeIAXyQd93xBcuDx9OqmZ0Pr0Hs9Ne65B kgMmTwHBA2JtU//cjnG9YJZRPiw6uqLp5F4AfZXeXaYM/CCl/lNzdRMniHQZpPOwMpJ2Cnjy6uxz 5VKOTk52PEh7VuS0MShcEp5+o6otc1q1H1lNadDn0XqkD/CJtgN0+E+Yfea4fgWifmZbA6qX51w7 svOjOS657/I/tl7kPd7RcZDmZFJ0Q5T0hxNJv2CWVt5uMXHdxQzXXB1dq13PCz61XNxghd6BLJvm 5vPmTTCzQlxF1auDGZtduIErgPXqb13itEu01q9D7sZLI7vV+Hyi0bfig+S5y9drfDPXI6g5ogZY 9VRqvVCjSZ0rnE0EZ+rpLKnNk9DQPq4JAK4Y0IPyIOWSqvMOAB/HdoxHh+P5xEVhyzIwjbSbpLMM JI6hp0WnctLfYbZDK0qwHk3Pl2xpx9GfGVdw1gvPwckvg2OthDxuVbR38zRQyIC29yyUWNpCS53j 9U8Qxdkh+ND6pZzz7TtPtYITALCw4ubyeMZ2FdNIhAtW1gKj76ma9qw/jrYNvmr1UjxkJ/5sbW8/ 0iC687PFz91tS2MZD0vXjx/j6/qF9vXLAfkpFQCyOZQWuhxkMRXrcid0ydb0L7/euJ/Ke0/lQBFL zS75/olX7bP0pFJ0IHFi+3PJ3nOKZFqMUOkbUIyvZwGhpnBlL77YQxR1G40C5zQpyA3RwscbherX 6140PYoLLyFuQegARFqUzGv7mUQ7YPDSj1Evahtpg9/BnIaPe6lbtlpwwl1fBbnyqwPP21c959Ne mFlN4hHQb5wUYLNY+1HVkHVpljdhHXuzF1pMdfS7ee0A2lB6qiN+n0IB3M7XdGwe0V0MFbC+VKZ5 C/MxDA5pwePZayiVE6KkXRt4YeYfiBy6OmzWiFjS5I5BlpKLnPA0sDa+g1ge1i3FvvXLttkYyouQ SsuDG6jVUh/A6QZrTnIL8EvhulN+l/Dm2bi8cEeV+YjqjDiGwkzGm/lDcCaTPWL8/vp56CwNRt+n vkH0BrUj0UgIwoLL8xBzZt8fSWE0xF4dX88t8HN0Jla7HL7IYvYj1hC94bn6kXpi8tDw2AG0qsES 68tRRCQ/rHYPt5Pysi4BdHDzksbJVZN834RPxk7e1zzpGGk5MajGZw9EiB2QF3d8Bw96orBK26rd YLcqjdzu9UiUE1Bvk8quDm5B+Zq8szuT4g5GNy6bEKqaHVxDotpEtohVz1hhm2D1QlU88Dus7u1G OUfrW6j2zZHn23hb7vMo26ZMsy6vLWlDlU+w+GwypsUhj2SonVt8EFr1ytzInYw0xf4QgVIX8g2Q HmIzRLMLmJagb+tGQhR/RdjzZJZU1yASY1mHhRoSHivMBD13NkXwIy60mOsqD6JgwdspOgJ6rmQU kE7GWYegmxCP/8CUmX/gfAwLSz6WzlNgPyQfEYVkTsP6o5e8Y40fA7bOcp9WH/lNX8xBtBvJTlcy sI50nNxCSKduP9BIh6uLNMU8yinu6HxZOpTb59Ed9O+pfRJs9V3o7/TdWEJ32t+PCTXtFlbD3jSz 4mAeazgt6ngNXE13oDlRSyL5rEn7WtJ05djggaaZ3339/DBAbtCGNl9IiPl5Jvdx2XUjdFW9tT1+ AGbK4i+FKm3kmyV/UhfQHgQpQTN0tz8g7XY8QPvYYExAIxmptiht7u7nD9O2pZJqsnUz6NWUtsne eZiXNsSsVJCdaI4a/pK+n8+vKZiuN/puBAPyA1pckuCoM6AhDuQNXCbNtWe4PxSA9wB8+v25y1V1 4uQXfWc1oLrW3FW16M3JXdJDk0fmnsxh+9h760nRd7OhlcS607RidZ99oojnOtKehI5QkIymsCPB jaAKXQuQYLF09P1gDXBsbcTPC9OGy7RFy8TN7DdzUr/Jm7FS/+cozsx6/Yt1G9OPa61hgR6w1Mph /V/Ax4zU7cn64vE1c99AKDRjS9Bv0BgczDKuoVuXqeXoute8fiRjZXvR5PTvv+IpyVzF9//Lk/eS dU2KA0DVt0tefikIu+/ZXWnCpA12jhhe8oAlAPax9IlP9sNaAE8AfH2kDPSsmHvSwB9cSolR6A9W FKJJrXPPLN9sdLUfYWP0OC3cjdxZ0mbibbW3YJH2hPqkHnBjcPeWgOy+9q6RgIfMTvnm9Wqk8k63 M1qpPEqUD9pTd1Xd5n9z8h7350R7uLyz74wh8PVMcxxj7hV40cxY9GeAI2hnalYjSVFppc6BomwG vgNRkvHZGwfw9PUbWZGWd165YzUPM+2l8ulFw8wt3eYQ/XGsf9vY9/7O4Xkf1U34K/G4rdNy3IND KTDueJGl2iJ/07OEbHMLNXpqdnzu6LH8mBO5DMgCMw7zbTmDzPfgcMmN0zNzAZs99C2jFcEGYW1N 27eCuYHtz37jMk20Ej00SE7YdLGPZaTeTEa/ofv6pD1c1pTdqqNV3BPItrT47R2K3diDiOaOrzoh GXTpY0sOwUZnW8GU92Cdb+aMRPi+wfiDI9cL35oZiiuwjeR9Tn/s62pewGfwDdJl1iEmi8nPeFZH HJf1TG+S19alzvDJo1QYU7FvOLJ7YMEUBErsh257ngOLhidj5ywH3yz5vu0ly8r/bqC4641rUXLm Tvl8lM/+xMioewJZkEZ8U/WBNHzBfSPNqetzamXyzdi9qYc/iA1sqDZ6Me0t6On+FvSsac+x1XVT MFhbi1EQN3wsiWopCrWR/iZ03M155M4FrdwG7K1HEccBOOKO9FOnxb8ektNzz2uDu4g2qE5jnbSR dOGO/kCByz9U5IbFAfRDnj5cXc/X1lXaxZb76beOWm6F9c33/zRgQMRTe7dsFXk92iBNvNTCXvab hEd0s2Yj+VgGNi+YgxPpLVhz6vnNZEkm7IO1LY27gPkAHtL3mC7LQoqeK+eC+GUAvvyNsWn5dBuq 3+wIF51Y/gF36+DJVVYHI2pEyxaHZr8KjQ1DmTN3RgrA/Eg7vAygTXSdgoJ5v0XDEROEbmlfb3g/ egCO4OZONzDF9wY3nvdhFMCS5iTqWmkI4U660SnC80Xt5ilwJgTiykUPtnX0xe8EWnfvi1x9i+8m EMbWbXbPso8FZbOBnLa6r6dxwTKJAvOR7ffdcMmwgz6evMr32FJ7b7Pip0FSb+zxTCpcM4o8up++ N4ky0a2mB3ec/TraTYVOeimk3wD7myHazVt9N8JuJbNDy+hNXuf/vUR+v1vayKeGbZkUegrpQ4x2 HUM26C9OgD40l6tzHF3R7m8iXlr0BrbutF33/04CRpoUjhAP7frlSo6YZeW16cyb8eC+DVhvsAts /n0t5JJeNUN3c0nPuHDv4iSDyL9ReyUvobNh7Jdzznu9N1B32qTwwnr3g6P6gxssl8SckYuakwf6 RR5dOPHTQk8M8Eb6PBNn2xNCcvjws3Am+CdCyizQ3acyCykZ9POYtxl5I/tYKJ30CU+f9ML9guYk aE9w22mOTJvO2HYPwtEDb6RdVir9w1XN8LS8Dwa2UW5aX4/H9n2MNycs2byYsTZJB+mlLmpOeZth +SSTrQG+lY0hNu+U+TXfNFHWL9A2ev/vlTnSEnQD37wnViDSHvPkdO60ICmwa1t4NjanSbTRerGQ liOVEa2ZaxziARAbMNJv8LkKU5386Xh01ukAMzFkdzFOgA7W9eJwqdjmkjdyF6wE8NSny5mqHjM7 8J7gDZsGM4bO5/5C7eqN+XN3PgLRyv7GaAF5aD/VnQRfC8+hJvhYmE6Tfhccnrie/X09nv+PtHJb TnbBu0CpH6quK/17oh/Fof2muLcX9d/gFUfb1zN5/j4M+ISxLaspGAGMDq64RRzWm86jNj2xAOVB O2JG1HJfkArDU3Kq50vnEVXaSy1thICKfYMHngD9zp28rOekUy6q+piJU0s/C+IpOxto40PDNhA+ IcyjZNaaiE675kTJXS8HnKEn13RPoN6tliWTa9l+EE8HWL3plVgANirR+gxOcWLFJiXoR0ASpDt6 9o1Oz5lX1hqp0OLTpRmRSYW+gRR0GkrdkXLdUUlmatViIswO1SAeFDbuTWCt25dVZiJgdGAZoJbG 3yNzxFPWBrnAxnQFr8UuP7V0Xo8n634UNJPOp7v+SRlQbVqhCxYsmJaNUw4JIkENSv6aixaej61z Z1ovH87Tu34uRnEh8VehPPSFjaeXJ8ArirE8lXAvX/j/ARyxumeHDJ9Nrs9tr2y4bq+FQvj+XEmH fHMWJG+M0mFcxSaO7Tr3zlx07XX+HY0QSL+3lqOuR/0V6Jgrux2q7dgu1tI3G71DNd+EOoHz3epx 8Kr+Lp+/ReS80X3ztl0fJ4QTl8v30FY8kWB7YrEAc6dNeCos8B7Np/yG6LK4GPu2LYgHG0wry99a EzdEk/wLdDZJP8Vb1ZtxeBpomeMWPv77NVdO5TxF0WkoQwClbxxs+jO+GYrGd6R8JNSRvbQ1viSJ M2ci+B1azvvkzSh26SFEvvHyDa7rvw4J6Y9jtMKW9vV0KBOPjSd0pv0bcpU7te3Q757oPAvRh+bL 82Ob1yaHvBv4k+WOZF/fwPSpG1CnbqPOn0Goo3NsPN1ZyT7NpKS5SLev2uAcAdd3WNLgWs4j6Euj qtanNl3/v03HlKjXXiypPRd+Q9Ca1JqnfCNY6va9shF1NBceUnvN4f38DIf39QgsYLunCSxZm1oM pfdbdcxZ6kLURKdZny1rTkgi9Hm9oaXu370yFJ0mlVtkZ2FfDxwag7jyR+rNhB8G+LA26pgrRoel ozY/lWvK2vM98ka25DzrkVJvmXq8R4pOE6RC8agMVW1pX4/AkrULlcXF3V+mBdo93Uamvyfvt2TT RDuHQjDTQlufZxJ9N6QJ7/mcBHN2/bupTjqfxtZ+lfyipgF8KYEZWl3RjLIpgOhAZPEThty41rDY fh2T1lOm7gxqG2r7528ueJd+s7pnrZ6RRxhVWsydJliwFvZXQs1JlLDdYfpQ3gyNnt2g8cdY1WD9 GdQ02bIPj1MTTYSP+3Ycr/yYOpz1E5CPJXyBW7yg4aDcaeIz+955PK57zMS+u8k3b7XNjBIJm+LL 6fl0PphnAqR6PztJWrTtC4H814/30/vNO2aBqKeI70IwYWUv9zDyWDjZU4yZ60wWj81pg/pOxI/d xsSO26ZEaPOUzNmXX+E9eJu8mbbtF/h5cYQynY02Sad536T7adkFDUfe14NlbGcKlqLT6pm4jnOL v0+5b1qT2nRLB7gbt+uB7tn0wK5KvglvixfobQUEuGz8s7PMeJO0lDsNRzNfLZ6b3+STPUXrpEj4 ndFpIv1th4bCOh2DCIbw48aC9pOFzdHLcvsOwj3DqZuZQNPtQ99QJ3cajtmaH04gjnpgVxY7U9C1 XjEa+oUjoYe92ae/ayp/Fn84Mkt/uEVNmvHu7b8hOkJ3fwy9P+o24f7RnJ++lazQhR6MTjt6loFZ LzV64bmcMBiQD0e83VhnnfPu2nNz/XNg742cpipjGPx6E6GPF9SbYh0r0RO+zzkfcb6UDOlsz8fC HPqH+yv9+d0XQ9+QF3wsF0B7X4OMAoQ06C76JVq7S+0a7l8Bj782sf2w0USKO3TmzrYRadq6r8ky 35yNVGjNvo3/C+JC08vr1ClMLhHiLBTvnnylwllrhGY/d5okcKYe3s8BJcS/wdOzBX1oqhPm/BuY /h7VYT7vm4xOpPSW8xLeCMkV/XR9uRJmk7tv8qHjlTXYDUrkDZJG0y+9n3W641adbci7TyLYRH/L zp4m308htaeW74LosX4L3+C4o2LRH71SL9PmrwzleibPvqPhdHQSR/Q0SeBAij15M4w9m+irO/RH iG0dyPxpWt385B8M2fH5DPNTKdkTM8es9amSwP8Czqep1J+70dqIMQZZQsWWwXM39558A9q5h/62 NKOpw+p6bqnl+yAa1B7y++zOD+YEm3ulEydGiYM7SoX7Gs4wd9q8D+eOfrP2Vt6n38jntk56wcrP jq0d/x7C/DiR1WVAd64z0ggmEcsj5EFvRMrIv8NuwNEMM0MKmQX84IaXJEXer9+8i75HGXbuURYm sR21fCPEHe7PItTG8QLIhj/Wb6QoMJt3znj8cFNmuM4pc2PGFaLTSA2Ficixuy5I8mDc83Pv5n3n D+Ez4vKZPsPyB3lfN0KyPgBwjT3aNzJGts13OiNbU9LPlLyxbbxWnXW6Htxt/JnP5dl9Yw0HRAtU PWXb+LKX0Dbkv9lpSQL9sIc+yt5wf8SYOiZvKV7wf8LeiIGMjrzJFqHmnpmen0lEPj6hULIjjjgX Hp3G75AkOOjPt8aPHe/K1inI/PuuC0jt0+UNjPV61A0XWSh8meWplXQzj+Q+CQM7eeJkT0GvG3Iu KFO/YSzBEWkcrTHRCZMQQy+f0jVAot6XEPCsk+e1fCfE9TiL97oDKco8rYFRIE732NqrtnCnZy1M wC1NilnJ7kX1pPb54sHQBFIMRksX3+ma++6itwnqCf1dKSVjHaD589PlDdpPckXhbiGbaXOPfDE/ 0+dOdH3zykwrbkY+lgC4OBhqOEBCtCWwAxy0rvVE0ejqDYB+qsf0G7/TQ30Y0Pv0eOijY1F7UnVk cTHFH393lYSlY30y2oxQuKnhSPt6OGmhQyfr8MmG6MsKOtW4aXqwmh8vbjoLtYeGgcqnT8iVfuLr 5CGnQlt/U88J9eYQM3p1FdKxWUCogBCJ/M7AyxNvOWEVdcKzKIe2vz43Z2eJcxKDUm8LHN9a5cmZ ltBE3X2jua7wYDxLvc8HZi7D6YT/1gUOfD7NohN/eixJB4Tg8tuSv1ms7wNLaVgvYGOaf1JjwDe3 af07tBa1aKV6ZkMnW6wMPZyPDKr6tyGjNroYtPSblj72ntSeFjNqNTh1ANodq0OjxPg716zrzzrt W9vHuaHokuEJz1jSt217qpPVESC/4xwccRdE8HHf7hGyGCNSt7vLb7RlCx4W1SREn1xMiVXp1GJO cxJNaqb2qGNN6OFkSyskWb4ejPOppfIKfRC98fmJB084aQ/7o1Wmq6jEwBphCfOamLXgeSvYSlUD Uhbmi86WKbQCRPdKa8CaI5opa3fEQGdhexOure8GCcbp/Ypl2iFvJmA7shboeULVeBI31hM28aRl 7DcJP92Zrq+vQVYYHJrVloAaMegfWWf0B62vdN90HjkB5CnbYr9SmU6HkfvaWDyJpkfM/SvEDfYj 3O/hhJAzgOWehTFrcdU9PmerjcHFeRBEs0BDEGe26cz3075aFC8MYtnes1A76aZ+sXWPT8cBYmR6 JK6wra6zK01g0+kGDewfIFZQ3uf9RKIzc8Agf4nrnhMTAIuB+9DghoupbhYsVGIhIKRzQAqTTLhT WBzfDbjc4i37MinhGWpnq+Y143b6ehiL/++wbhqeokpa309vOvMUuEoD982aBU7AM6XlbD5o1Ahz t7ividdVMFlvCEYnAqdp4221G+XGBiZov2YRa17Cb+Ca3jo7vEK/HqdW+VOudNoy8hkx0nO8UMNa txC92VK0k5jml98vj0Gui5RPcYdPPWSF1Or37Jixv12i4CWZurgHp6stufa+UIFjbsyAGtrr+m/g Himmaos3ISg2WLcJvp0ZGi7pQYWO4Gmp9uegwVpqxjNiFscPfmcoH5nYaNifIDAbdfwjxdhxncOg 7D33BqWtqEg1tXIXwvpyir9C3ER0Tja+FUJBrCq+e0TdMM2ebIlD/StY3cRDb3v5sVvHDKq/RFkU sZXuImk4a4OHSOprGObhjN+sXZzPA30obhEVI36DSDckF+r1/wPvBQlD40nguXS4RfyZ7Fzk55BG 7CSHRUhtnjULyCq2ZAepS2zWcGxT//qk66jBrb5P9iZzBATsp4FVktBMGh6Ja/j4nZ4Xsl7RUdyp jjPqbmYtGsmaVqWE9OAQrKOg492QT2XaosDhfpZDigKA4/sSKVLTLau3JOkdpTXon/KZ85yMJJrx Vfke/aApCd4zyoDcOtufbqkuxABzm34yeCxT+j383AscYNkY23itsccm0QF6hRhIiIZhL3JGqt+c Y2gclNO2u55QW0l/24cRB3dlzXIpMo3SX1wN+XSWDv0RhsUlNiYT9vLYNPMLNqzV6DlYzFJMU9Rr ZHLBfDlzdrNULwOljWQZlN+gfKI29ko06EVPSyFFVd2FjlpinKqniCFEskWuMY4sbToSf9gAIMV+ k1HgQg6bRLPf6sbVgZdzXtYQSxqpUSdsXbIVi+Rh/QwoBKNseqTncjrCREid2VzENQYbHeGk0jyg ox+KpGsi0ojIPTrWVNgi1Yz68+OQZo2+NuIm9ZvxucEpjmCCFtSQLrZpS0v5B6TzZ8LqXBQ49Wy6 vM1Rzk3n09O7/T70sjAdjDYk54aI/wsqzsz7DC2HkrbNluYrJnXC9JZ1RmBLk/I7fDSqDipr/8vT MmUcZTHPTV6s15KsgUbs7g4ZOEK61Xatb8nAUq5qRy+yIJOGaEqReWZCf4Dttrm30azdsrCZ3jVT 4gqp1Ut8iWRP0UikpZeVaf+SOOgWw+UV9pnkT2eFjQL5ddH46N7MEr6HifO60Uvrubh6AofVrxex IFRGNKlVM72472AcGX3twaymFyCKIX1HZ3fhPYjmOOF+2k25/kX6Qcrxl3RO6i9EtshgGANZJ5T0 AtPQa3ZusUfq0mhby4kzRutsnNnmVWs4WDXukzZSTzb2DPt6tXpct7XE/OuqfGZoG5mrBym+s34C W3JnedNpNmfQDv1qUjRbT2L4d8izwkA3Oa3SlHf8pYWM5EwomGapS5ZYcm5MZWE6RMuC7a1/BS/D YLkgCxzHZAKsTr9TxePs2OlwQWL0pB7WzZKO0C5lbXW/l5/AXAwztF+vEk/Y9GRrn7Suopm7Btb6 t/luKOrZi3VZihg+mcem41pb3aD9m20qiPNYR9Mh2TzZqnrUrajao3lM2vfJC9458UWOZ4XWulYX DNXvtTnLeKqL4kVUkmmFvmnfC73fXXGFVEcdal2+Hjt4fZIYksQc7ku3+D+gsczsgmXdUp4QV8Dc MiTZ44wnNCCgkOqcMHYRFGvN8vKObkDUReFdp5iXBb15STthf6S8RLu9H2hBpYAr/e9H76B/Kh3a NNhElwPaogz7VYA7M/NrFK9g/8+rjjfwdP46Jdd5RHJ6Xaf8wpRn7HD3En7Lo3t00/BNSyMVeT9E N8DZS875bFa++kl6Rz0pLuqf5/ohH/7OZRhbTYxkN9Tocu39pQWDOo/MEx+OVP2xljMNU+kxWG8x Fzp0yI5QqXslz/jodchWRJKbTTIrib0Y0N1Jy9vj5QQp8Wk/sKavSRrkG8tv9el7tS3tqt1yVyKz R4JLu4V+P2DOeXhFtr6B1XPWWySdqF7jxzlv0pXRnqA88rEEiEORlIUNn+hGZCV7CTM3sWai3pf6 reiQE1kamHdNflahm71Cc/YuZxu6rc7G6M/5BX8FGhPkHbQq583pDq/vEWDxcTHR7lVMA9K+yX4f SmfcvITl4yg+qYufyWWmj+494mepNSkPnJTsC8P+aoMAhGCGnOtn7CPtaE7oCVGvme1U385wU6d2 emkWL7YpFLU8MwP7qkwBrpdnErknmXk/LczaYhY7jOsdrIxr0ajQ9uDYQvqrkO0r9yanU9WvfMRE v1l6adb319S0xr7ecnv9OlQ3SQxvW7fIOAZ934uGTp9oc3EsDa1e0la/fje5NGsPfFXxx608Iy+S Z4Z4fFtz4yZyzVexTgI2gMj6SXqFPYqmpyQW2LmFR4EL8QzRduOjdW4mHM6fBXNJWtrmgRAsTtkq FL6bIX2mwyIlE+XTaCWXsY206egf9+Zsbw2iX32e1s4bxI+jjmK4fqe+oNuUPxh8jp+KBwjWHkcG es/WNF4DX+s836WFqXNL0n0rTbFCcTrU17FSp8b/Aq5lyv2dsnU33eqn4Vug5uMluulukX4eKd/H ORXiDAuzUBqT5uJFiq1R1WSPzv2a/qMyT8pN0Zbi/fLQM8t9jh5zvVdjiES49T7FVpzNMIXys7f7 K9DoJqEbzo6OCKE2rYyp/SzUR8FnxkLPBHqFGp3t79QZ1WOmnfUnrXuvzAxd6XdZEoHJShjWs7pH oU2jCq73EOBd3omL+6FPvy0Xl6b+cqvWz1+pv0KxgDjsXBx6OsQUijGUTRQKhULxfkjSRqWQQqFQ KBQKhUKh+I1A/h2FQqFQKBQKheL/xn8mwENTCmVuZHN0cmVhbQplbmRvYmoKNSAwIG9iaiA8PC9U eXBlL1hPYmplY3QvQ29sb3JTcGFjZVsvSW5kZXhlZC9EZXZpY2VSR0IgMjU1KAAAAAAAMwAAZgAA mQAAzAAA/wArAAArMwArZgArmQArzAAr/wBVAABVMwBVZgBVmQBVzABV/wCAAACAMwCAZgCAmQCA zACA/wCqAACqMwCqZgCqmQCqzACq/wDVAADVMwDVZgDVmQDVzADV/wD/AAD/MwD/ZgD/mQD/zAD/ /zMAADMAMzMAZjMAmTMAzDMA/zMrADMrMzMrZjMrmTMrzDMr/zNVADNVMzNVZjNVmTNVzDNV/zOA ADOAMzOAZjOAmTOAzDOA/zOqADOqMzOqZjOqmTOqzDOq/zPVADPVMzPVZjPVmTPVzDPV/zP/ADP/ MzP/ZjP/mTP/zDP//2YAAGYAM2YAZmYAmWYAzGYA/2YrAGYrM2YrZmYrmWYrzGYr/2ZVAGZVM2ZV ZmZVmWZVzGZV/2aAAGaAM2aAZmaAmWaAzGaA/2aqAGaqM2aqZmaqmWaqzGaq/2bVAGbVM2bVZmbV mWbVzGbV/2b/AGb/M2b/Zmb/mWb/zGb//5kAAJkAM5kAZpkAmZkAzJkA/5krAJkrM5krZpkrmZkr zJkr/5lVAJlVM5lVZplVmZlVzJlV/5mAAJmAM5mAZpmAmZmAzJmA/5mqAJmqM5mqZpmqmZmqzJmq /5nVAJnVM5nVZpnVmZnVzJnV/5n/AJn/M5n/Zpn/mZn/zJn//8wAAMwAM8wAZswAmcwAzMwA/8wr AMwrM8wrZswrmcwrzMwr/8xVAMxVM8xVZsxVmcxVzMxV/8yAAMyAM8yAZsyAmcyAzMyA/8yqAMyq M8yqZsyqmcyqzMyq/8zVAMzVM8zVZszVmczVzMzV/8z/AMz/M8z/Zsz/mcz/zMz///8AAP8AM/8A Zv8Amf8AzP8A//8rAP8rM/8rZv8rmf8rzP8r//9VAP9VM/9VZv9Vmf9VzP9V//+AAP+AM/+AZv+A mf+AzP+A//+qAP+qM/+qZv+qmf+qzP+q///VAP/VM//VZv/Vmf/VzP/V////AP//M///Zv//mf// zP///wAAAAAAAAAAAAAAACldL1N1YnR5cGUvSW1hZ2UvQml0c1BlckNvbXBvbmVudCA4L1dpZHRo IDIyMS9MZW5ndGggODM3Ny9IZWlnaHQgMjI1L0ZpbHRlci9GbGF0ZURlY29kZS9NYXNrIFsyNTIg MjUyIF0+PnN0cmVhbQp4nO1dObrrqrJWpMk4ItJkTkREdGbiiIjIkyEi8mQcOXmieiRkI69m73e/ g7285FYU1f8U6Pn8jVZysfdfOedPt5JT8Mtlvsxumma8X+rjdFm8Dynf/3QPP2orWd6tRE1uqrTB gXMXtz7O9QjIXAldiUz5T3f2TCvRu8oeB5S5+UK39TUgyCGhld5K6PryEtOf7vRQWxk2QZ+RXSst RMN8oReBtmlGGutRJXj9VPi7CcxhJWylBDrMnQdigIfrMb10AUr5BhQDS5f0+NM09NrjWQL2HZQM /jshEfm1vrx4uTmgdUbWIf8qB5f419nSkozlmC3j6m21i6n0+ryanRg8UUbDUPn36/1/0ZKfVN6k o/VgCWmEESUGRxys/9bh+EvYV6IVQOgc6Nhq48/1MKfF6Rgtf4GDKB5soeEX2PizhHFbTRKzf3J/ WDqLr70QXwaG0s3+S4N+jwu7iyl+V0fPt+InMQJo+lbyvkYZtpU+tqF/wHbWMxawIiSONNR+I0mf e61HWCiUiX8g/vSoYxJirAfhe0cZZP5P6F6UcBHd83r7iS4ECNqm7xD14ZadcWcQHm8M9/dJUoSE afK/JZsiLvNEBHZdbtW3+/MreoctLRB//o5sJvGzFJFsrci3twzOffl5u7naSLYf1fCvjBvRhy9y 7xEq79xPe71EEZbjTPOYayqS36Evoaa6PxZxlurcvImNa1pzMJY/kZRhxJDQx37bj+ZbrndCrjYN Xk7rQ6pH9VMJ7vCF9RGeZP4NeDnhi8Pnl6NcxcV/I2kIyiFVmYgxtxuTXP/dSrll/Wwu+lbZH3/S l8q85TvFwlC1vRdkG763+8hN3r6Z9/i4JW6U1ORIzb9JNoFBudyK8umGnU74FAgzx/IxIi8bIuH4 BvfPenOvzPs+17NlnHRd5O6WE7aCZBJh9JnUY7mK5Wkhq8z7NsVrtKXpXgE8GQDI6TJVdGvNvkPO RWQ1l+ADU9pB1D/yEdVsflc0hnLEUnVj+7j+RUQFZsAVHMNz81RxExTfktdXeTTo6zeidfXMMcZw 8rbeU/XoLjTf/TSw3pg95ksFy6dJIQGBV1dOThfnUxXHXDGxzJLaMu9ZcN6AEPfend6nU0yEf9Y0 a3IGs6l47uecE7JyEdIQZmW4kVArwIkgfKlzHW6piNbUJW3lXHIMaBKoubvxq/Lb9FINjcx4roR+ GphZH3YjzvmZcdbqeCKaElBA7uzFIaHriT1JMYkjCzj/PM6XtNT0aDXIocDUcvvUfmZ2zWLSo5sU b43q2JHCRSY9CHX21rTurGVtacuvZjJhRyJjpJOgG7P7jDafjHGEe2UbdcClTvBSZ0JgZLF/l8Vo rXi+1okXIIEFcEbIYt80+ludj1+ExE99Q5rQMhRxcd7JCC7qutXrgdiyBAECwcKovE9l48SLc5O9 LUOdy57h4LHPb1qcXGIzjv33JC4rX7xoEmokdBzcXJVclSLx/o1FaU+0GDj+ONvYtbU7OIRnCSuV b2usI3yrnUrSgSpuJnABt1YP7jUJuOeFTED9BXEjGt1sicst50aCM/Tja4I5r705S9x6Rsig2EgC bYIIVWnLrAmqciqAHmc0cHiKaO7NRiimidKBQzvRyeQu576ArcDIk82AfzfPFsxNQUJOeCtZCuEV ADYrmwMbHWHbrRFLCC9NDlwZcSLkrAHLeeKcjDsPe1JrNhUhmkQWjaFYw5I9ddcjYaxzt13KUwXM q9Ktj6f6WQfxbJKx4KkSD/naKX9h7zUtuYguNZ7ApGweZ04dDwHztHTEkuICmkY/1VGI8c7R5iek JOmYMzo0AVTZCJrYk5sKXi4YRjjhJkdie4PyXGyIes74VS95gnMFrRCcTzi3GvhZw6JgOMERjCGV XBpGGBR26dsm/JKxbCKUU8Q9l5Ocy0JGUlZ4Cn0A30ZZ4/4mfmTBpIASOKGhl7K2yzmJvc4RF8cd I55LXE5SdngbvLKeiW03dDLr0GSuP8EMNrzdnNBLalG/cY6422UeznlKY7uSIAsI2rO5TCZkNMwz Qsn21QUJzm5smnqcM0HKOeLOtYQVMReML6SzUDpyoTlPCCwTBR7Cvxsbw0Lfq8PkzRDI0ZY4p0H+ KZ07C8JQLIQnShwzg/GbahyBb0eJlHPT8VsWg18fq9swhvLG7N6c04seuJ/l3MLFWjMyjvvqJX8C VxsQlzWkaXoEf0BI/dLG6vSs5dLkbD9FWXmGi46if7JFvIF1kHdmyrlq/29qKNUL8LPqDqwWvubc PGwtPwOeizMUlKexD8lmJVVuvUnTkE/gBZKhE8fGQLL8hS1xM5vKUZ2rQdtpaBeEjwoUMmIoN8qx G6UHAUqZ0z3rAArHLIY+6+NS11qaQR3raZxOgc+UwxFOcKneg7p2I6VTKONSK2rW5OCmVuVmFY4p yxZASYUdy3ZIG/xkrLcJe3iCPPE3a8QLLwg/NLgk5YBayjniWxoaG8d+y9anN15wc17GlYb93AO+ cw7TCwhZQT6aiTh1vcGZ4a3hO5pN48Qaj6bgCwGD5kOb8xpXMMy51SedIq5MVNXqmOWl6alvkbYZ cVkk75bVltzUpbV6WC1O6nPOGKuRrt7zdBKw/MdAokRuJieOimUrPy3QGAyftjy0jlA+tTmxtzDs GOcqs4d17mGsySSzYMo1TFqwBNEkX5dJyDM0iM7dWmBIuLk5+TJxbYsjZX/X0ut6g30z8CHjnNpZ 6pqn6soW+QaP30wL6BN6luxbXc7xud9xrn45w3TFKOfQDfDwSW6rKRohmCU6UjVdEcGmroFpG6si sQzTvDn7Ir9Uf/lFNzEyqYa7jsMJg9Jh3NNMkor+LIJ887IH6llF2JMCCoSn6Jy4eME952i8sM9v 5hbLdZnQEY37uWSqxOXnjV+mgDGBaGoIL4YTpweWaBAxQ1SSICZ1Occ/CEO7uMW79eGf9c+Zo2VZ YOkCRBCnpnekeHI2/gN7dKPYsTBWsnCMa2fpENtehUXi/0IRiYrrAczgJW7mKSqKkijDoqM6sTk7 wcnG/VyaxDgYc6W2oUZSmoxGmtyccZ7TBE/1BS+mpMnLjSoecY4TSQM6mKN6NhxEXJgxLJZOzZUZ kG3cJPkbrgCh6ja7kgUfAkSVrICtiemhXyZAEaHgXzQq0CrCMOeSjpNVaDPwTb1QvXkWorlhHYXV UZSMp4IkjOtyjqdrIWvccMwe0amsM37bFpUzOx400CZzY2NReRJYEre32hFwDLeyCb5ex5Y4RC87 uqrEgusXhmPLwgO/8rv9KVGzwlaPOVdgmliLEqRMnbhYodu8EUlm/HZoG4f5vrc4iTQPco4xhHkz CavqdlOrZ81e4jxT+saDBNmsyGRRaPq2F0uv+c5Y4Jy2xuF4IOqcjqhxO19tZjaIe02omBE1b5IF FbE142JOt6D0IefA9A50GcOpMc6lKsQOJtTa2XM2CjZOYT6qX1g0zmSFn0kTgwlLVKx71hK90DQP QnvLcGzpL7CidNpF2sYBmEoUIOom6EqBmiUTh1GiDsfLBvzqiqXxc/PgXEEajlCAcTjy8FyQM/VT 3EXlIb8CD74VTV6TNQE4TQpqvPh2cI0vG83ERznH+rx3HUYaS3OsYQe+ek+Mm7VOeJU0b+nqi+XS +LYx4vxgVuA0gtqcdkOF8QQSq1DFYa2vAVBlY1zmaQpiJI9cAc0gEa/HiItjEUqZJArYFuMQWTei jHXmZqJgw08oF5wllhbmiWRz/dch+gVEjhGXxsr1oti6nYpa7TJqxoKpbo/Mqp8REtOlnpcqpnYE DjhnhmOMuOfeLvWaGbXtW8VQZQafeyqVeIUhyujmxqHjOnEINI+LbJ5LE6AOEjfUiqoISqUNnBWN U+yO4OXUMo9GAYox1V7SOrSiE329ZNU3IcB3EmdAr93MuTGOSsiumJ5tBapicGr7ONtM/OFja6lp 03cSt4i071WZOZVSspBxG3aoo8cXQ5seQKDSOJLDaoZTOjfSHpok7QsXmYC6hJRKXtW7bX2z9D+Y LBP/e6kzQgZuTmNiyw+JO7AthTOWSyeF4HryKrqRecQiWBrAlftdIE2fLCvq3HEuOhx9nRPufULc QQuqcvv5Su52qhrRTNpYZtnpRXwnbMqs/+G3DjLxbxDL7r44XsVn/6YYwfr+IhbRatvNsk16v9kY ZCmlqXzb9kH2SfmcOHfprI5UsLfj8EWnqlaAYG6W9GxNKR9Htiaic8zvl7jliQhl09LUMxkCXfRK qIQ/AT7SWSVA7ttWrcGRwploLW+qrnuxpMh9+oJYdpdzR42VOl8RIsgZ7mQxmQwhKwKbVlab7Ydc Mkra83N2quAjzsVu//1Muyt0i8K1EgPQsdkTFIlcauYRpIQPiWgkbTLa2MUtmw+fogp/6t5fneUm jit75fkqgtjZKSaWPFNvvl3yWAl2tHAXHWgxn+xZSyfw0HnOPYAKN++16q4JZi9pV88cETqY4sYZ tBUmJMPrn+P9NjT6Mh/bEieEfVSPDZXOHVufJ/fKvbBa3bTsPtiYxM6CC9RSXwmT6vLsrU7CB3rE iaM7TRuY5k7mmueXom5iKt7SZfIsXzfDi8xZHsnlYlddJ9FJtqd6BggcTCi6Dkc5WfyEOwV1ArAg cEB3RYzRInL0qEGWJl57S68BZwKhshPXqqgQlw7n2H3jOfKJRZH3ZwkwKj1z6HWrli5xOjlHCBUI T0Im6QJO4pzg0Artzc4bfeMPtWfJtjjqZO1MxnSpC/MpLNMFAY0VSJNR+n9loQTrkbUvVU5k1d8i FOmsiBIHPAoSepmUeajVclUcQ/kx0zBTvsyXjiV9qs7VfhlE61LRyEbZbGUeVRohwhCkYtOEL+2i iccyNTqHceB7xSO8bZYZju1XeAJqvnTl3HT/1ojOhSoS2fLrB9NiKjZdC6ezPS1NT5Z2Fkx2Nbs/ qMcr/ZUrD65DvD/uOcV6HixhmvqLm8pEgeVl7o6UWc9J4aXR/NUrCLrXkCZ4Ey+XuZngrBHLR5Ws C5XqsJLWr9d2mahdNn/CaJy07ic0ui4PEfr7jkCxluTGXbOP48o+D8UZuDw8Bc+rgqF/nYUj2Yjl vd5Jspx2F59BcMPmGR70KZUm8G6FVVC6NigJF/rLB03IURKBWaIavHbTuVpDYWuK0JLUuiErkUbG n48QvIeF1/p7pqDF4NZko/WhCbLJxnVRhsgYw8HCz2bAYRwWv/aqgRCATab+GTy987IgVCeLUTpB LDPv62kqp/nHTDZhwVqlV8Ueg7wD+xqYtQfQtMKwWMK9OuRHQRH8h/hmx5FrLPy/uTGO1umjWBbv Fw9/+3tzxDdzKE/54WAfJC3t7b8v3gvixfV3oJMsXil6LhTgpfeVtUkLvsSWGBCiGpXuyb7athZD AcN+WCASpZFIY9vhnlYiQww+hJSS6NVucsEw8He20PMiV/3zFZZL7R1Np6pv1gTVfAw8iACb+oVb N5/7MeJeBShPYVs78Lci6iRsbUZAuCWlOTfzhR/ahHsbhui+H3C+3Um5Pze5F2FDbutMeRRSOxCW ecz53+LczBMWBwal5A0FuYGZSdZ4nxPmoKngZptSOEj5Oc7tiBPndKxzr9od/z3ob7Tl9DO3HXEv de6HWgmrdW3+vnKHP3hsz7JIiPGrG5wm6BX37ou0Xes9+tUdXTvE/T7ncqh9WXuD/yMfyeP2+cEj HBj+tWfxUncAG8X+HnFvWDbGNI+PAQitgUR7FgGpzq6S/1qrm2ZFHPn+/T3bICjCxytzbhM/6/ro 39a5tVsdbnhRo7dcq2pG7Iv8pD2Lztcecu7O2yR2b/SvU6z2qmXgXCQWGPXxww2Y5ZF/9Fs7zkly dNi3jPneNJsHufN/LNQDAKBe/iO+cdPIOS+WILKUTXRBinl6das2EHhFgumZ5e1ZokzOHZb3Ierl 6AGTfLnDvltOs0mH2Q9Q7MMhiZmtBtnwQAoEeygTmAF1hPhXX+RHuASHqxcKqN8SAY4dnUsEPL0y KLj7nF/M9k77R0mKyfhyqVx/6+oEQhSVb2T6VrGsSaMzV2lY73hATyumUT9lOHcVqd5xjtd9HxKn XZL1RA0wYEgzuBfjLN2tubMxCihTHjnHPsvjTP0GVpgvC0kgSiG6AGNnNmeZeR0nWJo3RuHBFbFC j6tXLsmEmq2BZgyLWQCDWfplv9VtYh9FXAPpwoSX3HL0TREZywELINoP5LxXw9SeBXHLcQw7yI7i hKp1rpqQZHcTEtKa57fkZY2bWHG86g46Mb9YgAZuPrIUehmXa1Ct20qfbFs3tjPYg2eDWfy66EQJ jBXh6ptqaJoIKKkXDpEPPB+wlSdrwEjwzMyST7Mwe7K9286IWRic8guiYKgKB4J895OsvCHoz159 ImsPyZlfzSv4EIIz4B4AdOSzWUXJwirrttK3SB3pe8JgPaXBRF/OX8fKNUFV64AYAU4RnQBmKhhd MC85UrkGvmgIDedylVDrqlYyUFgJ0r3pg5dZnsEIgySFTdjxB3XN/Iw4takTKRQOgrEDGpVzGumb crf6Dw0OksbCG4253FnLOp1M8xuDxE3tStUXn4ybMnUz2Z2CWkUKD9lbUYwJWiTyARSiWvGHvciy hG9ba1nXHtCpB+czFzGV8K1XH22uJwRA9BPnojTlERspquMl0g/Rerg14JIozUbMV+H43loW+YHB ZYROts98vxxMh53oY+lIauA8ZjiBlTAG0qyVIWb+oE6te7Yo4iV5OAJxvtsBnjEfKCNodvd4UzOi q2DokbWOraWOunTWKxeDBL6gCmz1A2te9GRgPLu7DnHTmfIPT1vWvjUoa3voTBR2kZ1p5hRM8jfP foECFnhF4zgQy9hyjgaBTew+5ZFyy2lsW8nNOr531T6LiG/L6KwOigmSMNoHecd+tXLuSqRc5YHz 8dhLeapF4fEZAxp0fcNLa4kCHtqJNEFHU0Dzf2WtYQspoSb0dTIxymrx1PQwjVcNbIDX214UCVDH okvJfEYWOCRROhRjrhbJRiChh5oQSC60vtOGzYSYsQtA+9iq7a4DXOszuO+sb074Jq7JxvRAIAbE 3ZFzApSwG1c/5/FNw/M+50hLfSQF3rOCzz5WLScwLvLi+INQV9HoXC0kJqLFLjC0w6HYjnM6PS5v qZvT4K2HWz4hRqHzD1kUndEbMCjZ5ul1bp/EMhEZUdwa2XUjnOsr8mU4FcuxhiYM6l05att1oOjQ DBFnhPKFztHeHtaLzxo8Z+qkx+ApSFomgAFInklA1gN+h/McRidece4pCjHk6ezy0nc6d49qWPEr dyaOeCDZqaRlYmdiCFrwwn5O3lX+CtN3cwXAC2LDZWjjWc+h/kCEItGlcI9CIIIZPKPObRZK0Zjf RN6x4ataFoIw+5xLvDB/CFJvC7MPmY0sMjBB420k/FJM3AhY5KxGrCywXkfg2poSDQM6/ZAN8kc8 ndkA47W1fMqFs3QoOLZMBOgYj+1N+sLJkKpcJZAp4M9fg6Q7bEY7fVBAboC4RTOC+aCgDFoVQG/H oe5FwW9mk2r6oKm4WktjUPhGEDwTRm5PhqeDodQWtyP7SvcaV9Dbc1m/LBsb4HUvTDVrIhccAltK zsmJVKS0xXwjeXedJREXTl/vmfsicu3fUPbcLgzuiyUhXbqaZ9pIfcYeCkhwNXy48sRG8FonBzpn OEdSGTmzpa8fcWNULjX3f2ctPa3qrA/w4zpsGbvoOS1QiOdKPa59NutNZ8rngnEXkaA+spXVZ/a6 kQg6GNkDrV0CsCXOsJ3iC0T25oY2RL/q9WeIgRIuhqihdPCNyk3KXOsSPZufI87dLwgyjlyRot0J /JBzRXckoCkDO248V0DdYwtx1SlJYIlNKDBCkZROieSEPOwBooYdI2vXeC52eunEI17lheqGq6Es FsRIHIOYCXumy3PEEkIzWeDURNosPga2SteeE3/uMbAXWIpvoYPeR4psvS4x6NJeRTALYcw5Juhq OSdnAsMsEYqEkgx/CRDf77Jju/ZWLu21o+dOJk67SlFVOI7AsnVAObTBRcs5NipXG7shtIeqKlNy 4hQoVep3Wf1ler6+BJeXFMuKJVZk5+TrjkuKoxLguPuRHKJM7RAPtbwhCntmkzleZp1o1lKOazN3 cMC5uxD3Lu9ZDNsqzupgYzxfLw41TaRhDRLRM8CJ7aLtsLWaqFjtLoycwl4D2yDJWonMI7HznHJd ymv00psE8oJzrNIBRxCodujgCpvZlGpIj68yx80c1LgU/ZwPakaMWIc3nJOl8G+3QZPN7zVSMXqB iSHNTx5fMDsFnqdRtID7fJUYRHRl5nyOwi0fVIJt8nModXrpltfxl7d0WUutqwKwiPvVj2RLTyAi PUcrYjLtD5Ofs7mOTORxdntoDXll4+UN6+w2WOtjiustwCPe3pSe4zWPEynblVMYRmSVc/WtVkBU gA37FP28vuDcpsbtNXGvsoKRlsX9Ek2BStOuQTlnZnng4Nr4j8AlHiYWOPZjiTfOfM063wrkGDFb E5WRBUzP1XCOomEQuMajTo3jjwwuSQL0Sufq7g/8Sw/sT5eDRiwPU563LRv3y3UXUWWOUvSg8TkG zkzY3mJSfHN8xiSXWXzl6wbCr/ctmXpE8XMS3QcWU7uCCbICEViZTRXOXY/DL2gy+Q9ad3Cp83YW /mPOWe90NeJGPUeuGFdQwwvBphna4x+RNPDVKVUCjj/EWYEbRl2aRpdWUwBPlYaU76pPGnCJOafO ral/qCHAy5lvz26FkPX+9ik6Tf1VztnRVy7KYxThp0ycUbFr4/XNJOarcxYZpWMGGwxlfoV+vSGO ZXGHMvN0osysimVmgEi57AOHpiSdL0+qs9DpKKlrN3r6AucaBFbmByQsE85RxrtmYzr9I/PmlLlS jvcmXxPrdHjhbr20wOVzziWbUceWc8y/lV4b4DG0J8y1GN+AtXyKYFaX+crPfewKaMiywiUIW1nk X42MOF71c1EQzijwLH/hjVjSFH39raMgzOv5xiOUbUuKeyjQvOMcXyKIzhdY5wLx0Av/Aw3Vm9MW qlyejqy8XLYKT/kZcUVKSIhITyGYF+ahnyO6NLYkNSPOMf8pj3+LkSSog611dm9WuaJgfkZcYqEy ZryZ5UEaVefqUWTwFfMk8eVXnf96e2IPFeD157r7knwtQuHJR60/UYMQQ1AbD69au0wRCknglaMw QZPg/vb8RfCfrmBqQdYXwq+k4uUZPGfDGYNkCzbLB4AoKue4vs+w+rUTpzPDhcDBYnaMCuvcl8Qy R1pLoJyDGR8vBUQ2tiTubUIaLoWQCb331rI2T4sNukVF7S5WH/m5u3KOc/H6zBY9oTXUyBLDrzak aTjnx3TuSRcVQ7XDXToem/c0pfuEOLCW7WSjVvRyVBxwrsAU4EethuaCRgJe8OMD1rK2bNRua1TM Rdg/4BxP+AdOcNh6BHZbBvEJNlUF9MuE1VakJXR5d3qwZwn3465Xn96+v+zL8Ma3Q+JmraW4BHYL RK+dNubK+zau9mZWDxk4dvZ/J0eXtdwqqWY7cHSaLmjMuShxRlBnJVWVfm7025T/Gr5xIBAHIhSW Qi8528aXm+xYXcHpi2aKXWAtExfGKUJlCUs/qoHgRoGL7QllouRnUOdqW6g65bIpCWsM2NHy8nct mVHXZROWJfVA1zyIWJJCKgNlLmvYWtYmG/hUh9BWKGhUNH4NkrblyHb9yk4hqg/gRXXOTHJW9We/ wfXCwWBmfsjP3VnGiuuhtMHAzVz/cKKR7Ul2ikCi+qtoD77CUyw4nAA5Gy6xBzBPTlwmMbkWBsJd 8cxGQPCWIXxcRLOEhqI/3GOF2b31p1WOeGU5zQZxWKIB95mRTuLPYGup+pKXkhXewGagdKXzywTH GaiHLSeFYuvDsl1r6HzCaCSKgZW0aTxCkT6gqYJ96591UzG5lodJenC4BumTTFztt29sCYWclbZm 5yX0dpLIgRjqZCUXco8SBt3A67xVGpYHzuNzNY7Z1mmOp714wmTFBomek2uInf0iRY/iVms5ifdB l3Jq3MW/crIXZDMvkyxzMRWek7zpY0o5p1H55JJEqSAVgK62xTlaU6jzErUedAaz4mBbKS+kcSo+ HqEodTBmsk9am2GRi51gVW898ejQZYMV0xHO8U9clcP61qT9buK9qiDS9CKUbDcHTy+SBqsBp8mc DMllfyBrx+v/UaHPPIEoYF1EwICM/owojsEYRF5YK3QKlpl+nnNoM1mfebOlzVZK/Di8X2qyFUAK pK9EosTxI29nEPiZHGj4ZsrATupcbWWR5befxSOm3fEvq+v28g/jSQ2G6TGQqQ9qYeVTMtNPBveD HmW8FOPF9bYn/6gl5RgnpkHmN2gmMmr9OmmW+HsvkFDD+1NOXJuXnLgWqJ3P37TReoNAmAK5AwNE KtYnswcWrZXZEp4y0OU9R4Vt71rRWur9dUQ+oC8FnkKNJizRcPNqeEqG46qV3uIcGaj4Gufocruw BffZWLnXYMKfINlAfkCcFSfZPnCKIJMJnuMZz1LsOSHwH3GOIyaIwy7V0/gv71d0f9wf5Vke99W+ lsf6vz4v8Eq91Tfq64We1M/CC0/6SHniO/Teo8APPD7oE6uYYmIuPewbH/zW39eihif9bU7et7N4 xC+2bNbcf9nl/XXtzlfcqXiNbun7v9KylCS6Kf3FQnauCSFcY7DeaaeXn6fx9zYlq1tm41oI3sjm BwWz7gCUfl5EDAXJOcEADgp9v6MhSflTy/xxi7Trd7Wb/k1V9KcNRjOH3zLLhoY7bOfO4OX3yyVG Cflg++mfb8GUNUMncK8TXmDGVuBzoYq7xRa/2IrWUkz18jQvPnnmZykr8sufjhMCxprVra/pQq4d +0j/tl9Ky18RAiUKpmvUcrS13kjT2tVV0+YftMHn2kNLB3e+YX95h923NwfJOxf/qtAnLaZ4sF6r 4IxwqqmFS6//NUzTdo92B8E1dGk2tzwmVSgrK8vI6v7x1pG3e9QdasF+Oh+6F2LctgJXalkV9q+g 7LilgOsOkMIJJkTrPAKWu8NVaOhaNPVZySl4D1dsWY5XNf1N7Z4DL3kkGhGQX7xbQiS4qm4/OgPt FcgenkD5O9qqQYvbXKYFaaQJGwq868VE/nRfP2v1Oinem5qEOulQGelmv8T0/5WsbbvrdUL+V0j6 r/3X/mu79n++TBPpCmVuZHN0cmVhbQplbmRvYmoKNyAwIG9iaiA8PC9UeXBlL1hPYmplY3QvQ29s b3JTcGFjZS9EZXZpY2VSR0IvU3VidHlwZS9JbWFnZS9CaXRzUGVyQ29tcG9uZW50IDgvV2lkdGgg NDg0L0xlbmd0aCA0NTQ1L0hlaWdodCAzMC9GaWx0ZXIvRENURGVjb2RlPj5zdHJlYW0K/9j/4AAQ SkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAeAeQDASIAAhEBAxEB /8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQID AAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RF RkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKz tLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEB AQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdh cRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldY WVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPE xcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+/iiiigD+AP8A 4PnP+cXX/d7P/vo9fv8Af8E8P+U6/wDwcVf94jf/AFjzxtX4A/8AB85/zi6/7vZ/99Hr9/v+CeH/ ACnX/wCDir/vEb/6x542oAP+DXH/AJQUfsM/93M/+th/tBUf8E8P+U6//BxV/wB4jf8A1jzxtR/w a4/8oKP2Gf8Au5n/ANbD/aCo/wCCeH/Kdf8A4OKv+8Rv/rHnjagA/wCCeH/Kdf8A4OKv+8Rv/rHn javwB/53rv8AP/SHiv3+/wCCeH/Kdf8A4OKv+8Rv/rHnjavwB/53rv8AP/SHigD8Af8Agof/AMoK P+DdX/vLl/62H4Jr+/z/AIOjv+UFH7c3/ds3/rYf7PtfwB/8FD/+UFH/AAbq/wDeXL/1sPwTX9/n /B0d/wAoKP25v+7Zv/Ww/wBn2gA/4Ojv+UFH7c3/AHbN/wCth/s+1+AP/B85/wA4uv8Au9n/AN9H r9/v+Do7/lBR+3N/3bN/62H+z7X4A/8AB85/zi6/7vZ/99HoA/AH/gof/wAoKP8Ag3V/7y5f+th+ Ca/v8/4KH/8AKdf/AIN1f+8uX/rHngmv4A/+Ch//ACgo/wCDdX/vLl/62H4Jr+/z/gof/wAp1/8A g3V/7y5f+seeCaAPwB/4MY/+cov/AHZN/wC/cV+/3/BPD/lOv/wcVf8AeI3/ANY88bV+AP8AwYx/ 85Rf+7Jv/fuK/f7/AIJ4f8p1/wDg4q/7xG/+seeNqAPwB/53rv8AP/SHij/gr/8A87iH/evn/wC8 zo/53rv8/wDSHij/AIK//wDO4h/3r5/+8zoAP+DGP/nKL/3ZN/79xX4A/wDBPD/lBR/wcVf94jf/ AFsPxtX7/f8ABjH/AM5Rf+7Jv/fuK/AH/gnh/wAoKP8Ag4q/7xG/+th+NqAP3+/4MY/+cov/AHZN /wC/cUf8Ff8A/ncQ/wC9fP8A95nR/wAGMf8AzlF/7sm/9+4o/wCCv/8AzuIf96+f/vM6APwB/wCD o7/lOv8Atzf92zf+sefs+0f8HR3/ACnX/bm/7tm/9Y8/Z9o/4Ojv+U6/7c3/AHbN/wCsefs+0f8A B0d/ynX/AG5v+7Zv/WPP2faAD/gof/ygo/4N1f8AvLl/62H4Jr9/v+d67/P/AEh4r8Af+Ch//KCj /g3V/wC8uX/rYfgmv3+/53rv8/8ASHigA/53rv8AP/SHij/neu/z/wBIeKP+d67/AD/0h4o/53rv 8/8ASHigA/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKP8Agxj/AOcov/dk3/v3FH/BjH/z lF/7sm/9+4oA/f7/AINcf+UFH7DP/dzP/rYf7QVH/BPD/lOv/wAHFX/eI3/1jzxtR/wa4/8AKCj9 hn/u5n/1sP8AaCo/4J4f8p1/+Dir/vEb/wCseeNqAP4A/wDgnh/ygo/4OKv+8Rv/AK2H42o/4J4f 8oKP+Dir/vEb/wCth+NqP+CeH/KCj/g4q/7xG/8ArYfjaj/gnh/ygo/4OKv+8Rv/AK2H42oAP+Ch /wDygo/4N1f+8uX/AK2H4Jo/4KH/APKCj/g3V/7y5f8ArYfgmj/gof8A8oKP+DdX/vLl/wCth+Ca P+Ch/wDygo/4N1f+8uX/AK2H4JoAP+Do7/lOv+3N/wB2zf8ArHn7PtH/AAUP/wCUFH/Bur/3ly/9 bD8E0f8AB0d/ynX/AG5v+7Zv/WPP2faP+Ch//KCj/g3V/wC8uX/rYfgmgD9/v+d67/P/AEh4o/4M Y/8AnKL/AN2Tf+/cUf8AO9d/n/pDxR/wYx/85Rf+7Jv/AH7igA/4MY/+cov/AHZN/wC/cUf86KP+ f+kw9H/BjH/zlF/7sm/9+4o/50Uf8/8ASYegA/4PnP8AnF1/3ez/AO+j1+AP/BPD/lBR/wAHFX/e I3/1sPxtX7/f8Hzn/OLr/u9n/wB9Hr8Af+CeH/KCj/g4q/7xG/8ArYfjagA/4Ojv+U6/7c3/AHbN /wCsefs+1+/3/BID/nTv/wC9gz/3plfgD/wdHf8AKdf9ub/u2b/1jz9n2v3+/wCCQH/Onf8A97Bn /vTKAP3+/wCCh/8AynX/AODdX/vLl/6x54Jr/IFr/X6/4KH/APKdf/g3V/7y5f8ArHngmv8AIFoA /f7/AIOjv+U6/wC3N/3bN/6x5+z7R/wdHf8AKdf9ub/u2b/1jz9n2j/g6O/5Tr/tzf8Ads3/AKx5 +z7R/wAHR3/Kdf8Abm/7tm/9Y8/Z9oA/v8/4Ojv+UFH7c3/ds3/rYf7PtfgD/wA713+f+kPFfv8A f8HR3/KCj9ub/u2b/wBbD/Z9r8Af+d67/P8A0h4oAP8AgkB/zp3/APewZ/70yv4A6/v8/wCCQH/O nf8A97Bn/vTK/gDoA/1+v+Do7/lBR+3N/wB2zf8ArYf7PtH/AAa4/wDKCj9hn/u5n/1sP9oKj/g6 O/5QUftzf92zf+th/s+0f8GuP/KCj9hn/u5n/wBbD/aCoA/gD/4Ncf8AlOv+wz/3cz/6x5+0FR/w UP8A+UFH/Bur/wB5cv8A1sPwTR/wa4/8p1/2Gf8Au5n/ANY8/aCo/wCCh/8Aygo/4N1f+8uX/rYf gmgD/X6ooooAKKKKACiiigD+AP8A4PnP+cXX/d7P/vo9fv8Af8E8P+U6/wDwcVf94jf/AFjzxtX4 A/8AB85/zi6/7vZ/99Hr9/v+CeH/ACnX/wCDir/vEb/6x542oAP+DXH/AJQUfsM/93M/+th/tBUf 8E8P+U6//BxV/wB4jf8A1jzxtR/wa4/8oKP2Gf8Au5n/ANbD/aCo/wCCeH/Kdf8A4OKv+8Rv/rHn jagA/wCCeH/Kdf8A4OKv+8Rv/rHnjavwB/53rv8AP/SHiv3+/wCCeH/Kdf8A4OKv+8Rv/rHnjavw B/53rv8AP/SHigD8Af8Agof/AMoKP+DdX/vLl/62H4Jr+/z/AIOjv+UFH7c3/ds3/rYf7PtfwB/8 FD/+UFH/AAbq/wDeXL/1sPwTX9/n/B0d/wAoKP25v+7Zv/Ww/wBn2gA/4Ojv+UFH7c3/AHbN/wCt h/s+1+AP/B85/wA4uv8Au9n/AN9Hr9/v+Do7/lBR+3N/3bN/62H+z7X4A/8AB85/zi6/7vZ/99Ho A/AH/gof/wAoKP8Ag3V/7y5f+th+Ca/v8/4KH/8AKdf/AIN1f+8uX/rHngmv4A/+Ch//ACgo/wCD dX/vLl/62H4Jr+/z/gof/wAp1/8Ag3V/7y5f+seeCaAPwB/4MY/+cov/AHZN/wC/cV+/3/BPD/lO v/wcVf8AeI3/ANY88bV+AP8AwYx/85Rf+7Jv/fuK/f7/AIJ4f8p1/wDg4q/7xG/+seeNqAPwB/53 rv8AP/SHij/gr/8A87iH/evn/wC8zo/53rv8/wDSHij/AIK//wDO4h/3r5/+8zoAP+DGP/nKL/3Z N/79xX4A/wDBPD/lBR/wcVf94jf/AFsPxtX7/f8ABjH/AM5Rf+7Jv/fuK/AH/gnh/wAoKP8Ag4q/ 7xG/+th+NqAP3+/4MY/+cov/AHZN/wC/cUf8Ff8A/ncQ/wC9fP8A95nR/wAGMf8AzlF/7sm/9+4o /wCCv/8AzuIf96+f/vM6APwB/wCDo7/lOv8Atzf92zf+sefs+0f8HR3/ACnX/bm/7tm/9Y8/Z9o/ 4Ojv+U6/7c3/AHbN/wCsefs+0f8AB0d/ynX/AG5v+7Zv/WPP2faAD/gof/ygo/4N1f8AvLl/62H4 Jr9/v+d67/P/AEh4r8Af+Ch//KCj/g3V/wC8uX/rYfgmv3+/53rv8/8ASHigA/53rv8AP/SHij/n eu/z/wBIeKP+d67/AD/0h4o/53rv8/8ASHigA/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fu KP8Agxj/AOcov/dk3/v3FH/BjH/zlF/7sm/9+4oA/f7/AINcf+UFH7DP/dzP/rYf7QVH/BPD/lOv /wAHFX/eI3/1jzxtR/wa4/8AKCj9hn/u5n/1sP8AaCo/4J4f8p1/+Dir/vEb/wCseeNqAP4A/wDg nh/ygo/4OKv+8Rv/AK2H42o/4J4f8oKP+Dir/vEb/wCth+NqP+CeH/KCj/g4q/7xG/8ArYfjaj/g nh/ygo/4OKv+8Rv/AK2H42oAP+Ch/wDygo/4N1f+8uX/AK2H4Jo/4KH/APKCj/g3V/7y5f8ArYfg mj/gof8A8oKP+DdX/vLl/wCth+CaP+Ch/wDygo/4N1f+8uX/AK2H4JoAP+Do7/lOv+3N/wB2zf8A rHn7PtH/AAUP/wCUFH/Bur/3ly/9bD8E0f8AB0d/ynX/AG5v+7Zv/WPP2faP+Ch//KCj/g3V/wC8 uX/rYfgmgD9/v+d67/P/AEh4o/4MY/8AnKL/AN2Tf+/cUf8AO9d/n/pDxR/wYx/85Rf+7Jv/AH7i gA/4MY/+cov/AHZN/wC/cUf86KP+f+kw9H/BjH/zlF/7sm/9+4o/50Uf8/8ASYegA/4PnP8AnF1/ 3ez/AO+j1+AP/BPD/lBR/wAHFX/eI3/1sPxtX7/f8Hzn/OLr/u9n/wB9Hr8Af+CeH/KCj/g4q/7x G/8ArYfjagA/4Ojv+U6/7c3/AHbN/wCsefs+1+/3/BID/nTv/wC9gz/3plfgD/wdHf8AKdf9ub/u 2b/1jz9n2v3+/wCCQH/Onf8A97Bn/vTKAP3+/wCCh/8AynX/AODdX/vLl/6x54Jr/IFr/X6/4KH/ APKdf/g3V/7y5f8ArHngmv8AIFoA/f7/AIOjv+U6/wC3N/3bN/6x5+z7R/wdHf8AKdf9ub/u2b/1 jz9n2j/g6O/5Tr/tzf8Ads3/AKx5+z7R/wAHR3/Kdf8Abm/7tm/9Y8/Z9oA/v8/4Ojv+UFH7c3/d s3/rYf7PtfgD/wA713+f+kPFfv8Af8HR3/KCj9ub/u2b/wBbD/Z9r8Af+d67/P8A0h4oAP8AgkB/ zp3/APewZ/70yv4A6/v8/wCCQH/Onf8A97Bn/vTK/gDoA/1+v+Do7/lBR+3N/wB2zf8ArYf7PtH/ AAa4/wDKCj9hn/u5n/1sP9oKj/g6O/5QUftzf92zf+th/s+0f8GuP/KCj9hn/u5n/wBbD/aCoA/g D/4Ncf8AlOv+wz/3cz/6x5+0FR/wUP8A+UFH/Bur/wB5cv8A1sPwTR/wa4/8p1/2Gf8Au5n/ANY8 /aCo/wCCh/8Aygo/4N1f+8uX/rYfgmgD/X6ooooAKKKKAP/ZCmVuZHN0cmVhbQplbmRvYmoKOCAw IG9iaiA8PC9MZW5ndGggNDQxMi9GaWx0ZXIvRmxhdGVEZWNvZGU+PnN0cmVhbQp4nMVby3LbyJLd 6ytq2R1BwXg/fFdsie1Rhyz5imzHxEzPogSUaDhAgAYIueXPmJ+a7/DSCy86endX92RWAQQfAvtq ImbaHWKJLFRl5ePkySzq09lPizMvFLEdikV2Nluc/f3MFb/Qu46w8Y9+xr4rFquzVz87wsHo4eyH HxcfaS5+x796uT+4e3NmW474fPaf/4XHs7MksgIR2QF++m4gzj3bFrU6++nsk/Btm/ehtxw7oRk8 L12JV/lqaYvLSvx9KI7j2DSFBNLPQuzzfjQbym1jleGjru9ZjiN8J7HcuDtRSAfSMkD+H6apWm8q 8STSqqy+pJUoZEPjLE/zqlSNyJRoVP1Iv2IWfVqrZlPnqfn8U0sThFwXeSpLIQuhysfvFT23UjXe Sr/jmbUq8mVdNbIhVdpieUTy5w/i+Y7lRcL3HCsIuoPEuwf5Oa9XUtypVb5R5UaJSkzbTVXnX2RW Pb+nfh72G1i0N6NnC9cRq7Mg9GhQdIPdl5TfDlzbTKDR/mtKa+kpZrD7kpq9CvM6/JmezU+KGfRy Br2ggRFx+KpljV3XzKHR/ivLoqeYwe5L2m1YdIPdlz15t2I6IgptFtPnUfHMRNeOeaMVj/ROzywZ h2YmjUbX9LFnkNCafjC+pi0i1+bNIx6NTAzjbiKNxib6trEPjWjige8bHDlmYNd3GLAcRP2562so 2YlzqDaOrbiPDWcvOG6mF1e3N9PrsUBwxDmDXxgj0Nx9KMGKoRdbQbSLjNstLmfzxdXNdDG9u7od bGPTqrTKOS1Kiw83i4JQeO4Bbjlk9nBks7vZ26vF7GYxO7oTrUgr78b3jj3s3nD2qOGCsJsYhEcN t4NSIvJDy++tkOwK/a6usjbdVK9fBoFIKCOLX/yZ5ctKY+9LNwjD6PkNbh+QAkpJuC6zVd7kf5Yv 3scd2ednlX6Qr/6tquWLlnfgZaPrv1PNmIKO+YGD7KkjmEe2c9oRgnjEVu9lUSGJCvmHzPKsaoQS +Wpd1Rv1QoUGY253pZcWUqzlUtajWjXY8wmsw+V3vdByE2xBfCYKLIJ3TVScA6Li0RTfihySQz9P VKUfDfkTeJBv6R0gtxPw+mG45UHuIQ8KeArzIP0sE6F+OMqEKHrcxLKfIw8X07vFVFzM7hZXP19d TC+nL/XseGyTS/vSW9j4L4xdL8Fr7CbBv790q8B+fis39mw/8cW5eDu9vLu6FPNfL4Rrv3QrZ2Qr 23vl2K9c24mEnby27deB++K4HdvHjSb2Xz/AvxQ9Y57hTdzgJedxfbAHP3h+4V9u5zNY5+5qKqZ3 0ze3N+Li7tf/ePFWgLwR776+nomb6d3F1Vy8vb1Z/IocfS08Id45L97QHlEaO7aYT28W4v3Vxezm f8TlTFxM54sZBFm8bEsArzOy5U+3Vzfzi6mYj3CcE8vbzikVzq9mN1PxUocAqYm9sZi13cjE6wsD 1UvivtJz9yo9gORn+pHiMLc3l1dECGdz8WZ2M7ubXmN0ObsW89nde/qk23/70M4uqMaeKSe7B4fT Y9/yuKA+Jss1isMmX7Y5FW27lSfKzq8l8w2uLVMwDiov+2oUcze13KC+m4i0fapE+Q9UnBWmy1ql ClxIqEKskWm5IDTTK2v8cFFk4WVHWstJfHoPsy07QpJjyacFLfmQy7pECm9azb0mom02KuNCWVJt LUkOfLquqzWELqvVfU3VqZYOp0tbWXzC+WtRQT5UynVTgWdRXb1uM7wvBSpafJzjFPW3xjypq+y8 zNRaQWd0xBWfkwr2XDxwOUz78fyGCEBxL0mjNyfMG9mWF+5rwLXdTgN+7LACbmZvbi+upj9dzyxx 01JTAHrYfKtXOSnkadeWVWnMeF8Q89k/dVqt1vKP75IN2fQ2bkS7yfGUWmEIs0roFeVPWi3zTE4E niwbzW7omJtaLZmgdsawxGx4cHIo8fi1yLcdgeMaCD3Li/+CBliii6quVdXA8LDao/rSdUQKJT7I J9nJlcExjW/QCCLKJdsLtvyI9/MH6p9o+3UEG2Nj6BNOG4SWl+wIfDQS/cTyneci0bHOxe19kS9l 3/kpoGvTTxnf33ct39tTmO0HfdA4seObTWib2WBhaietFYUJ4qldwUdW5MzcQIKxywp+vMkf5dZ9 xGNuNFcKfKbqXO51m3SHqiQnIUdE6H3I7/kXthh2GngOfnY2tHYla8iCqZawKhveOzuhCS8wTHyg CdeNAf5aE4lDhoImTJ+tUWmryjSXupOGc7RcZKUFJGXIy8uUpEs3cHgaQ0WKxg+yaCjCH2rZZm0B iSmMylTVJaPBthXHnlcS/6+1f/XBx4r7Bqec9FiTQZ+Psm7wlvodfgBFsesaFZ1UgBtbfnSAn7br dvgZ2loBWwTo9YtQz2RmiYvbu7vZ7RzHfSDzs2Zg7ALbp7LBzwIBVWi4g6nJMCkXUoy63PbDOpxO VvAtnVrWVUYgIO/rvKaKC/ZX3MSUNbRjVNXkJVxOByNrH+njEf4lB35xIhZdx/KTPQV4tmNCATAS hCYUXITComK5qQgkARodA+awtYKCmnzDahdZjhM3HJswF8pYuLbsQVaasNFedI8VatWWOCq7POCm 8yKR5nXaFvp4v/2wblXGybFpiw1UAX3u7kyywHe+Lqn6/6zux0/vUCm3BwRATmN8O7ZjPvznz5+t 1AQdHQebDDLCttXQeR15IGL28ZuGpY0qvj1UJYMEEKDsuEFaaNeHvzlJhP9/+9Hq18B0AtZNjixg lA2XU/WGwJsMr1b3EmFHYda0JMM2qQJCNmrJPoZVoOMC9hnXhB1ZgfsXVIGMJ4X2c86ZOA72oVFR qEcoBs5Z56pZ8mlT2TawUib/IJUU3FpnwkCCaUxpZHPCRW3bCvyT6cJNPCsIn0sX5Lx335bkkyTX 3W4Mj+7vxqj09/Or7QaO16ULm1gyNpkzBSSEJpTcyN2cxDFCmiCQhxxVg/zAXOyhZlNlbS1Nih0A /hNQHaBBXt1xBt4ByEJ22LRwe8Brq+ovEgnmCZ+BR8kNFivUF7KS5p+G0XGe6vQuLmiVjB2Gm33j TuJGiRXsg4UbQvMGLewAdTVTDUWcFuBMzgpPbhRzv1WZfwG34QNQlH6rM6hfkAiUGKtaywddgFEQ iQIiVjt5x+RZmXIMwd+xlCwoIIYK6okIbazPRjCDTyv+bF3IL5VYStI3X7lgAbX8R3ni9K4VOvtQ mQTEPvn0QUcbem0yvMnmO46kY4XBcifGh2ohRkXuCGxYUgYExlA26w8DsPvWr0MUjc+r4fj3tGjZ ixhADWjWu3RhIaEuujSrNLmkD9khx88dBla4T5eOnntoKOLNEPB3eHIFKyNt5ewLuTZ+m8IUqWqk vvzaryqMY0A5uc4ChqVyulfavrQ0lzB0hAniDL831aR3gi0T6QqLQ8Y9fvAgtsKD9BDYURf4gMfE oAvxRCoOtU9pVlcwE2wY+LojwdHAnmvtv5OB35MnHBBKMie5KifUoXN3HjaoKvlak6g5Fw/y4Gb0 id0JoqHY+iiJXR5k0xPacKzwgCqFdthpI6TLY9IGcZyBJibHxThM1eL/Ms3OWMihZEqrblwJvm+F +wh4VAlN+1FtjPGJB8LkMPV3VE4pZUECY9noFAlH7+x5wiG9yNq3gO05QZeHXE3V7hSoQVU01Wsx Z1iEZ+WZhs8BHqZU5Jba/zTqHIdQSXU/5EZOoSCe6BUNarGOGUwKqb0Z9TSfyJQuvadiGUZDXTtL WIAbA027pmJcZ6EuH5zQgm1FB1TlUA2DQDNZo8dXCEXZydSvdaev7tJhhz9PenczT+GsUp/2sQKO meXhRh+q7mLkhBldz4r2mza2F/RFh+ObooOvXrryKoM9j6ueENXszHZ75McMZHJVL5HeWloCQa/p BGja6uvv+YoV74Hg2UK1SNcUG3OajN1godQQBBDOnHsmz6Ry4jN1Ry62HbJxPTihFe23bmyU/VGn By8xxZcxnGpeC+e3H3nT/py9eibCNZ/Val01edr1JKp7xKJJRiiPsupvgCFvO7erXQdzM7rtqixg OuLkHqy7JXemJPvEBC2rCso8qq7lijLdIMV1XkwOjbLnFXkJ0U5sNq4OO7GiA555TB0rRfTbcDwc peXiCtpZyka7NGUOFAu6L3LPvEDu8MATHmq71kFHKbE7kItCuuHmvMe8mgywMn0YxhyIomTXy+SC lftgetafmiL3CZGLg6rL38P0POHSkOqfB6TxVtaMT3v9gJJ7eVRQ6AQBCxS0EfcBii3o72aL0eM7 SWDF+0yPLsL6asg2KH9F+FmqDcJqN3kNKznKr8Df2nQGBtT0w7DfhpPdt7r7yc6sg6rpHhpisA7q D4ogh1YnHQPuqDWh6kedWrVeqr6duGUQOrFqXY/rgb6lsc/8nMALOj2Ejq9bi6aseVX13o+gIhYo uh6oWqe7LUKmS/XQdYy1GdS4xASDzThJGuJ+hNMYeqShjAur7yWPdhotmhPvJFpY7B2xnxMKcMzX VE4pYL8jYeT6/6Q44yeLfCuOThbVThhZcfJcUe0h+N9Rq6FPspfyVA0JsmQlB7GVxG7XcvQST6v0 kikK+QHnbKIrRaU7XMQX9L2CrDffUelUIjA5/lo9idt6+bUEzxJO8MpJkoRwRKBc12k6V3Snwe8d EZ6Vj4Ahtibe6WYF1Zdqp72nS3ma/gDnLXKK2O6qo7vCGVdD4FnJPolxbD/sm/ZOrCEmq1JKH5vK CAHvoZpXk3xkcfjMB7pH4julYe9XUYNRzCscmPRHhHcDV+k860ks4EzLWj7gl7k1tSb4+daaITDM FEqQphvHNYYpTMgQD7n5siUddrPNnkduu074oR9ayQEX8v2ezEW67TRtcm4JQAmyeKw6cgOYzx9y vi8quk2Zo77PLI27XeudH2g7q1HhIzXQ8JXNF9nDhBp8JdSYnI/H4NbeFzgeqbOp6GJsW+k/DW6B tllJoFKHhgC5T+Nq8BIrOaBCLpWXOuEkjia1RnDmoY3m8MS+oBqq6+CqPZNvur01cryp23U1aIsj EBhcqZeCMtV0oAdfn9XmpeYh1S2S40UTLvNFoJ3bTfCtDXMRzAJIKbD8tjRd6Qm3QLiVI09owbWS w07boRYqTvj9+ajU5qZypvMDh2cDysDfWzqQSLekSVvAT4TORCAKSl0Gaw8rWYsAkps3/32iO8nV xh5RcsItUwp1FINKbyrqk3KLolB8LVhncmUSvL5q46Y/PnqsUskMlzsq4L4m9CqciLkC4O+jQoKl hMNYRP22D9pmKHlUww2RdHszMKHiJ1Wmj/4kqp4gm+Z9fxPaAWyXvACo4wqg7zkclGOOHXeAbvue 1sGzsPwiTAY7ynKpr8q3vobF6pxu0jTx+NfR73ze3nPTqZPgQqfW5vyEFlzLPrhTPKaF99+luASO fC9FhIoltiNbvJUZSvATjmZ3X88Z+lrsdBtEbqRJeXfEvr3ZY59JFfp6plNs2il23Sm2u9OhSOJm TYXSAbUE6h9SbWUcY3vXyfdb/T0n35Byxc/cpzcn91jb2oQWfxlBNyU0ZumqZVQFSYxj/gUN6Obx BLUc4dNXVAJ0P9jLpSuMYUrjEmqiD0uVxPBemY9eCpS0/R0eq0GzbCr7B6oYN2HiWPbpe+/Ypz+D eIZy+dRnBMV5Eh+hyibLWbWju0aR5XjPrXdN1nsa3FZ3vRwoC5y+kPxXFDub6d4Hg8Yv7ZclZ06E U53fo8wrNOBys2Wdb7h3ZNzgMR808N+BH2MMLq//HoNuura5d1yNEazuP3egd0z1e0qdtYCCpqKs vdnJ50e2GPmK6fE/nPFQHwUidujvYpzIE+dB2P3hDP1Ki9A3xnlWFHrb74t6h19HBcuOdJddP8pf Ru1G/8svEP4TYZTZgAplbmRzdHJlYW0KZW5kb2JqCjEgMCBvYmo8PC9QYXJlbnQgOSAwIFIvQ29u dGVudHMgOCAwIFIvVHlwZS9QYWdlL1Jlc291cmNlczw8L1hPYmplY3Q8PC9pbWczIDcgMCBSL2lt ZzIgNSAwIFIvaW1nMSA0IDAgUi9pbWcwIDMgMCBSPj4vUHJvY1NldCBbL1BERiAvVGV4dCAvSW1h Z2VCIC9JbWFnZUMgL0ltYWdlSV0vRm9udDw8L0YxIDIgMCBSL0YyIDYgMCBSPj4+Pi9NZWRpYUJv eFswIDAgNTk1IDg0Ml0+PgplbmRvYmoKMTAgMCBvYmpbMSAwIFIvWFlaIDAgODU0IDBdCmVuZG9i agoyIDAgb2JqPDwvQmFzZUZvbnQvSGVsdmV0aWNhL1R5cGUvRm9udC9FbmNvZGluZy9XaW5BbnNp RW5jb2RpbmcvU3VidHlwZS9UeXBlMT4+CmVuZG9iago2IDAgb2JqPDwvQmFzZUZvbnQvSGVsdmV0 aWNhLUJvbGQvVHlwZS9Gb250L0VuY29kaW5nL1dpbkFuc2lFbmNvZGluZy9TdWJ0eXBlL1R5cGUx Pj4KZW5kb2JqCjkgMCBvYmo8PC9UeXBlL1BhZ2VzL0NvdW50IDEvS2lkc1sxIDAgUl0+PgplbmRv YmoKMTEgMCBvYmo8PC9OYW1lc1soSlJfUEFHRV9BTkNIT1JfMF8xKSAxMCAwIFJdPj4KZW5kb2Jq CjEyIDAgb2JqPDwvRGVzdHMgMTEgMCBSPj4KZW5kb2JqCjEzIDAgb2JqPDwvTmFtZXMgMTIgMCBS L1R5cGUvQ2F0YWxvZy9QYWdlcyA5IDAgUj4+CmVuZG9iagoxNCAwIG9iajw8L0NyZWF0b3IoSmFz cGVyUmVwb3J0cyBcKGltcHJlc29VbmlmaWNhZG9OYWNpb25hbENhc3RlbGxhbm9cKSkvUHJvZHVj ZXIoaVRleHQgMi4wLjggXChieSBsb3dhZ2llLmNvbVwpKS9Nb2REYXRlKEQ6MjAxNzEwMDMwOTAw NTIrMDInMDAnKS9DcmVhdGlvbkRhdGUoRDoyMDE3MTAwMzA5MDA1MiswMicwMCcpPj4KZW5kb2Jq CnhyZWYKMCAxNQowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwNDI2MTAgMDAwMDAgbiAKMDAwMDA0 Mjg2NiAwMDAwMCBuIAowMDAwMDAwMDE1IDAwMDAwIG4gCjAwMDAwMTEwNTkgMDAwMDAgbiAKMDAw MDAyNDA5OSAwMDAwMCBuIAowMDAwMDQyOTUzIDAwMDAwIG4gCjAwMDAwMzM0MzIgMDAwMDAgbiAK MDAwMDAzODEzMCAwMDAwMCBuIAowMDAwMDQzMDQ1IDAwMDAwIG4gCjAwMDAwNDI4MzEgMDAwMDAg biAKMDAwMDA0MzA5NSAwMDAwMCBuIAowMDAwMDQzMTUwIDAwMDAwIG4gCjAwMDAwNDMxODMgMDAw MDAgbiAKMDAwMDA0MzI0MSAwMDAwMCBuIAp0cmFpbGVyCjw8L1Jvb3QgMTMgMCBSL0lEIFs8Y2U1 MTA2NjZkNzEzNGNhYmNmMDAwNjQwMjAwODAwOTk+PDZiZTFlNWI5ZWM0YWUzMDUyOTliM2Y1NDc5 NTgxNWQ5Pl0vSW5mbyAxNCAwIFIvU2l6ZSAxNT4+CnN0YXJ0eHJlZgo0MzQzNQolJUVPRgo=";
        savePdfOnSD(b);
    }


//    public static GetFilePathAndStatus getFileFromBase64AndSaveInSDCard(String base64, String filename,String extension){
//        GetFilePathAndStatus getFilePathAndStatus = new GetFilePathAndStatus();
//        try {
//            byte[] pdfAsBytes = Base64.decode(base64, 0);
//            FileOutputStream os;
//            os = new FileOutputStream(getReportPath(filename,extension), false);
//            os.write(pdfAsBytes);
//            os.flush();
//            os.close();
//            getFilePathAndStatus.filStatus = true;
//            getFilePathAndStatus.filePath = getReportPath(filename,extension);
//            return getFilePathAndStatus;
//        } catch (IOException e) {
//            e.printStackTrace();
//            getFilePathAndStatus.filStatus = false;
//            getFilePathAndStatus.filePath = getReportPath(filename,extension);
//            return getFilePathAndStatus;
//        }
//    }
//
//    public static String getReportPath(String filename,String extension) {
//        File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Pruebas");
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//        String uriSting = (file.getAbsolutePath() + "/" + filename + "."+extension);
//        return uriSting;
//
//    }
//    public static class GetFilePathAndStatus{
//        public boolean filStatus;
//        public String filePath;
//
//    }


    public static boolean savePdfOnSD(String base64) {
        return saveContentOnFile(Base64.decode(base64, 0), getReportPath(getActualDatePdf(), ".pdf"));
    }

    public static String getReportPath(String filename, String extension) {
        String folderRoute = Environment.getExternalStorageDirectory().getPath()
                + "/TALINA/pdf";

        File file = new File(folderRoute);
        if (!file.exists()) {
            file.mkdirs();
        }

        return folderRoute + File.separator + filename + extension;
    }

    public static boolean saveContentOnFile(byte[] content, String routeFile) {
        try {
            FileOutputStream os;
            os = new FileOutputStream(routeFile, false);
            os.write(content);
            os.flush();
            os.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public static String getActualDatePdf() {

        String fechaFormateada;
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        String day = formateDateFields(String.valueOf(calendar.get(java.util.Calendar.DATE)), 2);
        String month = formateDateFields(String.valueOf(calendar.get(java.util.Calendar.MONTH) + 1), 2);
        String year = formateDateFields(String.valueOf(calendar.get(java.util.Calendar.YEAR)), 2);
        String hour = formateDateFields(String.valueOf(calendar.get(java.util.Calendar.HOUR_OF_DAY)), 2);
        String minute = formateDateFields(String.valueOf(calendar.get(java.util.Calendar.MINUTE)), 2);
        String second = formateDateFields(String.valueOf(calendar.get(java.util.Calendar.SECOND)), 2);
        fechaFormateada = day + "-" + month + "-" + year + " " + hour + "-" + minute + "-" + second;

        return fechaFormateada;
    }

    private static String formateDateFields(String field, int lenght) {
        String resultado = field;

        for (int indice = field.length(); indice < lenght; indice++) {
            resultado = "0" + resultado;
        }
        return resultado;
    }

}
