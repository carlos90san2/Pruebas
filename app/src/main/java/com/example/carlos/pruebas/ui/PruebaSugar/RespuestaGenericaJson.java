package com.example.carlos.pruebas.ui.PruebaSugar;

import com.orm.SugarRecord;

import java.io.Serializable;

import co.uk.rushorm.core.RushObject;

public class RespuestaGenericaJson extends RushObject implements Serializable {


    private ResultadoJson resultado;

    public RespuestaGenericaJson() {
    }


    public ResultadoJson getResultado() {
        return resultado;
    }


    public void setResultado(ResultadoJson resultadoJson) {
        this.resultado = resultadoJson;
    }


}
