package com.example.carlos.pruebas.ui.PruebaSugar;

import java.util.List;

public class DestinoWrapper {

  private List<Destino> destinos;

  public List<Destino> getDestinos() {
    return destinos;
  }

  public void setDestinos(List<Destino> destinos) {
    this.destinos = destinos;
  }
}
