package com.example.carlos.pruebas.ui.Dagger2Pruebas;

import dagger.Component;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

@Component(modules = {EnvioModule.class, ContextModule.class})

public interface AppComponent {
    void inject(Dagger2Pruebas dagger2Pruebas);
}
