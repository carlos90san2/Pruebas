package com.example.carlos.pruebas.interactors;

import com.example.carlos.pruebas.model.Contact;

import java.util.List;

/**
 * Created by X70826SA on 08/05/2017.
 */

public interface ContactsInteractor {

    void getContacts();
    void addContact();
    void deleteContact(int position);

    interface contactLoadedFinishedListener{
        void contactLoadedOk(List<Contact> contactList);
        void contactLoadedError(String error);
    }

    interface contactReloadFinishedListener {
        void contactReloadOk(List<Contact> contactList);
        void contactReloadError(String error);
    }
}
