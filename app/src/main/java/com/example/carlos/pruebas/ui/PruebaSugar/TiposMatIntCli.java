package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class TiposMatIntCli extends RushObject{

  private String codCliente;
  private String codProducto;
  private String numHoras;
  private String numDias;
  private String bolDiferenciaTurno;
  private String difMinDias;
  private String horaMinMan;
  private String horaMinTar;
  private String horaMaxMan;
  private String horaMaxTar;
  private String obligatorio;

  public TiposMatIntCli() {
  }

  public String getCodCliente() {
    return codCliente;
  }

  public void setCodCliente(String codCliente) {
    this.codCliente = codCliente;
  }

  public String getCodProducto() {
    return codProducto;
  }

  public void setCodProducto(String codProducto) {
    this.codProducto = codProducto;
  }

  public String getNumHoras() {
    return numHoras;
  }

  public void setNumHoras(String numHoras) {
    this.numHoras = numHoras;
  }

  public String getNumDias() {
    return numDias;
  }

  public void setNumDias(String numDias) {
    this.numDias = numDias;
  }

  public String getBolDiferenciaTurno() {
    return bolDiferenciaTurno;
  }

  public void setBolDiferenciaTurno(String bolDiferenciaTurno) {
    this.bolDiferenciaTurno = bolDiferenciaTurno;
  }

  public String getDifMinDias() {
    return difMinDias;
  }

  public void setDifMinDias(String difMinDias) {
    this.difMinDias = difMinDias;
  }

  public String getHoraMinMan() {
    return horaMinMan;
  }

  public void setHoraMinMan(String horaMinMan) {
    this.horaMinMan = horaMinMan;
  }

  public String getHoraMinTar() {
    return horaMinTar;
  }

  public void setHoraMinTar(String horaMinTar) {
    this.horaMinTar = horaMinTar;
  }

  public String getObligatorio() {
    return obligatorio;
  }

  public void setObligatorio(String obligatorio) {
    this.obligatorio = obligatorio;
  }

  public String getHoraMaxMan() {
    return horaMaxMan;
  }

  public void setHoraMaxMan(String horaMaxMan) {
    this.horaMaxMan = horaMaxMan;
  }

  public String getHoraMaxTar() {
    return horaMaxTar;
  }

  public void setHoraMaxTar(String horaMaxTar) {
    this.horaMaxTar = horaMaxTar;
  }
}
