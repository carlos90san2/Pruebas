package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultadoEntregaWrapper {

  @SerializedName("resultadoEntrega")
  private List<ResultadoEntrega> resultadosEntrega;

  public List<ResultadoEntrega> getResultadosEntrega() {
    return resultadosEntrega;
  }

  public void setResultadosEntrega(List<ResultadoEntrega> resultadosEntrega) {
    this.resultadosEntrega = resultadosEntrega;
  }
}
