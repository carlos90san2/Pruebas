package com.example.carlos.pruebas.ui.Dagger2;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class Coche {

    private Motor motor;

    public Coche(Motor motor) {
        this.motor = motor;
    }

    public String getMotor() {
        return ("Coche con " + motor.getTipoMotor());
    }
}
