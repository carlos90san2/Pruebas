package com.example.carlos.pruebas.ui.Dagger2;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

import dagger.Component;

/**
 * El componente es el puente entre los módulos creados y la parte del código que solicita esos objetos
 */

@Component(modules = {MotorModule.class})
public interface MotorComponent {
    void inject(DaggerActivity daggerActivity);
}
