package com.example.carlos.pruebas.model;

import java.io.Serializable;

import co.uk.rushorm.core.RushObject;

/**
 * Created by X70826SA on 08/05/2017.
 */

public class Time extends RushObject implements Serializable{
    private String time;
    private double milliseconds_since_epoch;
    private String date;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getMilliseconds_since_epoch() {
        return milliseconds_since_epoch;
    }

    public void setMilliseconds_since_epoch(double milliseconds_since_epoch) {
        this.milliseconds_since_epoch = milliseconds_since_epoch;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Time{" +
                "time='" + time + '\'' +
                ", milliseconds_since_epoch=" + milliseconds_since_epoch +
                ", date='" + date + '\'' +
                '}';
    }
}
