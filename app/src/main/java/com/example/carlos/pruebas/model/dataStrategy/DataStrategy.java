package com.example.carlos.pruebas.model.dataStrategy;

import com.example.carlos.pruebas.model.Contact;

import java.util.List;

/**
 * Creado por Carlos Sanchidrián Sánchez en 11/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public abstract class DataStrategy {

    public onGetContactsListener onGetContactsListener;

    public onGetContactsListener getOnGetContactsListener(){
        return onGetContactsListener;
    }

    public void setOnGetContactsListener(onGetContactsListener onGetContactsListener){
        this.onGetContactsListener = onGetContactsListener;
    }

    public interface onGetContactsListener{
        void onGetContactsListener(List<Contact> contactList);
        void onGetContactsListenerError(String error);
    }
}
