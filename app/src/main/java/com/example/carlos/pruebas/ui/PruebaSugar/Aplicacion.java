package com.example.carlos.pruebas.ui.PruebaSugar;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

/**
 * Gestiona apartados globales de la aplicación, como pueden ser los cambios en la configuración y
 * permite acceder a los recuros globales de la aplicación.
 */
public class Aplicacion extends com.orm.SugarApp {
  // -------------------------------------------------------------------------
  // ATRIBUTOS*/
  private static Context _contexto = null;

  private static Activity _currentActivity = null;

  // -------------------------------------------------------------------------
  // ON CREATE*/

  /**
   * Proporciona un acceso global desde todos los puntos de la aplicación al contexto de ésta.
   *
   * @return Contexto de la aplicación.
   */
  public static Context getContext() {
    return _contexto;
  }

  // -------------------------------------------------------------------------
  // GETTERS*/

  public static Activity getCurrentActivity() {
    return Aplicacion._currentActivity;
  }

  public static void setCurrentActivity(Activity act) {
    Aplicacion._currentActivity = act;
  }

  @Override
  public void onCreate() {
    super.onCreate();

    _contexto = this.getApplicationContext();
  }

}