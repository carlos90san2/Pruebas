package com.example.carlos.pruebas.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.carlos.pruebas.R;
import com.example.carlos.pruebas.model.Contact;
import com.example.carlos.pruebas.model.Time;

import java.util.ArrayList;
import java.util.List;

import co.uk.rushorm.android.AndroidInitializeConfig;
import co.uk.rushorm.core.Rush;
import co.uk.rushorm.core.RushCore;
import okhttp3.OkHttpClient;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(logging);

        List<Class<? extends Rush>> classes = new ArrayList<>();
        classes.add(Time.class);
        classes.add(Contact.class);

        AndroidInitializeConfig config
                = new AndroidInitializeConfig(getApplicationContext(), classes);
        RushCore.initialize(config);

        startActivity(new Intent(this, MainActivity.class));
        finish();

    }
}
