package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import co.uk.rushorm.core.RushObject;

/**
 * Created by dani on 7/05/16.
 */
public class TipoResulValija extends RushObject{
  @SerializedName("CodResultadoValija")
  private String codResultadoValija;
  @SerializedName("DesResultadoValija")
  private String desResultadoValija;

  public TipoResulValija() {
  }

  public String getCodResultadoValija() {
    return codResultadoValija;
  }

  public void setCodResultadoValija(String codResultadoValija) {
    this.codResultadoValija = codResultadoValija;
  }

  public String getDesResultadoValija() {
    return desResultadoValija;
  }

  public void setDesResultadoValija(String desResultadoValija) {
    this.desResultadoValija = desResultadoValija;
  }
}
