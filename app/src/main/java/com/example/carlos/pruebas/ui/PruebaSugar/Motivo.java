package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class Motivo extends RushObject {

  private String codMotivo;
  private String descripcion;
  private String numTipPersona;
  private String idRush; //Clave primaria de la tabla, se compone de codMotivo + numTipPersona

  public Motivo() {
  }

  public String getCodMotivo() {
    return codMotivo;
  }

  public void setCodMotivo(String codMotivo) {
    this.codMotivo = codMotivo;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getNumTipPersona() {
    return numTipPersona;
  }

  public void setNumTipPersona(String numTipPersona) {
    this.numTipPersona = numTipPersona;
  }

  public String getIdRush() {
    return idRush;
  }

  public void setIdRush(String idRush) {
    this.idRush = idRush;
  }
}
