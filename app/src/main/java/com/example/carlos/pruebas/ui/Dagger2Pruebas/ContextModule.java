package com.example.carlos.pruebas.ui.Dagger2Pruebas;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

@Module
public class ContextModule {

    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context providesContext(){
        return context;
    }
}
