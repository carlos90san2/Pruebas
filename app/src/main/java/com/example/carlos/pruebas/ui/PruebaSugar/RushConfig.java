package com.example.carlos.pruebas.ui.PruebaSugar;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import co.uk.rushorm.android.AndroidInitializeConfig;
import co.uk.rushorm.core.Rush;
import co.uk.rushorm.core.RushCore;

/**
 * Creado por Carlos Sanchidrián Sánchez en 20/09/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class RushConfig {

    public RushConfig() {
    }

    public static boolean init(Context context) {

        List<Class<? extends Rush>> classes = new ArrayList<Class<? extends Rush>>();
        classes.add(DatosUnidad.class);
        classes.add(Destino.class);
        classes.add(DiaFestivo.class);
        classes.add(Familia.class);
        classes.add(GrupoSeccion.class);
        classes.add(GrupoSeccionRush.class);
        classes.add(Motivo.class);
        classes.add(Ocupacion.class);
        classes.add(Producto.class);
        classes.add(RespuestaDatosMaestros.class);
        classes.add(ResultadoEntrega.class);
        classes.add(ResultadoJson.class);
        classes.add(ResultadoLiquidacion.class);
        classes.add(Seccion.class);
        classes.add(Situacion.class);
        classes.add(TipoDocumento.class);
        classes.add(TipoImporte.class);
        classes.add(TipoResulValija.class);
        classes.add(TipoResulServ.class);
        classes.add(TipoServicio.class);
        classes.add(TiposMatIntCli.class);
        classes.add(Turno.class);
        classes.add(UnidadLista.class);


        try {
            AndroidInitializeConfig config
                    = new AndroidInitializeConfig(context.getApplicationContext(), classes);
            RushCore.initialize(config);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
