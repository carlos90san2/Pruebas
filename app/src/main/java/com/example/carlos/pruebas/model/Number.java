package com.example.carlos.pruebas.model;

/**
 * Created by X70826SA on 08/05/2017.
 */

public class Number {

    private String one;
    private String key;

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
