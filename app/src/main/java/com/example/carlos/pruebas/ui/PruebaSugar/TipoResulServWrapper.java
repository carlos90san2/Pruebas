package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TipoResulServWrapper {

  @SerializedName("tipoResulServ")
  private List<TipoResulServ> tiposResulServ;

  public List<TipoResulServ> getTiposResulServ() {
    return tiposResulServ;
  }

  public void setTiposResulServ(List<TipoResulServ> tiposResulServ) {
    this.tiposResulServ = tiposResulServ;
  }
}
