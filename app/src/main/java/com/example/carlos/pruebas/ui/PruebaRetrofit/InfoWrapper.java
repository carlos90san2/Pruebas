package com.example.carlos.pruebas.ui.PruebaRetrofit;

/**
 * Creado por Carlos Sanchidrián Sánchez en 26/12/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class InfoWrapper {

    private int id;
    private String name;

    public InfoWrapper() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
