package com.example.carlos.pruebas.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.carlos.pruebas.R;
import com.example.carlos.pruebas.adapters.ContactsAdapter;
import com.example.carlos.pruebas.interfacesMVP.ContactsMVP;
import com.example.carlos.pruebas.model.Contact;
import com.example.carlos.pruebas.presenters.ContactsPresenter;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Creado por Carlos Sanchidrián Sánchez en 05/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class MainActivity extends AppCompatActivity implements ContactsMVP.View, ContactsAdapter.onContactListInteractorListener{

    @BindView (R.id.recyclerViewMain) RecyclerView mRecyclerView;
    @BindView (R.id.fabAdd) FloatingActionButton fabAdd;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Contact> contactList;
    private ContactsPresenter contactsP;

    @OnClick(R.id.fabAdd)
    public void submit(View view){
        contactsP.addContact();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        //prefs.edit().putString("key", json).apply();
        prefs.getString("key", "mal");

        contactsP = new ContactsPresenter(this, MainActivity.this);
        contactsP.getContacts();
    }

    @Override
    public void printContactsList(List<Contact> contactList) {
        this.contactList = contactList;
        mAdapter = new ContactsAdapter(contactList, this, contactsP);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void printContactsError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateAdapter(List<Contact> contactList) {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRowClick(int position) {
        Intent i = new Intent(this, ContactDetailActivity.class);
        i.putExtra("contact", (Serializable) contactList.get(position));
        startActivity(i);
    }
}
