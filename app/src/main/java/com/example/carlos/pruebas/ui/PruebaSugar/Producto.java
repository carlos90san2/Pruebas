package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class Producto extends RushObject{

  private String codProducto;
  private String descripcion;
  private String codResultado;
  private String idRush; //Clave primaria de la tabla. Se compone de codProducto + descripcion


  public Producto() {
  }

  public String getCodProducto() {
    return codProducto;
  }

  public void setCodProducto(String codProducto) {
    this.codProducto = codProducto;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  public String getCodResultado() {
    return codResultado;
  }

  public String getIdRush() {
    return idRush;
  }

  public void setIdRush(String idRush) {
    this.idRush = idRush;
  }

  public void setCodResultado(String codResultado) {
    this.codResultado = codResultado;
  }
}
