package com.example.carlos.pruebas.ui.PruebaSugar;


import java.util.List;

import co.uk.rushorm.core.RushCore;
import co.uk.rushorm.core.RushSearch;

/**
 * Creado por Carlos Sanchidrián Sánchez en 21/09/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class DatosMaestrosRushManager {


    public static RespuestaDatosMaestros getDataFromDB() {
        RespuestaDatosMaestros respuestaDatosMaestros = null;
        List<RespuestaDatosMaestros> list = new RushSearch().find(RespuestaDatosMaestros.class);
        if (list != null && list.size() > 0) {
            respuestaDatosMaestros = list.get(list.size() - 1);
            respuestaDatosMaestros.getDatosUnidad().saveListaGruposSeccionesRush();
            respuestaDatosMaestros.saveDiasFestivosRush();
        }
        return respuestaDatosMaestros;
    }


    public static void save(RespuestaDatosMaestros respuestaDatosMaestros) {
        RushCore.getInstance().clearDatabase();

        respuestaDatosMaestros.getDatosUnidad().setListaGruposSeccionesRush();
        respuestaDatosMaestros.setDiasFestivosRush();

        //Asigna las claves primarias de las tablas
        try {
            RushCore.getInstance().registerObjectWithId(respuestaDatosMaestros, String.valueOf("1"));
        } catch (Exception e) {
            RushConfig.init(Aplicacion.getContext());
            RushCore.getInstance().registerObjectWithId(respuestaDatosMaestros, String.valueOf("1"));
        }
        RushCore.getInstance().registerObjectWithId(respuestaDatosMaestros.getDatosUnidad(), respuestaDatosMaestros.getDatosUnidad().getCodUnidad());
        for (Destino destino : respuestaDatosMaestros.getDestinos()) {
            RushCore.getInstance().registerObjectWithId(destino, destino.getCodDestino() + destino.getCodPostal());
        }
        for (DiaFestivo diaFestivo : respuestaDatosMaestros.getDiasFestivosRush()) {
            RushCore.getInstance().registerObjectWithId(diaFestivo, diaFestivo.getDiasFestivosUnd());
        }
        for (Familia familia : respuestaDatosMaestros.getFamilias()) {
            RushCore.getInstance().registerObjectWithId(familia, familia.getDescripcion());
            for (ResultadoEntrega resultadoEntrega : familia.getResultadosEntrega()) {
                RushCore.getInstance().registerObjectWithId(resultadoEntrega, resultadoEntrega.getDescripcion());
            }
        }
        for (GrupoSeccionRush grupoSeccionRush : respuestaDatosMaestros.getDatosUnidad().getListaGruposSeccionesRush()) {
            for (GrupoSeccion grupoSeccion : grupoSeccionRush.getGrupoSeccion()) {
                RushCore.getInstance().registerObjectWithId(grupoSeccionRush, grupoSeccion.getCodGrupo());
            }
        }
        for (Motivo motivo : respuestaDatosMaestros.getMotivos()) {
            RushCore.getInstance().registerObjectWithId(motivo, motivo.getCodMotivo() + motivo.getNumTipPersona());
        }
        for (Ocupacion ocupacion : respuestaDatosMaestros.getOcupaciones()) {
            RushCore.getInstance().registerObjectWithId(ocupacion, ocupacion.getCodOcupacion() + ocupacion.getNumTipPersona());
        }
        for (Producto producto : respuestaDatosMaestros.getProductos()) {
            RushCore.getInstance().registerObjectWithId(producto, producto.getCodProducto() + producto.getDescripcion());
        }
        for (ResultadoLiquidacion resultadoLiquidacion : respuestaDatosMaestros.getResultadosLiquidacion()) {
            RushCore.getInstance().registerObjectWithId(resultadoLiquidacion, resultadoLiquidacion.getCodFamilia() +
            resultadoLiquidacion.getDescripcion());
            for (ResultadoEntrega resultadoEntrega : resultadoLiquidacion.getResultadosEntrega()) {
                RushCore.getInstance().registerObjectWithId(resultadoEntrega, resultadoEntrega.getDescripcion());
            }
        }
        for (Seccion seccion : respuestaDatosMaestros.getDatosUnidad().getSecciones()) {
            RushCore.getInstance().registerObjectWithId(seccion, seccion.getCodSeccion() + seccion.getCodCarteroSeccion());
        }
        for (Situacion situacion : respuestaDatosMaestros.getSituaciones()) {
            RushCore.getInstance().registerObjectWithId(situacion, situacion.getCodSituacion() + situacion.getDescripcion());
        }
        for (TipoDocumento tipoDocumento : respuestaDatosMaestros.getTiposDocumentos()) {
            RushCore.getInstance().registerObjectWithId(tipoDocumento, tipoDocumento.getDesTipDocumento());
        }
        for (TipoImporte tipoImporte : respuestaDatosMaestros.getTiposImporte()) {
            RushCore.getInstance().registerObjectWithId(tipoImporte, tipoImporte.getCodImporte());
        }
        for (TipoResulServ tipoResulServ : respuestaDatosMaestros.getTiposResulServ()) {
            RushCore.getInstance().registerObjectWithId(tipoResulServ, tipoResulServ.getCodResultadoServ() + tipoResulServ.getDescripcion());
        }
        if(respuestaDatosMaestros.getTiposResultadosValijas() != null) {
            for (TipoResulValija tipoResulValija : respuestaDatosMaestros.getTiposResultadosValijas()) {
                RushCore.getInstance().registerObjectWithId(tipoResulValija, tipoResulValija.getCodResultadoValija());
            }
        }
        for (TipoServicio tipoServicio : respuestaDatosMaestros.getTiposServicios()) {
            RushCore.getInstance().registerObjectWithId(tipoServicio, tipoServicio.getCodTipoServicios());
        }
        for (TiposMatIntCli tiposMatIntCli : respuestaDatosMaestros.getTiposMatIntCli()) {
            RushCore.getInstance().registerObjectWithId(tiposMatIntCli, tiposMatIntCli.getCodCliente());
        }
        for (Turno turno : respuestaDatosMaestros.getDatosUnidad().getTurnosUnidades()) {
            RushCore.getInstance().registerObjectWithId(turno, turno.getDesTurno());
        }
        for (UnidadLista unidadLista : respuestaDatosMaestros.getDatosUnidad().getUnidadesLista()) {
            RushCore.getInstance().registerObjectWithId(unidadLista, unidadLista.getCodLista());
        }

        RushCore.getInstance().save(respuestaDatosMaestros);
    }

    public static boolean isEmpty() {

        List<RespuestaDatosMaestros> list = null;

        try {
           list = new RushSearch().find(RespuestaDatosMaestros.class);
        } catch (Exception e) {
            RushConfig.init(Aplicacion.getContext());
            list = new RushSearch().find(RespuestaDatosMaestros.class);
        }
        if (list == null || list.size() == 0)
            return true;
        return false;
    }
}
