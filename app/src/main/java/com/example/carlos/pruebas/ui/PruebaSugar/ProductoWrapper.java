package com.example.carlos.pruebas.ui.PruebaSugar;

import java.util.List;

public class ProductoWrapper {

  private List<Producto> productos;

  public List<Producto> getProductos() {
    return productos;
  }

  public void setProductos(List<Producto> productos) {
    this.productos = productos;
  }


}
