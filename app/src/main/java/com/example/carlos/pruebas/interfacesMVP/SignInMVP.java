package com.example.carlos.pruebas.interfacesMVP;

import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by Carlos Sanchidrián Sánchez on 09/05/2017.
 * carlos.sanchidrian@p-externos.iecisa.com
 */

public interface SignInMVP {

    interface View{
        void errorLogin(String error);
        void loginSuccess(GoogleSignInAccount acct);
        void signOutSuccess(String messae);
    }

    interface Connect{
        void requestConnect();
        void requestDisconnect();
        void setDataConnect();
        void checkResultConnect(int requestCode, Intent data);
    }
}
