package com.example.carlos.pruebas.ui.PruebaSugar;

import java.util.List;

public class OcupacionWrapper {

  private List<Ocupacion> ocupaciones;

  public List<Ocupacion> getOcupaciones() {
    return ocupaciones;
  }

  public void setOcupaciones(List<Ocupacion> ocupaciones) {
    this.ocupaciones = ocupaciones;
  }


}
