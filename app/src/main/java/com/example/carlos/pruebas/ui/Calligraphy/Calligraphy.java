package com.example.carlos.pruebas.ui.Calligraphy;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.carlos.pruebas.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Calligraphy extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // initalize Calligraphy
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/new_typography.otf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
