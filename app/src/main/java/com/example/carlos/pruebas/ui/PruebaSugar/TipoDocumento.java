package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import co.uk.rushorm.core.RushObject;

public class TipoDocumento extends RushObject {

    @SerializedName("m_iCodTipoDocumento")
    private String codTipDocumento;
    @SerializedName("m_strDesTipoDocumento")
    private String desTipDocumento;
    @SerializedName("m_iCodOrdenMaestros")
    private String orden;
    @SerializedName("m_iCodTipoPersona")
    private String numTipPersona;

    public TipoDocumento() {
    }

    public String getCodTipDocumento() {
        return codTipDocumento;
    }

    public void setCodTipDocumento(String codTipDocumento) {
        this.codTipDocumento = codTipDocumento;
    }

    public String getDesTipDocumento() {
        return desTipDocumento;
    }

    public void setDesTipDocumento(String desTipDocumento) {
        this.desTipDocumento = desTipDocumento;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getNumTipPersona() {
        return numTipPersona;
    }

    public void setNumTipPersona(String numTipPersona) {
        this.numTipPersona = numTipPersona;
    }
}
