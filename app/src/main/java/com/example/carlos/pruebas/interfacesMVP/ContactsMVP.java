package com.example.carlos.pruebas.interfacesMVP;

import com.example.carlos.pruebas.model.Contact;

import java.util.List;

/**
 * Creado por Carlos Sanchidrián Sánchez en 05/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public interface ContactsMVP {

    interface View {
        void printContactsList(List<Contact> contactList);
        void printContactsError(String error);
        void updateAdapter(List<Contact> contactList);
    }

    interface Presenter {
        void getContacts();
        void addContact();
        void deleteContact(int position);
    }
}