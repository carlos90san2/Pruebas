package com.example.carlos.pruebas.ui.Dagger2;

import android.app.Application;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class MotorApplication extends Application {
    private MotorComponent motorComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        motorComponent = DaggerMotorComponent.builder().motorModule(new MotorModule()).build();
    }

    public MotorComponent getMotorComponent() {
        return this.motorComponent;
    }
}
