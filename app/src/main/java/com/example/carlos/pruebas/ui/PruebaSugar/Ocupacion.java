package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

/**
 * Created by SEVPCW0740 on 14/04/2016.
 */
public class Ocupacion extends RushObject{

  private String codOcupacion;
  private String descripcion;
  private String numTipPersona;
  private String idRush; //Clave primaria de la tabla. Se compone de codOcupacion + numTipPersona

  public Ocupacion() {
  }

  public String getCodOcupacion() {
    return codOcupacion;
  }

  public void setCodOcupacion(String codOcupacion) {
    this.codOcupacion = codOcupacion;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getNumTipPersona() {
    return numTipPersona;
  }

  public void setNumTipPersona(String numTipPersona) {
    this.numTipPersona = numTipPersona;
  }

  public String getIdRush() {
    return idRush;
  }

  public void setIdRush(String idRush) {
    this.idRush = idRush;
  }
}
