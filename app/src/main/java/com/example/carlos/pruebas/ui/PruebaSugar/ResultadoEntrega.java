package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class ResultadoEntrega extends RushObject {

  private String codResultado;
  private String codFamilia;
  private String descripcion;
  private String tipoResultado;

  public ResultadoEntrega() {
  }

  public String getCodResultado() {
    return codResultado;
  }

  public void setCodResultado(String codResultado) {
    this.codResultado = codResultado;
  }

  public String getCodFamilia() {
    return codFamilia;
  }

  public void setCodFamilia(String codFamilia) {
    this.codFamilia = codFamilia;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getTipoResultado() {
    return tipoResultado;
  }

  public void setTipoResultado(String tipoResultado) {
    this.tipoResultado = tipoResultado;
  }
}
