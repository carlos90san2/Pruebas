package com.example.carlos.pruebas.ui.PruebaSugar;

import java.util.List;

public class TipoDocumentoWrapper {

  private List<TipoDocumento> tiposDocumentos;

  public List<TipoDocumento> getTiposDocumentos() {
    return tiposDocumentos;
  }

  public void setTiposDocumentos(List<TipoDocumento> tiposDocumentos) {
    this.tiposDocumentos = tiposDocumentos;
  }
}
