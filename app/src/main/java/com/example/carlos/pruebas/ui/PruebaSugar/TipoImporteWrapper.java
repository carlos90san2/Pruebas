package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TipoImporteWrapper {

  @SerializedName("tipoImporte")
  private List<TipoImporte> tiposImporte;

  public List<TipoImporte> getTiposImporte() {
    return tiposImporte;
  }

  public void setTiposImporte(List<TipoImporte> tiposImporte) {
    this.tiposImporte = tiposImporte;
  }
}
