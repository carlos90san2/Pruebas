package com.example.carlos.pruebas.adapters.holders;

import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carlos.pruebas.R;
import com.example.carlos.pruebas.model.Contact;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by X70826SA on 08/05/2017.
 */

public class ContactsHolder extends RecyclerView.ViewHolder{
    @BindView(R.id.textViewRowContact) TextView mTextView;
    @BindView(R.id.imageViewDeleteRow) ImageView imageViewDelete;

    public ContactsHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindContact(Contact contact){
        mTextView.setText(contact.getName());
    }

    public ImageView getImageViewDelete(){
        return imageViewDelete;
    }

}
