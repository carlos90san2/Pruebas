package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class ResultadoJson extends RushObject {

  private String descripcion;
  private String numero;
  private String resultado;
  private String nip;

  public ResultadoJson() {
  }

  public String getNip() {
    return nip;
  }

  public void setNip(String nip) {
    this.nip = nip;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getResultado() {
    return resultado;
  }

  public void setResultado(String resultado) {
    this.resultado = resultado;
  }

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }
}
