package com.example.carlos.pruebas.ui.PruebaRetrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.carlos.pruebas.R;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;


public class RetrofitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);


        String API_BASE_URL = "https://api.github.com/";

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder
                        .client(httpClient.build())
                        .build();

        GitHubClient client =  retrofit.create(GitHubClient.class);


        Call<List<InfoWrapper>> call =
                client.reposForUser("fs-opensource");

        call.enqueue(new Callback<List<InfoWrapper>>() {
            @Override
            public void onResponse(Call<List<InfoWrapper>> call, Response<List<InfoWrapper>> response) {
                // TODO: use the repository list and display it
                String a = "";
            }

            @Override
            public void onFailure(Call<List<InfoWrapper>> call, Throwable t) {
                // TODO: handle error
            }
        });
    }


    public interface GitHubClient {
        @GET("/users/{user}/repos")
        Call<List<InfoWrapper>> reposForUser(
                @Path("user") String user
        );
    }
}
