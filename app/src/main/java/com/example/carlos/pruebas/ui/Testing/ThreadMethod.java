package com.example.carlos.pruebas.ui.Testing;

import android.os.AsyncTask;

/**
 * Creado por Carlos Sanchidrián Sánchez en 19/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class ThreadMethod {

    private String greeting;

    public String runThread() {

        new AsyncTask() {
            @Override
            protected Void doInBackground(Object[] params) {
                greeting = "Hi";
                return null;
            }
        }.execute();

        return greeting;
    }


}
