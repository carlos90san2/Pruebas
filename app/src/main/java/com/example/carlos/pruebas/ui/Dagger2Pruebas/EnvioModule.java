package com.example.carlos.pruebas.ui.Dagger2Pruebas;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

@Singleton
@Module
public class EnvioModule {

    @Provides
    public Envio providesEnvio(){
        return new Envio("1234567", "casa");
    }
}
