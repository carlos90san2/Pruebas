package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.annotations.RushList;

public class ResultadoLiquidacion extends RushObject {

    private String codFamilia;
    private String descripcion;
    private String idRush; //Clave primaria de la tabla. Se compone de codFamilia + descricipon

    @SerializedName("resultadoEntrega")
    @RushList(classType = ResultadoEntrega.class)
    private List<ResultadoEntrega> resultadosEntrega;

    public ResultadoLiquidacion() {
    }

    public String getCodFamilia() {
        return codFamilia;
    }

    public void setCodFamilia(String codFamilia) {
        this.codFamilia = codFamilia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ResultadoEntrega> getResultadosEntrega() {
        return resultadosEntrega;
    }

    public void setResultadosEntrega(List<ResultadoEntrega> resultadosEntrega) {
        this.resultadosEntrega = resultadosEntrega;
    }

    public String getIdRush() {
        return idRush;
    }

    public void setIdRush(String idRush) {
        this.idRush = idRush;
    }
}
