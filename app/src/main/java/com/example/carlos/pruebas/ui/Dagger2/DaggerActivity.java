package com.example.carlos.pruebas.ui.Dagger2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.carlos.pruebas.R;

import javax.inject.Inject;
import javax.inject.Named;

public class DaggerActivity extends AppCompatActivity {

    @Named("gasolina")
    @Inject
    Motor motor;

    @Inject
    Coche coche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dagger);

        ((MotorApplication)getApplication()).getMotorComponent().inject(this);
        Toast.makeText(this, motor.getTipoMotor(), Toast.LENGTH_SHORT).show();

        Toast.makeText(this, coche.getMotor(), Toast.LENGTH_SHORT).show();
    }
}
