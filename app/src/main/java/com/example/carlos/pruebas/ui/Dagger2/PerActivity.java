package com.example.carlos.pruebas.ui.Dagger2;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {

}
