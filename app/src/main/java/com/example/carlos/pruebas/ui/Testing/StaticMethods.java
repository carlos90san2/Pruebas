package com.example.carlos.pruebas.ui.Testing;

/**
 * Creado por Carlos Sanchidrián Sánchez en 19/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class StaticMethods {

    public static String getGreeting() {
        return "Hello";
    }

    public static String getGreeting(String greeting) {
        return greeting;
    }
}
