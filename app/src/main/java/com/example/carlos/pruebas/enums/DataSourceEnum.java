package com.example.carlos.pruebas.enums;

/**
 * Creado por Carlos Sanchidrián Sánchez en 11/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public enum DataSourceEnum {
    DATA_MOCK,
    DATA_DB,
    DATA_WS
}
