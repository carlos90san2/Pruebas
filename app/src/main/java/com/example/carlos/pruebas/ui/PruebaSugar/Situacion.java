package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class Situacion extends RushObject{

  private String codSituacion;
  private String descripcion;
  private String idRush; //Clave primaria de la tabla. Se compone de codSituacion + descripcion

  public Situacion() {
  }

  public String getCodSituacion() {
    return codSituacion;
  }

  public void setCodSituacion(String codSituacion) {
    this.codSituacion = codSituacion;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getIdRush() {
    return idRush;
  }

  public void setIdRush(String idRush) {
    this.idRush = idRush;
  }
}
