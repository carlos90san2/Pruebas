package com.example.carlos.pruebas.model.dataStrategy;

import com.example.carlos.pruebas.model.Contact;

import java.util.List;

import co.uk.rushorm.core.RushCore;
import co.uk.rushorm.core.RushSearch;

/**
 * Creado por Carlos Sanchidrián Sánchez en 11/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class DataDB extends DataStrategy{

    public void setContacts(List<Contact> contactList){
        for(Contact contacts : contactList){
            RushCore.getInstance().registerObjectWithId(contacts, contacts.getPhone());
            contacts.save();
            //Obtiene el objeto con la información de la base de datos
            //Contact contact = new RushSearch().findSingle(Contact.class);
        }
        List<Contact> contactList2 = new RushSearch().find(Contact.class);
        onGetContactsListener.onGetContactsListener(contactList);
    }

    public void getContacts(){
        List<Contact> contactList = new RushSearch().find(Contact.class);
        onGetContactsListener.onGetContactsListener(contactList);
    }
}
