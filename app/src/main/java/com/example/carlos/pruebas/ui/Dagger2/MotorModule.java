package com.example.carlos.pruebas.ui.Dagger2;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * El módulo se encarga de proveer a nuestra actividad todas las instancias necesarias para que funcionen nuestras clases.
 * Es decir, debemos crear los módulos para que dagger sepa devolver los objetos que van a ser requeridos más adelante
 *
 */

@Module
public class MotorModule {

    /**
     * Los provides lo utilizamos delante de los métodos que proporcionan objetos para la injección de dependencias
     */

    @Named("diesel")
    @Provides
    public Motor providesMotorDiesel() {
        return new Motor("diesel");
    }

    @Named("gasolina")
    @Provides
    public Motor providesMotorGasolina() {
        return new Motor("gasolina");
    }

    @Provides
    public Coche provideCoche(@Named("diesel")Motor motor) {
        return new Coche(motor);
    }


}

