package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FamiliaWrapper {

  @SerializedName("familia")
  private List<Familia> familias;

  public List<Familia> getFamilias() {
    return familias;
  }

  public void setFamilias(List<Familia> familias) {
    this.familias = familias;
  }
}
