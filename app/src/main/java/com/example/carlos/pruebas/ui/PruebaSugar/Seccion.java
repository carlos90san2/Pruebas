package com.example.carlos.pruebas.ui.PruebaSugar;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import co.uk.rushorm.core.RushObject;

public class Seccion extends RushObject implements Serializable {

  private String codTurno;
  private String codGrupo;
  private String codSeccion;
  private String codTipoSeccion;
  private String bolRestaurarPDA;
  private String codCarteroSeccion;
  //@SerializedName("nombreCarteroSeccion")
  private String nombreCarteroSeccion;
  private String unidadLista;
  private String idRush;

  public Seccion() {
  }

  public String getUnidadLista() {
    return unidadLista;
  }

  public void setUnidadLista(String unidadLista) {
    this.unidadLista = unidadLista;
  }

  public String getCodTurno() {
    return codTurno;
  }

  public void setCodTurno(String codTurno) {
    this.codTurno = codTurno;
  }

  public String getCodGrupo() {
    return codGrupo;
  }

  public void setCodGrupo(String codGrupo) {
    this.codGrupo = codGrupo;
  }

  public String getCodSeccion() {
    return codSeccion;
  }

  public void setCodSeccion(String codSeccion) {
    this.codSeccion = codSeccion;
  }

  public String getCodTipoSeccion() {
    return codTipoSeccion;
  }

  public void setCodTipoSeccion(String codTipoSeccion) {
    this.codTipoSeccion = codTipoSeccion;
  }

  public String getBolRestaurarPDA() {
    return bolRestaurarPDA;
  }

  public void setBolRestaurarPDA(String bolRestaurarPDA) {
    this.bolRestaurarPDA = bolRestaurarPDA;
  }

  public String getCodCarteroSeccion() {
    return codCarteroSeccion;
  }

  public void setCodCarteroSeccion(String codCarteroSeccion) {
    this.codCarteroSeccion = codCarteroSeccion;
  }

  public String getNombreCarteroSeccion() {
    return nombreCarteroSeccion;
  }

  public void setNombreCarteroSeccion(String nombreCarteroSeccion) {
    this.nombreCarteroSeccion = nombreCarteroSeccion;
  }

  public String getIdRush() {
    return idRush;
  }

  public void setIdRush(String idRush) {
    this.idRush = idRush;
  }

  @Override
  public String toString() {
    return codSeccion;
  }
}
