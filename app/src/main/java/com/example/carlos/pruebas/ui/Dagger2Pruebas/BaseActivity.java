package com.example.carlos.pruebas.ui.Dagger2Pruebas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.carlos.pruebas.R;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    public AppComponent getComponent() {
        return ((EnvioApplication)getApplication()).getAppComponent();
    }
}
