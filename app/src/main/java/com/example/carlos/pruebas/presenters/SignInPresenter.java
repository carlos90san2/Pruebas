package com.example.carlos.pruebas.presenters;

import android.content.Context;
import android.content.Intent;

import com.example.carlos.pruebas.interactors.SignInInteractor;
import com.example.carlos.pruebas.interactors.SignInInteractorImpl;
import com.example.carlos.pruebas.interfacesMVP.SignInMVP;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by X70826SA on 09/05/2017.
 */

public class SignInPresenter implements SignInMVP.Connect, SignInInteractor.responseRequest{

    private Context context;
    private SignInMVP.View signInListener;
    private SignInInteractor interactor;

    public SignInPresenter(SignInMVP.View signInListener, Context context){
        this.signInListener = signInListener;
        this.context = context;
        this.interactor = new SignInInteractorImpl(this, context);
    }

    @Override
    public void requestConnect() {
        this.interactor.requestConnect();
    }

    @Override
    public void requestDisconnect() {
        this.interactor.requestDisconnect();
    }

    @Override
    public void setDataConnect() {
        this.interactor.setDataConnect();
    }

    @Override
    public void checkResultConnect(int requestCode, Intent data) {
        this.interactor.checkResultConnect(requestCode, data);
    }

    @Override
    public void errorLogin(String error) {
        signInListener.errorLogin(error);
    }

    @Override
    public void loginSuccess(GoogleSignInAccount acct ) {
        signInListener.loginSuccess(acct);
    }

    @Override
    public void signOutSuccess(String message) {
        signInListener.signOutSuccess(message);
    }
}
