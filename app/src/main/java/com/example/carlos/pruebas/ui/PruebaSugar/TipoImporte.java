package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class TipoImporte extends RushObject {
  private String codImporte;
  private String descripcion;
  private String signoOperacion;

  public TipoImporte() {
  }

  public String getCodImporte() {
    return codImporte;
  }

  public void setCodImporte(String codImporte) {
    this.codImporte = codImporte;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getSignoOperacion() {
    return signoOperacion;
  }

  public void setSignoOperacion(String signoOperacion) {
    this.signoOperacion = signoOperacion;
  }
}
