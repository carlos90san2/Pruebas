package com.example.carlos.pruebas.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.carlos.pruebas.R;
import com.example.carlos.pruebas.model.Contact;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactDetailActivity extends AppCompatActivity {

    private Context context;
    @BindView(R.id.textViewNameContact) TextView textViewName;
    @BindView(R.id.textViewSurnameContact) TextView textViewSurname;
    @BindView(R.id.textViewPhoneContact) TextView textViewPhone;
    @BindView(R.id.textViewDescriptionContact) TextView textViewDescription;
    @BindView(R.id.imageViewProfileContact) ImageView imageViewProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_detail_activity);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        this.context = this;
        Bundle bundle = getIntent().getExtras();
        fillView((Contact) bundle.get("contact"));
    }

    public void fillView(Contact contact){
        textViewName.setText(contact.getName());
        textViewSurname.setText(contact.getSurname());
        textViewPhone.setText(contact.getPhone());
        textViewDescription.setText(contact.getDescription());
        Glide.with(context).load(contact.getImage()).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageViewProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageViewProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
    }
}
