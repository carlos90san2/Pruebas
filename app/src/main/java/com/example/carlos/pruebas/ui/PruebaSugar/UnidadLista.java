package com.example.carlos.pruebas.ui.PruebaSugar;

import co.uk.rushorm.core.RushObject;

public class UnidadLista extends RushObject{

  private String codLista;
  private String desLista;

  public UnidadLista() {
  }

  public String getCodLista() {
    return codLista;
  }

  public void setCodLista(String codLista) {
    this.codLista = codLista;
  }

  public String getDesLista() {
    return desLista;
  }

  public void setDesLista(String desLista) {
    this.desLista = desLista;
  }
}
