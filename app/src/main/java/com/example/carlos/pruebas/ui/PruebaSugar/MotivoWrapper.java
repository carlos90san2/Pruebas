package com.example.carlos.pruebas.ui.PruebaSugar;

import java.util.List;

public class MotivoWrapper {

  private List<Motivo> motivos;

  public List<Motivo> getMotivos() {
    return motivos;
  }

  public void setMotivos(List<Motivo> motivos) {
    this.motivos = motivos;
  }
}
