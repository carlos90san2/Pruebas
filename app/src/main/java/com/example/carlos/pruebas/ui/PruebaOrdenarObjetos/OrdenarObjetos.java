package com.example.carlos.pruebas.ui.PruebaOrdenarObjetos;

import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.carlos.pruebas.R;
import com.orm.util.Collection;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrdenarObjetos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordenar_objetos);

        List<Objeto1> objeto1List = new ArrayList<Objeto1>();

        objeto1List.add(new Objeto1(3));
        objeto1List.add(new Objeto1(2));
        objeto1List.add(new Objeto1(10));

//        Collections.sort(objeto1List, new Comparator<Objeto1>() {
//            @Override
//            public int compare(Objeto1 o1, Objeto1 o2) {
//                return o1.getId() - o2.getId();
//            }
//        });
        Collections.sort(objeto1List);

        String a = "";
    }
}
