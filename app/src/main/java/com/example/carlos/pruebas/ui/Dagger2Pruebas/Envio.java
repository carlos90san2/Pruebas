package com.example.carlos.pruebas.ui.Dagger2Pruebas;

/**
 * Creado por Carlos Sanchidrián Sánchez en 18/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class Envio {

    private String codEnvio;
    private String direccion;

    public Envio(String codEnvio, String direccion) {
        this.codEnvio = codEnvio;
        this.direccion = direccion;
    }

    public String getCodEnvio() {
        return codEnvio;
    }

    public void setCodEnvio(String codEnvio) {
        this.codEnvio = codEnvio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
