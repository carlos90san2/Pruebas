package com.example.carlos.pruebas.model.dataStrategy;
import com.example.carlos.pruebas.enums.DataSourceEnum;
import com.example.carlos.pruebas.model.utils.ConfigData;

/**
 * Creado por Carlos Sanchidrián Sánchez en 11/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class DataManager {

    public DataManager(){
    }

    public static DataStrategy getDefaultDataStrategy(){
        return getCustomDataStrategy(ConfigData.dataSelected);
    }

    public static DataStrategy getCustomDataStrategy(DataSourceEnum dataSource){
        switch (dataSource) {
            case DATA_MOCK:
                return new DataMock();
            case DATA_DB:
                return new DataDB();
            case DATA_WS:
                return new DataWS();
        }
        return null;
    }
}
