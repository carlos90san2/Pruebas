package com.example.carlos.pruebas.ui.PruebaSugar;

import java.io.Serializable;

import co.uk.rushorm.core.RushObject;

public class GrupoSeccion extends RushObject implements Serializable {

  private String codGrupo;
  private String descripcion;

  public GrupoSeccion() {
  }

  public String getCodGrupo() {
    return codGrupo;
  }

  public void setCodGrupo(String codGrupo) {
    this.codGrupo = codGrupo;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    GrupoSeccion that = (GrupoSeccion) o;

    if (!codGrupo.equals(that.codGrupo)) {
      return false;
    }
    return descripcion.equals(that.descripcion);

  }

  @Override
  public int hashCode() {
    int result = codGrupo.hashCode();
    result = 31 * result + descripcion.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return descripcion;
  }
}
