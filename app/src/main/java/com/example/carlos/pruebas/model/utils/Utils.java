package com.example.carlos.pruebas.model.utils;

import android.content.Context;

import com.example.carlos.pruebas.model.Contact;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Creado por Carlos Sanchidrián Sánchez en 11/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class Utils {

    public static List<?> readGsonFromFile(Context context, String fileRoute){
        Type collectionType = new TypeToken<List<Contact>>() {
        }.getType();

        Gson gson = new Gson();
        InputStream input = context.getClass().getClassLoader().getResourceAsStream(fileRoute);
        JsonReader reader2 = new JsonReader(new InputStreamReader(input));

        try {
            return gson.fromJson(reader2, collectionType);
        }catch (Exception e){
            return null;
        }
    }
}
