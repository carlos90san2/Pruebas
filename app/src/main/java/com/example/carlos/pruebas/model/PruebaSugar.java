package com.example.carlos.pruebas.model;

import com.orm.SugarRecord;

import java.util.List;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.annotations.RushList;

/**
 * Creado por Carlos Sanchidrián Sánchez en 06/09/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class PruebaSugar extends RushObject {
    private String nombre;

    @RushList(classType = Apellidos.class)
    private List<Apellidos> apellido;

    public PruebaSugar() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Apellidos> getApellido() {
        return apellido;
    }

    public void setApellido(List<Apellidos> apellido) {
        this.apellido = apellido;
    }
}
