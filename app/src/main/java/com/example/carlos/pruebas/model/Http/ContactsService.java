package com.example.carlos.pruebas.model.Http;

import com.example.carlos.pruebas.model.Time;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by X70826SA on 08/05/2017.
 */

public interface ContactsService {
    @GET(".")
    Call<Time> getContacts();
    //Call<Time> listRepos(@Path("time") String user);
}
