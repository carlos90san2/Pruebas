package com.example.carlos.pruebas.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.carlos.pruebas.R;
import com.example.carlos.pruebas.interfacesMVP.SignInMVP;
import com.example.carlos.pruebas.presenters.SignInPresenter;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.SignInButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartActivity extends AppCompatActivity implements View.OnClickListener, SignInMVP.View{

    private Context context;
    private SignInPresenter presenter;
    private final String TAG = "StartActivity";

    @BindView(R.id.sign_in_button)      SignInButton buttonSignIn;
    @BindView(R.id.sign_out_button)     Button buttonSignOut;
    @BindView(R.id.textViewName)        TextView textViewName;
    @BindView(R.id.textViewGivenName)   TextView textViewGiven;
    @BindView(R.id.textViewFamilyName)  TextView textViewFamily;
    @BindView(R.id.textViewEmail)       TextView textViewEmail;
    @BindView(R.id.textViewId)          TextView textViewId;
    @BindView(R.id.imageViewProfile)    ImageView imageViewProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
        presenter = new SignInPresenter(this, StartActivity.this);
        presenter.setDataConnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        context = this;
        buttonSignIn.setOnClickListener(this);
        buttonSignOut.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.sign_in_button:
                presenter.requestConnect();
                break;
            case R.id.sign_out_button:
                presenter.requestDisconnect();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.checkResultConnect(requestCode, data);
    }

    @Override
    public void errorLogin(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginSuccess(GoogleSignInAccount acct) {
        textViewName.setText(acct.getDisplayName());
        textViewGiven.setText(acct.getGivenName());
        textViewFamily.setText(acct.getFamilyName());
        textViewEmail.setText(acct.getEmail());
        textViewId.setText(acct.getId());

        //Necesita activar la credencial de Google+ https://console.developers.google.com/apis/api/plus.googleapis.com
        String URL = "https://www.googleapis.com/plus/v1/people/" +acct.getId() +"?fields=image&key=AIzaSyBz2jGjZl0VGkllxYYKRHPuuAX_oMxdyVQ";
        Log.i(TAG, "URL:" +URL);
        //Imagen circular
        Glide.with(this).load(URL).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageViewProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageViewProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
        buttonSignOut.setEnabled(true);
        Toast.makeText(this, "Login Correcto", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void signOutSuccess(String message) {
        textViewName.setText("");
        textViewGiven.setText("");
        textViewFamily.setText("");
        textViewEmail.setText("");
        textViewId.setText("");
        Glide.with(this).load(R.drawable.googleg_standard_color_18).into(imageViewProfile);
        buttonSignOut.setEnabled(false);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
