package com.example.carlos.pruebas.model.dataStrategy;

import android.content.Context;
import android.provider.ContactsContract;

import com.example.carlos.pruebas.model.Contact;
import com.example.carlos.pruebas.model.utils.Utils;

import java.util.List;

/**
 * Creado por Carlos Sanchidrián Sánchez en 11/05/2017.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

public class DataMock extends DataStrategy{

    private static final String MOCKS_ROUTE = "res/raw/";
    private final static String CONTACTS_MOCK_ROUTE =  MOCKS_ROUTE + "contacts_mock.json";
    private Context context;

    public void getContacts(Context context){
        List<Contact> contacts = (List<Contact>) Utils.readGsonFromFile(context, CONTACTS_MOCK_ROUTE);

        if(contacts != null){
            onGetContactsListener.onGetContactsListener(contacts);
        }
        else{
            onGetContactsListener.onGetContactsListenerError("Error al obtener los contactos");
        }
    }
}
