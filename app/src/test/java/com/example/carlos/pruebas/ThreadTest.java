package com.example.carlos.pruebas;

import android.os.AsyncTask;

import com.example.carlos.pruebas.ui.Testing.ThreadMethod;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Creado por Carlos Sanchidrián Sánchez en 19/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AsyncTask.class)

public class ThreadTest {

    private ThreadMethod threadMethod;

    private AsyncTask asyncTask = mock(AsyncTask.class);

    @Before
    public void setUp() {
        threadMethod = new ThreadMethod();
    }

    @Test
    public void runThread() {
        PowerMockito.mockStatic(AsyncTask.class);
        when(asyncTask.execute()).thenReturn(asyncTask);
        String greeting = threadMethod.runThread();
        Assert.assertEquals(greeting, "Hi");
    }
}
