package com.example.carlos.pruebas;

import com.example.carlos.pruebas.ui.Testing.CallStaticMethods;
import com.example.carlos.pruebas.ui.Testing.StaticMethods;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Creado por Carlos Sanchidrián Sánchez en 19/01/2018.
 * email: carlos.sanchidrian@p-externos.iecisa.com
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMethods.class})

public class StaticMethodsTest {

    private CallStaticMethods callStaticMethods;

    @Before
    public void setUp() {
        callStaticMethods = new CallStaticMethods();
    }

    @Test
    public void checkValidGreeting() {
        Assert.assertEquals(StaticMethods.getGreeting(), "Hello");
        Assert.assertEquals(StaticMethods.getGreeting("Hi"), "Hi");
    }

    @Test
    public void checkValidExtendGreeting() {
        Assert.assertEquals(callStaticMethods.extendGreeting("Hello"), "Hello, Hello");
    }
}
